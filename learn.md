---
layout:     page
title:      Learning
permalink: /learning/
order:      6
---

<h3>2024</h3>
<h4>Courses</h4>
<ul>
  <li>Machine Learning Onramp in MATLAB by MathWorks, Mar 11–13</li>
  <li>AWS HPC immersion training, Mar 5</li>
  <li>IBM Neuro-AI toolkit workshop, Feb 19–20</li>
</ul>

<h3>2023</h3>
<h4>Courses</h4>
<ul>
  <li>Generics and Traits in Rust&nbsp;2021, Shaun Meyer, <a href="https://app.pluralsight.com/library/courses/rust-2021-generics-traits/table-of-contents">Pluralsight</a>, Nov&nbsp;23, 2023</li>
  <li>Secure Passwords: Eight Ways to Strengthen and Secure Your Passwords Today! Nov&nbsp;9, 2023</li>
  <li>Mobile Essentials: Safe Web Browsing Retraining, Nov&nbsp;9, 2023</li>
  <li>Acceptable Use and Social Media Responsibility Retraining, Nov&nbsp;9, 2023</li>
  <li>Trusted Research and Innovation Awareness Training, Nov&nbsp;2, 2023</li>
  <li>Security Awareness Proficiency Assessment&nbsp;(SAPA), Nov&nbsp;2, 2023</li>
</ul>

<h3>2022</h3>

<h4>Workshops</h4>
<ul>
  <li>
    Hands-On HPC Application Development Using C++ and SYCL, Burns, Khaldi, Wong, Reinders, Keryell et al., Supercomputing&nbsp;2022, Online Tutorial, Nov&nbsp;14, 2022
  </li>
  <li>
    Introduction to Quantum Computing, Scott Pakin, Eleanor G.&nbsp;Rieffel, Supercomputing&nbsp;2022, Online Tutorial, Nov&nbsp;13, 2022
  </li>
  <li>
    AMD vision for HPC and exascale, Optimisation tools for AMD’s CPUs and GPUs, Matthew Hole et al., Daresbury Laboratory, STFC, Oct&nbsp;5, 2022
  </li>
  <li>
    Data Parallel Essentials for Python, Praveen Kundurthy, Intel virtual workshop, Sep&nbsp;29, 2022
  </li>
  <li>
    Solve the Mystery of Performance Profiling, James Tullos, Intel virtual workshop, Sep&nbsp;28, 2022
  </li>
  <li>
    SYCL Essentials&mdash;oneDPL Coding, Praveen Kundurthy, Intel virtual workshop, Sep&nbsp;20, 2022
  </li>
  <li>
    FPGA programming, Xavier Martorell, Daniel Jiménez-Mazure, CASTIEL workshop, Sep&nbsp;12, 2022
  </li>
  <li>
    <a href="https://devcloud.intel.com/oneapi/get_started/hpcTrainingModules">OpenMP Offload Basics</a>, Intel oneAPI HPC Training Modules, Aug&nbsp;29&ndash;Sep&nbsp;2, 2022
  </li>
  <li>
    SYCL Essentials&mdash;Advanced Concepts, Rakshith Krishnappa, Intel virtual workshop, Aug&nbsp;18, 2022
  </li>
</ul>

<h4>Text books</h4>
<ul>
  <li>
    <a href="http://bit.ly/programming-rust">Programming Rust: Fast, Safe Systems Development</a> (1st, 2018, <a href="https://www.oreilly.com/library/view/programming-rust/9781491927274">O'Reilly Media</a>) by Jim Blandy, Jason Orendorff
  </li>
  <li>
    <a href="http://www.intermediateperl.com">Intermediate Perl</a> (2nd, 2012, <a href="http://shop.oreilly.com/product/0636920012689.do">O'Reilly Media</a>) by Randal L.&nbsp;Schwartz, brian d&nbsp;foy, and Tom Phoenix
  </li>
</ul>


<h3>2021</h3>

<ul>
  <li>
    <a href="https://www.learning-perl.com">Learning Perl</a> (7th, 2017, <a href="https://www.oreilly.com/library/view/learning-perl-7th/9781491954317">O'Reilly Media</a>) by Randal L.&nbsp;Schwartz, brian d&nbsp;foy, and Tom Phoenix
  </li>
</ul>


<h3>2020</h3>

<ul>
  <li><a href="https://courses.nvidia.com/courses/course-v1:DLI+C-FX-01+V2/about">
    Fundamentals of Deep Learning for Computer Vision</a> (led by Jony Castagna)
    <figure class="imagecentre" style="margin-bottom: 1em">
    <a href="{{ site.url }}/assets/cert/abalenkov--nvidia-deep-learn-cert.png">
      <img src="{{ site.url }}/assets/cert/abalenkov--nvidia-deep-learn-cert.png" alt="NVIDIA Deep Learning for Computer Vision, Certificate">
    </a>
    </figure>
  </li>
  <li><a href="https://www.futurelearn.com/courses/functional-programming-haskell">
    Functional Programming in Haskell: Supercharge Your Coding</a><br>(led by <a href="http://www.dcs.gla.ac.uk/~jsinger/">Jeremy Singer</a> and <a href="http://www.dcs.gla.ac.uk/~wim/">Wim Vanderbauwhede</a>)
    <figure class="imagecentre" style="margin-bottom: 1em">
    <a href="{{ site.url }}/assets/cert/abalenkov--haskell-cert.png">
      <img src="{{ site.url }}/assets/cert/abalenkov--haskell-cert.png" alt="Haskell, Certificate">
    </a>
    </figure>
  </li>
</ul>

<h3>2019</h3>
<ul>
  <li><a href="https://courses.nvidia.com/courses/course-v1:DLI+C-AC-01+V1/about">
    Fundamentals of Accelerated Computing with CUDA C/C++</a> (led by Jony Castagna)</li>
  <li><a href="https://courses.nvidia.com/courses/course-v1:DLI+C-AC-03+V1/about">
    Fundamentals of Accelerated Computing with OpenACC</a> (led by Jony Castagna)</li>
</ul>

<h3>2018</h3>

<ul>
  <li>Introduction to Geometry (<a href="http://www.maths.manchester.ac.uk/study/undergraduate/courses/mathematics-bsc/course-unit-spec/?unitcode=MATH20222">MATH20222</a> led by <a href="http://www.maths.manchester.ac.uk/~khudian">H.&nbsp;M.&nbsp;Khudaverdyan</a>)<sup><a href="#fn1">1</a></sup></li>
  <li>Topics in Quantum Mechanics (led by <a href="http://www.maths.manchester.ac.uk/~khudian">H.&nbsp;M.&nbsp;Khudaverdyan</a>)<sup><a href="#fn1">1</a></sup></li>
</ul>


<h2>Teaching</h2>

<h3>2019</h3>
<ul>
  <li><a href="http://www.wolfram.com/mathematica">Mathematica</a> for <a href="http://www.foundationstudies.se.manchester.ac.uk">Foundation year</a> students&nbsp;(<a href="http://www.maths.manchester.ac.uk/study/undergraduate/information-for-current-students/service-teaching/foundation-studies/course-unit-spec/?level=1&courseUnit=MATH19872">0D2 MATH19872</a>)<sup><a href="#fn1">1</a></sup></li>
</ul>

<h3>2018</h3>
<ul>
  <li>Calculus and Algebra for <a href="http://www.eee.manchester.ac.uk">Electrical and Electronic Engineering</a> students (<a href="http://www.maths.manchester.ac.uk/study/undergraduate/information-for-current-students/course-units-offered/course-unit-spec/?unitcode=MATH19681">1M1 MATH19681</a>)<sup><a href="#fn1">1</a></sup></li>
</ul>

<h3>2017</h3>
<ul>
  <li>Calculus and Algebra for <a href="http://www.mace.manchester.ac.uk">Mechanical, Aerospace and Civil Engineering</a> students (<a href="http://www.maths.manchester.ac.uk/study/undergraduate/information-for-current-students/course-units-offered/course-unit-spec/?unitcode=MATH19661">1M1 MATH19661</a>)<sup><a href="#fn1">1</a></sup></li>
</ul>

<h3>2016</h3>
<ul>
  <li>Calculus and Algebra for <a href="http://www.mace.manchester.ac.uk">Mechanical, Aerospace and Civil Engineering</a> students (<a href="http://www.maths.manchester.ac.uk/study/undergraduate/information-for-current-students/course-units-offered/course-unit-spec/?unitcode=MATH19661">1M1 MATH19661</a>)<sup><a href="#fn1">1</a></sup></li>
  <li>Calculus and Algebra for <a href="http://www.foundationstudies.se.manchester.ac.uk">Foundation year</a> students (<a href="http://www.maths.manchester.ac.uk/study/undergraduate/information-for-current-students/service-teaching/foundation-studies/course-unit-spec/?unitcode=MATH19801">0B1 MATH19801</a>)<sup><a href="#fn1">1</a></sup></li>
</ul>

<h3>2014</h3>
<ul><li>Scientific computing with MATLAB<sup><a href="#fn2">2</a></sup></li></ul>

<h3>pre 2014</h3>
<ul><li><a href="{{ site.baseurl }}{% link latex.md %}">Introduction to LaTeX</a><sup><a href="#fn1">1</a></sup></li></ul>

<h3>pre 2012</h3>
<ul>
  <li>Computational Electromagnetics<sup><a href="#fn1">1</a></sup></li>
  <li>Data Networking<sup><a href="#fn1">1</a></sup></li>
  <li>Java Programming<sup><a href="#fn1">1</a></sup></li>
</ul>


<hr style="align: left; noshade; width: 25%">

<ul style="list-style: none; padding: 0; margin: 0">
  <font size="-1">
  <li id="fn1"><sup>1</sup>&nbsp;@&nbsp;<a href="http://www.manchester.ac.uk">The University of Manchester</a></li>
  <li id="fn2"><sup>2</sup>&nbsp;@&nbsp;<a href="http://www.imt-atlantique.fr">IMT Atlantique (IMT Atlantique Bretagne&ndash;Pays de la Loire)</a> formerly known as Télécom Bretagne and as ENST Bretagne</li>
  </font>
<ul>
