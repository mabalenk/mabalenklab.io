---
layout:     page
title:      Perl
permalink: /perl/
order:      7
---

Please find below my highly synthetic notes on <i class="fas fa-rook"></i>&nbsp;&ldquo;<a href="https://www.learning-perl.com">Learning Perl</a>&rdquo; (7th, 2017, <a href="https://www.oreilly.com/library/view/learning-perl-7th/9781491954317">O'Reilly Media</a>) by Randal L.&nbsp;Schwartz, brian d&nbsp;foy, and Tom Phoenix, the famous Llama book&nbsp;{% cite Schwartz17 --file references %}.

<ol start="1">
  <li><a href="#intro">Introduction</a></li>
  <li><a href="#scalars">Scalar Data</a></li>
  <li><a href="#lists">Lists and Arrays</a></li>
  <li><a href="#subroutines">Subroutines</a></li>
  <li><a href="#io">Input and Output</a></li>
  <li><a href="#hashes">Hashes</a></li>
  <li><a href="#regexp">Regular Expressions</a></li>
  <li><a href="#match_regexp">Matching with Regular Expressions</a></li>
  <li><a href="#proc_regexp">Processing Text with Regular Expressions</a></li>
  <li><a href="#ctrl">More Control Structures</a></li>
  <li><a href="#modules">Perl Modules</a></li>
  <li><a href="#files">File Tests</a></li>
  <li><a href="#directories">Directory Operations</a></li>
  <li><a href="#strings">Strings and Sorting</a></li>
  <li><a href="#processes">Process Management</a></li>
  <li><a href="#adv_tech">Some Advanced Perl Techniques</a></li>
</ol>

<h3 id="intro">1.&nbsp;Introduction</h3>


<h3 id="scalars">2.&nbsp;Scalar Data</h3>

A&nbsp;<i>scalar</i> is a&nbsp;single item.

A&nbsp;scalar is the&nbsp;simplest kind of data Perl manipulates. A&nbsp;scalar is usually a&nbsp;number (255 or 3.25e20) or a&nbsp;string of characters (hello or the Gettysburg address).

<h4>Numbers</h4>
<h5>All Numbers Have the Same Format Internally</h5>

Internally Perl relies on C libraries and uses double-precision floating-point values to store the numbers.

In Perl you can specify both integers (255 or 2,001) and floating-point numbers (3.14159 or 1.35 &times; 1,025). But internally Perl computes with double-precision floating-point values.

In other words, an&nbsp;integer constant in a&nbsp;Perl program is treated as an&nbsp;equivalent floating-point value.

<h5>Integer Literals</h5>

A&nbsp;<i>literal</i> is how a&nbsp;value is represented in a&nbsp;source code, it's the data typed directly into a&nbsp;program.

{% highlight perl %}
0
2001
-40
137
61298040283768
{% endhighlight %}

Use embedded underscores for reader-friendliness:

{% highlight perl %}
61_298_040_283_768
{% endhighlight %}

<h5>Nondecimal Integer Literals</h5>

Octal (base&nbsp;8) literals start with a&nbsp;leading 0 and use the digits from 0 to 7:

{% highlight perl %}
0377  # same as 255 decimal
{% endhighlight %}

Hexadecimal (base&nbsp;16) literals start with a&nbsp;leading <tt>0x</tt> and use the digits from 0 to 9 and the letters A through F (or a through f) to represent values from 0 to 15:

{% highlight perl %}
0xff  # FF hex, also 255 decimal
{% endhighlight %}

Binary (base&nbsp;2) literals start with a&nbsp;leading <tt>0b</tt> and use only the digits 0 and 1:

{% highlight perl %}
0b11111111  # also 255 decimal
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;The "leading zero" indicator works only for literals&mdash;not for automatic string-to-number conversions.

Use underscores to improve readability of long nondecimal literals:

{% highlight perl %}
0x1377_0B77
0x50_65_72_7C
{% endhighlight %}

<h5>Floating-Point Literals</h5>

Perl follows C-like notation for floating-point literals:

{% highlight perl %}
  1.25
255.000
255.0
  7.25e45  # 7.25 times 10 to the power of 45
 -6.5e24   # negative 6.5 times 10 to the 24th
-12e-24    # negative 12 times 10 to the -24th
 -1.2E-23  # another way to say that the E may be uppercase
{% endhighlight %}

Perl after v5.22 supports hexadecimal floating-point literals. Instead of an&nbsp;<tt>e</tt> to mark the exponent, use a&nbsp;<tt>p</tt> for the power of&nbsp;2 exponent:

{% highlight perl %}
0x1f.0p3
{% endhighlight %}

<h5>Numeric Operators</h5>

Operators are Perl's verbs:

{% highlight perl %}
2 + 3       # 2 plus 3, or 5
5.1 - 2.4   # 5.1 minus 2.4, or 2.7
3 * 12      # 3 times 12, or 36
14 / 2      # 14 divided by 2, or 7
10.2 / 0.3  # 10.2 divided by 0.3, or 34
10 / 3      # always, a floating-point division, i.e. 3.(3)
{% endhighlight %}

Perl's numeric operators return what you would expect from doing the same operation on a&nbsp;calculator.

Modulus operator (%) in Perl returns remainder of the division. Operands are cast to integers first:

{% highlight perl %}
10.5 % 3.2  # -> 10 % 3 -> 1
{% endhighlight %}

Fortran-like exponentiation operator is represented by an&nbsp;asterisk:

{% highlight perl %}
2**3  # two to the third power, or eight
{% endhighlight %}

<h4>Strings</h4>

Perl has the ability to have any character in a&nbsp;string. This allows to create, scan and manipulate raw binary data as strings.

Add <tt>utf8</tt> pragma to use Unicode in a&nbsp;program:

{% highlight perl %}
use utf8;
{% endhighlight %}

Make sure to save your files with the UTF&nbsp;8 encoding.

<i class="fas fa-bell"></i>&nbsp;A&nbsp;pragma is something that tells the Perl compiler how to act.

There are two flavours of literal strings in Perl: <i>single-quoted</i> and <i>double-quoted</i> string literals.

<h5>Single-Quoted String Literals</h5>

A&nbsp;single-quoted string literal&mdash;a&nbsp;sequence of characters enclosed in single quotes, the ' character:

{% highlight perl %}
'Fred'    # four characters F, r, e, d
'Barney'  # six characters B, a, r, n, e, y
''        # null (empty) string (no characters)
'⅚∞☃☠'    # "wide" Unicode characters
{% endhighlight %}

Single quote and backslash are exceptional, they need <i>escaping</i> with a&nbsp;backslash:

{% highlight perl %}
'Don\'t let an apostrophe end this string prematurely!'
'Last character is a backslash: \\'
'\'\\'  # single quote followed by backslash
{% endhighlight %}

Spreading the string into multiple lines results in a newline in a new string:

{% highlight perl %}
Peace to the
World!  # -> Peace to the, newline, World! (19 characters in total)
{% endhighlight %}

Perl doesn't interpret the <tt>\n</tt> within a&nbsp;single-quoted string as a&nbsp;newline:

{% highlight perl %}
'Peace to the\nWorld!'  # -> Peace to thenWorld!
{% endhighlight %}

<h5>Double-Quoted String Literals</h5>

A&nbsp;double-quoted string literal is a&nbsp;sequence of characters enclosed in double quotes.

Now backslash takes on its full power to specify certain control characters:

{% highlight perl %}
"Barney"                 # Just the same as 'Barney'
"Peace to the World!\n"  # Peace to the World! and a newline
"Last character of this string is a double quote mark: \""
"Coke\tSprite"           # Coke, a tab, and Sprite
"\x{2668}"               # Unicode HOT SPRINGS character code point
"\N{SNOWMAN}"            # Unicode Snowman by name
{% endhighlight %}

<table class="book">
  <caption>Table&nbsp;2-1. Double-quoted string backslash escapes</caption>
  <tr>
    <th>Construct</th>
    <th>Meaning</th>
  </tr>
  <tr>
    <td><tt>\n</tt></td>
    <td>Newline</td>
  </tr>
  <tr>
    <td><tt>\r</tt></td>
    <td>Return</td>
  </tr>
  <tr>
    <td><tt>\t</tt></td>
    <td>Tab</td>
  </tr>
  <tr>
    <td><tt>\f</tt></td>
    <td>Formfeed</td>
  </tr>
  <tr>
    <td><tt>\b</tt></td>
    <td>Backspace</td>
  </tr>
  <tr>
    <td><tt>\a</tt></td>
    <td>Bell</td>
  </tr>
  <tr>
    <td><tt>\e</tt></td>
    <td>Escape (ASCII escape character)</td>
  </tr>
  <tr>
    <td><tt>\007</tt></td>
    <td>Any octal ASCII value (here, <tt>007</tt> = bell)</td>
  </tr>
  <tr>
    <td><tt>\x7f</tt></td>
    <td>Any two-digit, hex ASCII value (here, <tt>7f</tt> = delete)</td>
  </tr>
  <tr>
    <td><tt>\x{2744}</tt></td>
    <td>Any hex Unicode code point (here, <tt>U+2744</tt> = snowflake)</td>
  </tr>
  <tr>
    <td><tt>\N{CHARACTER NAME}</tt></td>
    <td>Any Unicode code point, by name</td>
  </tr>
  <tr>
    <td><tt>\cC</tt></td>
    <td>A&nbsp;&ldquo;control&rdquo; character (here, Ctrl-C)</td>
  </tr>
  <tr>
    <td><tt>\\\\</tt></td>
    <td>Backslash</td>
  </tr>
  <tr>
    <td><tt>\\"</tt></td>
    <td>Double quote</td>
  </tr>
  <tr>
    <td><tt>\l</tt></td>
    <td>Lower case next letter</td>
  </tr>
  <tr>
    <td><tt>\L</tt></td>
    <td>Lower case all following letters until <tt>\E</tt></td>
  </tr>
  <tr>
    <td><tt>\u</tt></td>
    <td>Upper case next letter</td>
  </tr>
  <tr>
    <td><tt>\U</tt></td>
    <td>Upper case all following letters until <tt>\E</tt></td>
  </tr>
  <tr>
    <td><tt>\Q</tt></td>
    <td>Quote nonword characters by adding a&nbsp;backslash until <tt>\E</tt></td>
  </tr>
  <tr>
    <td><tt>\E</tt></td>
    <td>End <tt>\L</tt>, <tt>\U</tt> or <tt>\Q</tt></td>
  </tr>
</table>

Double-quoted strings are variable interpolated &rArr; Some variable names within the string are replaced with their current values.

<h5>String Operators</h5>

Use the <tt>.</tt> operator to concatenate or join strings:

{% highlight perl %}
"Piece to " . "the World!"       # -> same as "Piece to the World!"
"Piece to" . " " . "the World!"  # -> same as 'Piece to the World!'
'Piece to the World!' . "\n"     # -> same as "Piece to the World!\n"
{% endhighlight %}

For string repetition operator use lower case letter <tt>x</tt>:

{% highlight perl %}
"Fred " x 3        # -> "Fred Fred Fred "
"Barney " x (4+1)  # -> "Barney Barney Barney Barney Barney "
5 x 4.8            # -> "5555", 4.8 is truncated to 4
{% endhighlight %}

<h5>Automatic Conversion Between Numbers and Strings</h5>

If an&nbsp;operator expects a&nbsp;number &rarr; Perl will see the value as a&nbsp;number.

If an&nbsp;operator expects a&nbsp;string &rarr; Perl will see the value as a&nbsp;string.

<h4>Perl's Built-In Warnings</h4>

Turn on warnings with the <tt>warnings</tt> pragma:

{% highlight perl %}
#!/usr/bin/perl
use warnings;
{% endhighlight %}

To see warnings in other people's code (in modules you didn't write):

{% highlight perl %}
perl -w <script_name>
{% endhighlight %}

Alternatively, specify command-line switches on the sheband line:

{% highlight perl %}
#!/usr/bin/perl -w
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;The <tt>warnings</tt> pragma turns on warnings for the file in which you use the pragma, whereas <tt>-w</tt> turns them on for the entire program.

To get a&nbsp;longer description of the&nbsp;problem use the <tt>diagnostics</tt> pragma:

{% highlight perl %}
use diagnostics;
{% endhighlight %}

To see diagnostics for a&nbsp;single program execution use:

{% highlight perl %}
perl -Mdiagnostics ./<script_name>
{% endhighlight %}

<h5>Interpreting Nondecimal Numerals</h5>

Apply <tt>hex</tt> and <tt>oct</tt> functions to convert a&nbsp;string representation of a&nbsp;number to a&nbsp;decimal base:

{% highlight perl %}
hex('DEADBEEF')    # 3_735_928_559 decimal
hex('0xDEADBEEF')  # 3_735_928_559 decimal

oct('0377')        # 255 decimal
oct('377')         # 255 decimal
oct('0xDEADBEEF')  # 3_735_928_559 decimal, saw leading 0x
oct('0b1101')      # 13 decimal, saw leading 0b
oct("0b$bits")     # convert $bits from binary
{% endhighlight %}

<h4>Scalar Variables</h4>

A&nbsp;variable is a&nbsp;name for a&nbsp;container that holds one or more values.

A&nbsp;scalar variable holds a&nbsp;single scalar value.

Scalar variable names begin with a&nbsp;dollar sign <tt>$</tt>, called the&nbsp;<i>sigil</i>, followed by a&nbsp;Perl <i>identifier</i>: a&nbsp;letter or underscore, and more letters, digits or underscores.

Perl variable names are case sensitive:

{% highlight perl %}
$pi
$Pi
$PI

$speed_of_light
$electric_permittivity
$magnetic_permeability
$ElectricConductivity
{% endhighlight %}

Enable <tt>utf8</tt> pragma to use Unicode characters in variable names:

{% highlight perl %}
$résumé
$coördinate
{% endhighlight %}

<i class="fas fa-exclamation-circle"></i>&nbsp;The sigil isn't telling you the variable type; it's telling you how you are accessing that variable.

<h5>Choosing Good Variable Names</h5>

Give preference to descriptive variable names:

{% highlight perl %}
$line_length  # vs $r
$stop_id      # vs $stopid
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;The <tt>perlvar</tt> documentation lists all of the Perl's special variable names, and <tt>perlstyle</tt> has general programming style advice.

<h5>Scalar Assignment</h5>

Use the equals sign <tt>(=)</tt> for assignment:

{% highlight perl %}
$freds_age      = 37;
$barneys_phrase ='Yabba!';
$barneys_age    = $freds_age + 5;
$dinos_age      = $freds_age / 10;
{% endhighlight %}

<h5>Compound Assignment Operators</h5>

Compound assignment in Perl is similar to C and Java:

{% highlight perl %}
$squats  = 10;
$squats +=  1;     # equivalent to $squats = $squats + 1;

$press_ups  = 10;
$press_ups *=  3;  # equivalent to $press_ups = $press_ups * 3;

$E   = 2.718281828459045;
$E **= 2;          # equivalent to $E = $E ** 2;
{% endhighlight %}

Compound assignment with a&nbsp;string concatenation operator:

{% highlight perl %}
$phrase  = 'Don\'t crack';
$phrase .= ' under pressure';
{% endhighlight %}

<h4>Output with print</h4>

To print out type:

{% highlight perl %}
print "There is no spoon...";   # no newline
print "Peace to the World!\n";  # explicit newline

use v5.10;
say "There is no spoon...";     # automatic newline
{% endhighlight %}

Print out a series of values separated by commas:

{% highlight perl %}
print "Area of circle: ", $PI*$r**2, "\n";
{% endhighlight %}

<h5>Interpolation of Scalar Variables into Strings</h5>

Double-quoted string enables <i>variable interpolation</i>:

{% highlight perl %}
$meal   = 'brontosaurus steak';
$dinner = "For dinner Fred ate a $meal";

$area = 3.17;
print "Area: $area";  # equivalent to print "Area: " . $area;
{% endhighlight %}

There is no need to interpolate for one lone variable:

{% highlight perl %}
print $area;  # instead of print "$area";
{% endhighlight %}

Print out a&nbsp;literal dollar sign with a&nbsp;backslash character:

{% highlight perl %}
print "Total cost: \$256.00";
{% endhighlight %}

<h5>Creating Characters by Code Point</h5>
<h5>Operator Precedence and Associativity</h5>
<h5>Comparison Operators</h5>

<h4>The if Control Structure</h4>
<h5>Boolean Values</h5>

<h4>Getting User Input</h4>
<h4>The chomp Operator</h4>
<h4>The while Control Structure</h4>
<h4>The undef Value</h4>
<h4>The defined Function</h4>

<h3 id="lists">3.&nbsp;Lists and Arrays</h3>
<h3 id="subroutines">4.&nbsp;Subroutines</h3>

<h3 id="io">5.&nbsp;Input and Output</h3>
<h4>Input from Standard Input</h4>

Obtain next line of input in scalar context:

{% highlight perl %}
$line = <STDIN>;  # read next line
chomp($line);     # chomp it (remove newline character)

# idiomatic Perl
chomp($line = <STDIN>);
{% endhighlight %}

Line-input operator <tt><></tt> returns <tt>undef</tt> when it reaches end-of-file. Use this to drop out of loops:

{% highlight perl %}
while (defined($line = <STDIN>)) {
    print "Line: $line";
}

# idiomatic Perl
# works only if there is nothing but line-input operator in conditional
while (<STDIN>) {
    print "Line: $_";
}
{% endhighlight %}

Line-input operator in list context results in all of remaining lines of input as list:

{% highlight perl %}
foreach (<STDIN>) {
    print "Line: $_";
}
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;It is best to use line-input operator in a&nbsp;scalar context to read input line-by-line. In list context Perl fetches all input at once.

<h4>Input from the Diamond Operator</h4>

Diamond operator is a&nbsp;special kind of line-input operator. Input can come from the user's choice, not only from the keyboard.

{% highlight perl %}
while (defined($line = <>)) {
    chomp($line);
    print "Line: $line\n";
}

# idiomatic Perl
while (<>) {
    chomp;  # by default chomp works on $_
    print "Line: $line\n";
}
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Current file name is kept in Perl's special variable <tt>$ARGV</tt>. This name is &ldquo;-&rdquo; if input comes from the standard input stream <tt>STDIN</tt>.

<h4>The Double Diamond</h4>

Double diamond operator supports special characters in the filename, e.g. <tt>|</tt>. Using double diamond will avoid performing a&nbsp;"pipe open" and running an&nbsp;external program.

{% highlight perl %}
use v5.22;

while (<<>>) {
    chomp;
    print "Line: $_";
}
{% endhighlight %}

<h4>The Invocation Arguments</h4>

Perl stores invocation arguments in a&nbsp;special array <tt>@ARGV</tt>.

Use it as any other array to:

<ul>
  <li><tt>shift</tt> items,</li>
  <li>iterate over it with <tt>foreach</tt>,</li>
  <li>check if any arguments start with a&nbsp;hyphen.</li>
</ul>

<i class="fas fa-bell"></i>&nbsp;Use modules <tt>Getopt::Long</tt> and <tt>Getopt::Std</tt> to process options in a&nbsp;standard way.

Tinker with the&nbsp;array <tt>@ARGV</tt> after the program start and before the diamond <tt><></tt> invocation:

{% highlight perl %}
@ARGV = qw# larry moe curly #;  # force these three files to be read
while (<>) {
    chomp;
    print "Line: $_\n";
}
{% endhighlight %}

<h4>Output to Standard Output</h4>

Printing array results in a list of items, with no spaces in between:

{% highlight perl %}
my @array = qw/ fred barney betty /;
print @array;  # -> fredbarneybetty
{% endhighlight %}

Interpolating array prints contents of an&nbsp;array separated by spaces:

{% highlight perl %}
print "@array";  # -> fred barney betty
{% endhighlight %}

Default separator is a&nbsp;space character. The separator is stored in a&nbsp;special variable called <tt>$"</tt>.

Other examples include:

{% highlight perl %}
# implementation of /bin/cat
print <>;

# implementation of /bin/sort
print sort <>;
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;<a href="https://perlpowertools.com">Perl power tools project</a> implements all classic Unix utilities in Perl => Makes these standard utilities available on non-Unix systems&nbsp;{% cite PerlPowerTools23 --file references %}.

<h4>Formatted Output with <tt>printf</tt></h4>

Template string in <tt>printf</tt> holds multiple <i>conversions</i> (percent sign and letter):

{% highlight perl %}
printf "Hello, %s! Your password expires in %d days!\n",
    $username, $days_to_exp;
{% endhighlight %}

Common conversions are:

<table>
  <tr> <td><tt>%g</tt></td> <td>number in floating point, integer or exponential notation (chosen automatically)</td> </tr>
  <tr> <td><tt>%d</tt></td> <td>decimal integer</td> </tr>
  <tr> <td><tt>%x</tt></td> <td>hexadecimal</td> </tr>
  <tr> <td><tt>%o</tt></td> <td>octal</td> </tr>
  <tr> <td><tt>%s</tt></td> <td>string</td> </tr>
  <tr> <td><tt>%f</tt></td> <td>floating-point with round off</td> </tr>
  <tr> <td><tt>%%</tt></td> <td>literal percent sign</td> </tr>
</table>

Automatic choice of floating point, integer and exponential notation:

{% highlight perl %}
printf "%g %g %g\n", 5/2, 51/17, 51**17;  # 2.5 3 1.0683e+29
{% endhighlight %}

Decimal integer conversion truncates the number:

{% highlight perl %}
printf "Pass expires in %d days!\n", 17.85;  # ...in 17 days!
{% endhighlight %}

Hexadecimal and octal conversions are:

{% highlight perl %}
printf "Hex: %h\n", 17;  # Hex: 0x11
printf "Oct: %o\n", 17;  # Oct: 021
{% endhighlight %}

String conversion with width specification is:

{% highlight perl %}
# positive width = right-justified string
printf "Surname: %10s\n", $surname;  # Surname: ```Nabokov;

# negative width = left-justified string
printf "Surname: %-10s\n", $surname;  # Surname: Nabokov```;
{% endhighlight %}

Floating-point conversion with round off is:

{% highlight perl %}
# positive width = right-justified number
printf "12f\n",   6*7 + 2/3;  # ```42.666667
printf "12.3f\n", 6*7 + 2/3;  # ``````42.667
printf "12.0f\n", 6*7 + 2/3;  # ``````````43
{% endhighlight %}

Asterisk * inside format string uses its argument as width:

{% highlight perl %}
printf "%*s\n", 10, Nabokov;  # ```Nabokov
{% endhighlight %}

Use two asterisks to specify total width and number of decimal places in a&nbsp;float:

{% highlight perl %}
printf "%*.*f\n", 6, 2, 3.1415926535;  # ```3.14
printf "%*.*f\n", 6, 3, 3.1415926535;  # ``3.142
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;See <a href="https://perldoc.perl.org/functions/sprintf"><tt>sprintf</tt></a> documentation for more options.

<h4>Arrays and <tt>printf</tt></h4>

To generate a&nbsp;format string on the fly store it in a&nbsp;variable first:

{% highlight perl %}
my @items = qw( wilma dino pebbles );

# use x operator to replicate given string a number of times
# @items in scalar context returns array length
my $fmt   = "The items are:\n" . ("%10s\n" x @items);

print "(debug) Format: $fmt";
printf $fmt, @items;
{% endhighlight %}

<h4>Filehandles</h4>

Filehandle names an&nbsp;I/O connection between a&nbsp;Perl process and the outside world.

<i class="fas fa-bell"></i>&nbsp;Filehandle <i>is not</i> necessarily a&nbsp;filename.

Perl recommends to use all uppercase letters in the name of a&nbsp;filehandle.

Six special filehandle names reserved by Perl are:

<ul>
  <li>STDIN</li>
  <li>STDOUT</li>
  <li>STDERR</li>
  <li>DATA</li>
  <li>ARGV</li>
  <li>ARGVOUT</li>
</ul>

If a&nbsp;user calls a&nbsp;Perl script as:

{% highlight bash %}
$ ./process.pl <in.txt >out.txt 2>err.txt
{% endhighlight %}

Inside Perl script files '<tt>in.txt</tt>', '<tt>out.txt</tt>' and '<tt>err.txt</tt>' will be available as <tt>STDIN</tt>, <tt>STDOUT</tt> and <tt>STDERR</tt> filehandles.

<h4>Opening a&nbsp;Filehandle</h4>

Use Perl's <tt>open</tt> operator to create a&nbsp;custom connection:

{% highlight perl %}
open CONFIG,  '.ssh/config';
open CONFIG,  '<.git/config';
open RESULTS, '>results.txt';  # wipes out file contents
open LOG,     '>>make.log';    # opens file for appending
{% endhighlight %}

Three-argument version of <tt>open</tt> is a&nbsp;safer option:

{% highlight perl %}
open CONFIG,  '<', '.git/config';
open RESULTS, '>',  $fh_results;
open LOG,     '>>', &logfile_name();
{% endhighlight %}

Specify an&nbsp;encoding along with the mode:

{% highlight perl %}
open CONFIG, '<:encoding(UTF-8)',      '.git/config';     # UTF 8
open LOG,    '>>:encoding(iso-8859-1)', &logfile_name();  # Latin-1
{% endhighlight %}

<h4>Binmoding Filehandles</h4>

Turn off processing of line ending:

{% highlight perl %}
# don't translate line endings
binmode STDOUT;
binmode STDERR;
{% endhighlight %}

Specify a&nbsp;<i>layer</i> to ensure filehandles know about intended encodings:

{% highlight perl %}
binmode STDOUT, ':encoding(UTF-8)';
binmode STDIN,  ':encoding(KOI8-R)';
{% endhighlight %}

<h4>Bad Filehandles</h4>

Operator <tt>open</tt> returns <i>true</i>, if it succeeded or <i>false</i> otherwise:

{% highlight perl %}
# capture return value
my $status = open LOG, '>>', 'configure.log';

if (! $status ) {
    # open failed, clean up and recover
    ...
}
{% endhighlight %}

<h4>Closing a&nbsp;Filehandle</h4>

{% highlight perl %}
close LOG;
{% endhighlight %}

<h4>Fatal Errors with <tt>die</tt></h4>

Operator <tt>die</tt> terminates the&nbsp;program:

{% highlight perl %}
if (! open LOG, '>>', 'logfile') {
    die "Cannot create logfile: $!";
}
{% endhighlight %}

Special variable <tt>$!</tt> contains a&nbsp;human-readable error message.

<tt>die</tt> automatically appends program name and line number, where it failed.

<i class="fas fa-bell"></i>&nbsp;Always check the&nbsp;status of <tt>open</tt> => The&nbsp;rest of the&nbsp;program relies upon it.

<h4>Warning Messages with <tt>warn</tt></h4>

Use <tt>warn</tt> function to issue a&nbsp;warning and proceed with the&nbsp;code.

<h4>Automatically die-ing</h4>

Pragma <tt>autodie</tt> automatically calls <tt>die</tt>, if a&nbsp;file <tt>open</tt> fails:

{% highlight perl %}
use autodie;

open LOG, '>>', 'logfile';

# Potential error message:
#   Can't open ('>>', 'logfile'): No such file or directory at test line 3
{% endhighlight %}

<h4>Using Filehandles</h4>

Open a&nbsp;filehandle and read from it:

{% highlight perl %}
if ( ! open PASSWD, "/etc/passwd") {
    die "Unable to open passwords file: $!";
}

# line-input operator <> around filehandle
while (<PASSWD>) {
    chomp;
    ...
}
{% endhighlight %}

Use filehandle open for writing or appending:

{% highlight perl %}
# output into LOG
print LOG "Captain's log, stardate 3.14159\n";

# output into STDERR
printf STDERR "%d percent complete.\n", $done/$total * 100;
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;There is no comma between a&nbsp;filehandle and values to print.

<h4>Changing the Default Output Filehandle</h4>

Functions <tt>print</tt> and <tt>printf</tt> output into <tt>STDOUT</tt>. Operator <tt>select</tt> changes the default behaviour:

{% highlight perl %}
select BEDROCK;

print "I hope Mr. Slate doesn't find out about this.\n";
print "Wilma!\n";
{% endhighlight %}

Gentlemen set it back to <tt>STDOUT</tt> when they are done:

{% highlight perl %}
select LOG;
$| = 1;  # disable output buffering
select STDOUT;
# ... time passes, babies are born, Universe expands, entropy grows and then...
print LOG "This gets written to the LOG at once!\n";
{% endhighlight %}

<h4>Reopening a&nbsp;Standard Filehandle</h4>

Send errors to a&nbsp;custom error log:

{% highlight perl %}
if ( ! open STDERR, ">>/home/mabalenk/.error_log" ) {
    die "Can't open error log for append: $!";
}
{% endhighlight %}

<h4>Output with <tt>say</tt></h4>

Built-in <tt>say</tt> is the same as <tt>print</tt>, but it puts a&nbsp;newline at the end:

{% highlight perl %}
use v5.10;

# all these forms produce the same output
print "Hello!\n";      # -> "Hello!\n"
print "Hello!", "\n";  # -/-
say   "Hello!";        # -/-
{% endhighlight %}

To interpolate an&nbsp;array quote it:

{% highlight perl %}
use v5.10;

my   @arr = qw( a b c d );
say  @arr;   # -> "abcd\n"
say "@arr";  # -> "a b c d\n"
{% endhighlight %}

Specify a&nbsp;filehandle with <tt>say</tt>:

{% highlight perl %}
use v5.10;

# note: no comma after filehandle name
say ROBOTLOG "Klatu barada nikto!";
{% endhighlight %}

<h4>Filehandles in a&nbsp;Scalar</h4>

Use scalar in place of a&nbsp;bareword:

{% highlight perl %}
my $fh_params;
open $fh_params, '<', 'sim_params.txt'
    or die "Could not open $fh_params: $!";
{% endhighlight %}

Combine two statements (declaring and opening):

{% highlight perl %}
open my $fh_params, '<', 'sim_params.txt'
    or die "Could not open $fh_params: $!";
{% endhighlight %}

Once filehandle is in a&nbsp;scalar, use it similarly to a&nbsp;bareword:

{% highlight perl %}
open my $fh_params, '>', 'sim_params.txt';

# note: no comma after filehandle name
say     $fh_params  "C_0: " . (2.99*10**8);
close   $fh_params;

open    $fh_params, '<', 'sim_params.txt';
while (<$fh_params>) {
    chomp;
    ...
}
close   $fh_params;
{% endhighlight %}


<h3 id="hashes">6.&nbsp;Hashes</h3>

<h4>What Is a&nbsp;Hash?</h4>

Hash is a&nbsp;data structure. It allows to look up hash values by name. Hash indices are called <i>keys</i>. They aren't numbers, but arbitrary, unique strings.

There is neither a&nbsp;fixed order in a&nbsp;hash, nor a&nbsp;first element.

Hash is a&nbsp;collection of key-value pairs.

Hash keys are always unique. They are converted to strings. Same value can be stored more than once.

<h4>Why Use a&nbsp;Hash?</h4>

Use hash when one set of data &ldquo;is related&rdquo; to another set of data.

<h4>Hash Element Access</h4>

To access an&nbsp;element of a&nbsp;hash use this syntax:

{% highlight perl %}
$hash{$some_key}
{% endhighlight %}

Use curly braces instead of square brackets around the key. Key expression is now a&nbsp;string, not a&nbsp;number:

{% highlight perl %}
$family_name{'fred'}   = 'flinstone';
$family_name{'barney'} = 'rubble';

# iterate over hash values
foreach my $person (qw< barney fred >) {
    print "I've heard of $person $family_name{$person}.\n";
}
{% endhighlight %}

When choosing a&nbsp;hash name, think of the word &ldquo;for&rdquo; between the&nbsp;name of the&nbsp;hash and the&nbsp;key.

Hash elements spring into existence when you first assign to them:

{% highlight perl %}
$family_name{'wilma'}  = 'flinstone';             # adds new key-value pair
$family_name{'betty'} .= $family_name{'barney'};  # creates an element if needed
{% endhighlight %}

This feature is called <i>autovivification</i>.

Accessing an&nbsp;element outside a&nbsp;hash returns <tt>undef</tt>.

{% highlight perl %}
$granite = $family_name{'larry'};  # no larry here => undef
{% endhighlight %}

<h4>The Hash as a&nbsp;Whole</h4>

Refer to the&nbsp;entire hash with a&nbsp;percent sign (%) as a&nbsp;prefix.

It is possible to convert a&nbsp;hash into a&nbsp;list and back again.

Assigning to a&nbsp;hash is a&nbsp;list-context assignment (list is key-value pairs):

{% highlight perl %}
%some_hash = ('foo', 35, 'bar', 12.4, 2.5, 'hello',
    'wilma', 1.72e30, 'betty', "bye\n");
{% endhighlight %}

Value of hash in a&nbsp;list context is a&nbsp;simple list of key-value pairs:

{% highlight perl %}
@any_array = %some_hash;
{% endhighlight %}

Turning the hash back into a&nbsp;list of key-value pairs is called <i>unwinding</i> in Perl.

<i class="fas fa-bell"></i>&nbsp;Use a&nbsp;hash either when items order is not important or when you have an&nbsp;easy way to control their order.

But key-value pairs in a&nbsp;hash stay together, i.e. value will follow its key.

<h4>Hash Assignment</h4>

To copy a&nbsp;hash simply assign one hash to another:

{% highlight perl %}
my %new_hash = %old_hash;
{% endhighlight %}

This is a&nbsp;computationally expensive operation in Perl: first an&nbsp;<tt>%old_hash</tt> is unwound into a&nbsp;list, then it is assigned to a&nbsp;<tt>%new_hash</tt> one key-value pair at a&nbsp;time.

To inverse a&nbsp;hash write:

{% highlight perl %}
# swap keys with values
my %ip_address = reverse %host_name;
{% endhighlight %}

Perl uses the rule: &ldquo;last one wins&rdquo;. Later items in the list overwrite earlier ones.

<h4>The Big Arrow</h4>

In Perl grammar any comma <tt>,</tt> can be written as a big arrow <tt>=></tt> (fat comma).

Alternative way to set up a&nbsp;hash of last names is:

{% highlight perl %}
# easier to see keys and corresponding values
my %last_name = (
    'fred'   => 'flinstone',
    'dino'   =>  undef,
    'barney' => 'rubble',
    'betty'  => 'rubble',  # extra comma is harmless, but convenient
);
{% endhighlight %}

Perl shortcut: it's possible to omit the quote marks on <i>some</i> hash keys, when you use a fat comma:

{% highlight perl %}
# omit quotes on _bareword_ keys
my %last_name = (
    fred   => 'flinstone',
    dino   =>  undef,
    barney => 'rubble',
    betty  => 'rubble',
);
{% endhighlight %}

Use this shortcut in curly braces of a&nbsp;hash element reference:

{% highlight perl %}
$score{fred}  # instead of $score{'fred'}
{% endhighlight %}

<h4>Hash Functions</h4>
<h4>The <tt>keys</tt> and <tt>values</tt> Functions</h4>

The <tt>keys</tt> function yields a&nbsp;list of all keys in a&nbsp;hash, <tt>values</tt> function returns the corresponding values:

{% highlight perl %}
my %hash = (
    'a' => 1,
    'b' => 2,
    'c' => 3,
);
my @k = keys   %hash;
my @v = values %hash;
{% endhighlight %}

Use these functions in a&nbsp;scalar context to retrieve the number of elements in a&nbsp;hash:

{% highlight perl %}
my $count = keys %hash;  # returns 3 => hash contains three key-value pairs
{% endhighlight %}

Use hash in a&nbsp;Boolean context to find out, if a&nbsp;hash is not empty:

{% highlight perl %}
# if hash is not empty
if (%hash) {
    print "True!\n";
}
{% endhighlight %}

<h4>The <tt>each</tt> function</h4>

The <tt>each</tt> function returns a&nbsp;key-value pair as a&nbsp;two element list. It is a&nbsp;common way to iterate over a&nbsp;hash. Use this function in a&nbsp;while loop:

{% highlight perl %}
while ( ($key, $value) = each %hash ) {
    print "$key => $value\n";
}
{% endhighlight %}

To go through the hash in order, sort the keys:

{% highlight perl %}
foreach $key (sort keys %hash) {
    $value = $hash{$key};
    print "$key => $value\n";

    # shorter alternative
    print "$key => $hash{$key}\n";
}
{% endhighlight %}

<h4>Typical Use of a&nbsp;Hash</h4>

Library database to keep track of how many books each person has checked out is a&nbsp;good example of a&nbsp;hash use:

{% highlight perl %}
$books{'fred'}  = 3;
$books{'wilma'} = 1;
{% endhighlight %}

See, whether an&nbsp;element of the&nbsp;hash is true or false:

{% highlight perl %}
if ($books{$someone}) {
    print "$someone has at least one book checked out.\n";
}

$books{'barney'}  = 0;      # no books currently checked out
$books{'pebbles'} = undef;  # no books ever checked out; new library card
{% endhighlight %}

<h4>The <tt>exists</tt> Function</h4>

The <tt>exists</tt> function returns true, <i>iff</i> the given key exists in the hash:

{% highlight perl %}
if (exists $books{'dino'}) {
    print "There is a library card for dino!\n";
}
{% endhighlight %}

<h4>The <tt>delete</tt> Function</h4>

The <tt>delete</tt> function removes the given key-value pair from the hash:

{% highlight perl %}
my $person = 'betty';
delete $books{$person};  # revoke library card for $person
{% endhighlight %}

<h4>Hash Element Interpolation</h4>

Single hash element can be interpolated into a&nbsp;double-quoted string:

{% highlight perl %}
# for each patron, in order
foreach $person (sort keys %books) {
    if ($books{$person}) {
        print "$person has $books{$person} items\n";  # fred has 3 items
    }
}
{% endhighlight %}

There is no support for entire hash interpolation, e.g. <tt>%books</tt>. Beware of the magical characters ($, @, ", \, ', [, {, ->, ::) that need back-slashing.

<h4>The <tt>%ENV</tt> Hash</h4>

Perl program <i>environment</i> is stored in the <tt>%ENV</tt> hash:

{% highlight perl %}
print "PATH is $ENV{'PATH'}\n";
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;In Perl, <i>dollar</i> sign '<tt>$</tt>' means there is one of something, <i>at</i> sign '<tt>@</tt>' means there is a&nbsp;list of something and <i>percent</i> sign '<tt>%</tt>' means there is an&nbsp;entire hash.


<h3 id="regexp">7.&nbsp;Regular Expressions</h3>

<i class="fas fa-book"></i>&nbsp;<a href="http://regex.info/book.html">Mastering Regular Expressions</a> (3rd, 2006, <a href="https://www.oreilly.com/library/view/mastering-regular-expressions/0596528124">O'Reilly Media</a>) by <a href="http://regex.info/blog">Jeffrey Friedl</a>&nbsp;{% cite Friedl06 --file references %}<br>
<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="http://www.learning-perl.com/2016/06/watch-regexes-with-regexpdebugger">Watch regexes with Regexp::Debugger</a><br>
<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="https://www.effectiveperlprogramming.com/2011/01/know-your-character-classes">Know your character classes under different semantics</a>

<table class="book">
  <caption>Table&nbsp;7-1. Regular expression quantifiers and their generalised forms</caption>
  <tr>
    <th>Number to match</th>
    <th>Metacharacter</th>
    <th>Generalised form</th>
  </tr>
  <tr>
    <td>Optional</td>
    <td>?</td>
    <td>{0,1}</td>
  </tr>
  <tr>
    <td>Zero or more</td>
    <td>*</td>
    <td>{0,}</td>
  </tr>
  <tr>
    <td>One or more</td>
    <td>+</td>
    <td>{1,}</td>
  </tr>
  <tr>
    <td>Minimum with no maximum</td>
    <td></td>
    <td>{3,}</td>
  </tr>
  <tr>
    <td>Minimum with maximum</td>
    <td></td>
    <td>{3,5}</td>
  </tr>
  <tr>
    <td>Exactly</td>
    <td></td>
    <td>{3}</td>
  </tr>
</table>

<table class="book">
  <caption>Table&nbsp;7-2. ASCII character class shortcuts</caption>
  <tr>
    <th>Shortcut</th>
    <th>Matches</th>
    <th>Note</th>
  </tr>
  <tr>
    <td><tt>\d</tt></td>
    <td>decimal digit</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\D</tt></td>
    <td>not a decimal digit</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\s</tt></td>
    <td>whitespace</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\S</tt></td>
    <td>not whitespace</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\h</tt></td>
    <td>horizontal whitespace</td>
    <td>(v5.10 and later)</td>
  </tr>
  <tr>
    <td><tt>\H</tt></td>
    <td>not horizontal whitespace</td>
    <td>(v5.10 and later)</td>
  </tr>
  <tr>
    <td><tt>\v</tt></td>
    <td>vertical whitespace</td>
    <td>(v5.10 and later)</td>
  </tr>
  <tr>
    <td><tt>\V</tt></td>
    <td>not vertical whitespace</td>
    <td>(v5.10 and later)</td>
  </tr>
  <tr>
    <td><tt>\R</tt></td>
    <td>generalised line ending</td>
    <td>(v5.10 and later)</td>
  </tr>
  <tr>
    <td><tt>\w</tt></td>
    <td>&ldquo;word&rdquo; character</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\W</tt></td>
    <td>not a &ldquo;word&rdquo; character</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><tt>\n</tt></td>
    <td>newline</td>
    <td>(not really a shortcut)</td>
  </tr>
  <tr>
    <td><tt>\N</tt></td>
    <td>non-newline</td>
    <td>(stable in v5.18)</td>
  </tr>
</table>

<h3 id="match_regexp">8.&nbsp;Matching with Regular Expressions</h3>

<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="http://unicode.org/Public/UNIDATA/CaseFolding.txt">Unicode's case folding rules</a>

<table class="book">
  <caption>Table&nbsp;8-0. Matching regular expressions</caption>
  <tr>
    <th>Expression</th><th>Note</th>
  </tr>
  <tr>
    <td><tt>m/pattern/s</tt></td>
    <td>match any character, even newline</td>
  </tr>
  <tr>
    <td><tt>m/pattern/i</tt></td>
    <td>case insensitive matching</td>
  </tr>
  <tr>
    <td><tt>m/pattern/x</tt></td>
    <td>make whitespace inside pattern insignificant</td>
  </tr>
  <tr>
    <td><tt>m/pattern/m</tt></td>
    <td>multiline matching</td>
  </tr>
    <td><tt>m/\Apattern/</tt></td>
    <td>beginning of line</td>
  <tr>
    <td><tt>m/pattern\Z/</tt></td>
    <td>end of line</td>
  </tr>
  <tr>
    <td><tt>m/\b{wb}/</tt></td>
    <td>word boundary</td>
  </tr>
  <tr>
    <td><tt>m/\b{sb}/</tt></td>
    <td>sentence boundary</td>
  </tr>
  <tr>
    <td><tt>m/\b{lb}/</tt></td>
    <td>line boundary</td>
  </tr>
  <tr>
    <td><tt>m/(pat)tern/</tt></td>
    <td>capture group between <tt>()</tt>, available as <tt>$1</tt></td>
  </tr>
  <tr>
    <td><tt>m/(pat)tern \1/</tt></td>
    <td>capture group, reuse capture group in matching expression</td>
  </tr>
  <tr>
    <td><tt>m/(?:pat)tern/</tt></td>
    <td>non-capturing parentheses, do not save result into <tt>$1</tt></td>
  </tr>
  <tr>
    <td><tt>m/(?&lt;LABEL&gt;pattern)/</tt></td>
    <td>named capture, stored in <tt>%+</tt>, available as <tt>$+{LABEL}</tt></td>
  </tr>
  <tr>
    <td><tt>m/(?&lt;LABEL&gt;pattern \g&lt;LABEL&gt;)/</tt></td>
    <td>named capture, reuse named capture group in matching expression</td>
  </tr>
  <tr>
    <td><tt>'Brave new world!'=~ m/new/</tt></td>
    <td>automatic match variables <tt>$&</tt> Brave, <tt>$`</tt> new, <tt>$'</tt> world!</td>
  </tr>
</table>

<table class="book">
  <caption>Table&nbsp;8-1. Regular expression precedence</caption>
  <tr>
    <th>Regular expression feature</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>Parentheses (grouping | capturing)</td>
    <td><tt>(...), (?:...), (?&lt;LABEL&gt;...)</tt></td>
  </tr>
  <tr>
    <td>Quantifiers</td>
    <td><tt>a* a+ a? a{n,m}</tt></td>
  </tr>
  <tr>
    <td>Anchors and sequence</td>
    <td><tt>abc ^ $ \A \b \z \Z</tt></td>
  </tr>
  <tr>
    <td>Alternation</td>
    <td><tt>a|b|c</tt></td>
  </tr>
  <tr>
    <td>Atoms</td>
    <td><tt>a [abc] \d \1 \g{2}</tt></td>
  </tr>
</table>

<h4>Pattern test program</h4>

{% highlight perl %}
while (<>) {
  chomp;
  if (/YOUR_PATTERN_GOES_HERE/) {
    print "Matched: |$`<$&>$'|\n";
  }
  else {
    print "No match: |$_|\n";
  }
}{% endhighlight %}

<h3 id="proc_regexp">9.&nbsp;Processing Text with Regular Expressions</h3>

<table class="book">
  <caption>Table&nbsp;9-0. Processing regular expressions</caption>
  <tr>
    <th>Expression</th>
    <th>Note</th>
  </tr>
  <tr>
    <td><tt>s/before/after/</tt></td>
    <td>substitution</td>
  </tr>
  <tr>
    <td><tt>s/before/after/g</tt></td>
    <td>global replacement</td>
  </tr>
  <tr>
    <td><tt>s/pattern/\U$1</tt></td>
    <td>turn pattern to upper case with <tt>\U</tt> # PATTERN</td>
  </tr>
  <tr>
    <td><tt>s/pattern/\L$1</tt></td>
    <td>turn pattern to lower case with <tt>\L</tt> # pattern</td>
  </tr>
  <tr>
    <td><tt>s/pattern/\u\L$1</tt></td>
    <td>turn pattern to title case with <tt>\u\L</tt> # Pattern</td>
  </tr>
  <tr>
    <td><tt>s/pattern/\l\U$1</tt></td>
    <td>turn pattern to inverse title case with <tt>\l\U</tt> # pATTERN</td>
  </tr>
  <tr>
    <td><tt>lc, uc, fc, lcfirst, ucfirst</tt></td>
    <td>case shifting functions</td>
  </tr>
  <tr>
    <td><tt>my @fields = split /separator/, $string;</tt></td>
    <td>break up a&nbsp;string according to a&nbsp;pattern</td>
  </tr>
  <tr>
    <td><tt>my $result = join $glue, @pieces;</tt></td>
    <td>glues together a&nbsp;bunch of pieces to make a&nbsp;single string</td>
  </tr>
</table>

<i class="fas fa-external-link-square-alt"></i>&nbsp;Tom Christiansen on <a href="http://stackoverflow.com/a/4234491/2766176">parsing HTML with regular expressions</a>

<table class="book">
  <caption>Table&nbsp;9-1. Regular expression quantifiers with the non-greedy modifier</caption>
  <tr>
    <th>Number to match</th>
    <th>Metacharacter</th>
  </tr>
  <tr>
    <td>??</td>
    <td>Zero matches (useless)</td>
  </tr>
  <tr>
    <td>*?</td>
    <td>Zero or more, as few as possible</td>
  </tr>
  <tr>
    <td>+?</td>
    <td>One or more, as few as possible</td>
  </tr>
  <tr>
    <td>{3,}?</td>
    <td>At least three, but as few as possible</td>
  </tr>
  <tr>
    <td>{3,5}?</td>
    <td>At least three, as many as five, but as few as possible</td>
  </tr>
  <tr>
    <td>{3}?</td>
    <td>Exactly three</td>
  </tr>
</table>

<i class="fas fa-external-link-square-alt"></i>&nbsp;Levels of regular expression compliance in <a href="http://unicode.org/reports/tr18/tr18-5.1.html">Unicode Technical Report #18</a>

<h4>Updating many files</h4>

{% highlight perl %}
#!/usr/bin/perl -w

use strict;

$^I = ".bak";

my $date = localtime;

while (<>) {
  chomp;
  s/\AAuthor:.*/Author: Randal L. Schwartz/;
  s/\APhone:.*\n//;
  s/\ADate:.*/Date: $date/;
  print;
}{% endhighlight %}

<h4>Equivalent one-liner:</h4>

{% highlight bash %}perl -p -i.bak -w -e 's/\AAuthor:.*/Author: Randal L. Schwartz/g' fred*.dat{% endhighlight %}


<h3 id="ctrl">10.&nbsp;More Control Structures</h3>
<h4>Control structures</h4>

{% highlight perl %}
  unless (...)  # equivalent to if (! ...)
  until  (...)  # equivalent to while (! ...)
{% endhighlight %}

<h4>Statement modifiers</h4>

{% highlight perl %}
  ... if ...
  ... unless ...
  ... while ...
  ... foreach ...
{% endhighlight %}

<h4>Examples</h4>

{% highlight perl %}
print "$n is a negative number" if $n < 0;
print " ", ($n += 2) while $n < 10;
{% endhighlight %}

Naked block provides scope for temporary lexical variables:

{% highlight perl %}
{
    body;
    body;
    body;
}
{% endhighlight %}

<h4>elsif clause</h4>

{% highlight perl %}
if $n < 5 {
    ...
}
elsif $n > 5 {
    ...
}
else {
    ...
}
{% endhighlight %}

<h4>Autoincrement and autodecrement</h4>

{% highlight perl %}
my $i = 1;
$i++;
{% endhighlight %}

{% highlight perl %}
my @people = qw(fred barney fred wilma dino barney fred pebbles);
my %count;                     # empty hash
$count{$_}++ foreach @people;  # create new keys and values as needed
{% endhighlight %}

<h4>Value of autoincrement</h4>

{% highlight perl %}
my $i =    1;
my $j = ++$i;  #  pre-increment, increment first, assign    second
my $k = $i++;  # post-increment, assign    first, increment second
{% endhighlight %}

<h4>Infinite loop</h4>

{% highlight perl %}
for (;;) {
    ...
}

while (1) {
    ...
}
{% endhighlight %}

<h4>Loop controls</h4>

{% highlight perl %}
last  # equivalent to C's break
next  # proceed to next iteration of loop
redo  # redo current iteration, do not test for any condition
{% endhighlight %}

<h4>Conditional (ternary) operator</h4>

{% highlight perl %}my $location = &is_weekend($day) ? "home" : "work";{% endhighlight %}

<h4>Logical operators</h4>

{% highlight perl %}
&&  # and
||  # or
{% endhighlight %}

<h4>Defined-or (since v5.10)</h4>

{% highlight perl %}
//  # equivalent to make's ?=
my $last_name = $last_name{$someone} // '(No last name)'
{% endhighlight %}

<h3 id="modules">11.&nbsp;Perl Modules</h3>

<h4>Finding modules</h4>

<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="http://www.metacpan.org">MetaCPAN</a>

Check if module is installed by trying to read its documentation

{% highlight bash %}perldoc Digest::SHA{% endhighlight %}

Get details on a module with the <tt>cpan</tt> command

{% highlight bash %}cpan -D Digest::SHA{% endhighlight %}

<h4>Installing modules</h4>

If the module uses <tt>ExtUtils::MakeMaker</tt> install new modules with
{% highlight bash %}
perl Makefile.PL INSTALL_BASE=&lt;prefix&gt;
make install
{% endhighlight %}

Modules based on <tt>Module::Build</tt> should be build and installed with
{% highlight bash %}
perl Build.PL
./Build install --install_base=&lt;prefix&gt;
{% endhighlight %}

To use <tt>CPAN.pm</tt> for module installation and all of its dependencies issue
{% highlight bash %}
perl -MCPAN -e shell
{% endhighlight %}

Another (user-friendly) alternative is to install modules with the <tt>cpan</tt> script (it comes with Perl)
{% highlight bash %}
cpan Module::CoreList LWP CGI::Prototype
{% endhighlight %}

Finally, there is <tt>cpanm</tt> (<a href="https://cpanmin.us">cpanminus</a>) that is designed as a zero-configuration, lightweight CPAN client.
{% highlight bash %}
cpanm DBI WWW::Mechanize
{% endhighlight %}

<h4>Using custom installation directories</h4>

Use <tt>local::lib</tt> to set environment variables affecting CPAN module installation. View default settings with
{% highlight bash %}
perl -Mlocal::lib
{% endhighlight %}

Call <tt>cpan</tt> with <tt>-I</tt> switch to respect <tt>local::lib</tt> settings
{% highlight bash %}
cpan -I Set::CrossProduct
{% endhighlight %}

To make your Perl program aware of modules in custom locations issue
{% highlight perl %}
use local::lib;
{% endhighlight %}

<h4>Using Simple Modules</h4>

{% highlight perl %}
use File::Basename;

my $name = "/usr/local/bin/perl";
my $base = basename $name;  # returns 'perl'
{% endhighlight %}

<h4>Using Only Some Functions from a&nbsp;Module</h4>

{% highlight perl %}
use File::Basename qw(basename);
{% endhighlight %}

<h4>Call functions by their full names</h4>

{% highlight perl %}
use File::Basename qw();  # import no functions

my $dirname = File::Basename::dirname $name;
{% endhighlight %}

<h4>File::Spec Module</h4>

{% highlight perl %}
use File::Spec;  # object oriented module

my $path = File::Spec->catfile($dirname, $basename);
{% endhighlight %}

<h4>Path::Class Module</h4>

{% highlight perl %}
my $dir    =  dir( qw(Users mabalenk repo));
my $subdir = $dir->subdir('git');        # /Users/mabalenk/repo/git
my $parent = $dir->parent;               # /Users/mabalenk
my $windir = $dir->as_foreign('Win32');  # \Users\mabalenk\repo
{% endhighlight %}

<h4>Databases and DBI</h4>

<i class="fas fa-book"></i>&nbsp;<a href="http://shop.oreilly.com/product/9781565926998.do">Programming the Perl DBI</a> (2000, O'Reilly Media) by Tim Bunce and Alligator Descartes&nbsp;{% cite Bunce00 --file references %}
<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="http://dbi.perl.org">DBI website</a>

Example of using the <tt>DBI</tt> module:

{% highlight perl %}
use DBI;

# connect to database
my $dbh = DBI->connect($data_source, $username, $password);

# prepare, execute, read query
my $sth = $dbh->prepare("SELECT * FROM persons WHERE surname == 'Smith'");
$sth->execute;
my @row = $sth->fetchrow_array;
$sth->finish;

# disconnect from database
$dbh->disconnect();
{% endhighlight %}

<h4>Dates and Times</h4>

Example of using the <tt>Time::Moment</tt> module:

{% highlight perl %}
use Time::Moment;

# convert time from system to human readable
my $dt = Time::Moment->from_epoch( time );

# obtain current time
my $dt = Time::Moment->now;

# access various parts of date
printf "%4d%02d%02d", $dt->year, $dt->month, $dt->day_of_month;

# arithmetic operations with two time moment objects
my $dt1 = Time::Moment->new(
    year  => 1987,
    month => 12,
    day   => 18,
);

my $dt2 = Time::Moment->now;

my $years  = $dt1->delta_years(  $dt2 );
my $months = $dt1->delta_months( $dt2 ) % 12;

printf "%d years and %d months\n", $years, $months;
{% endhighlight %}

<i class="fas fa-external-link-square-alt"></i>&nbsp;<a href="http://www.effectiveperlprogramming.com/2014/09/use-postfix-dereferencing">Use postfix dereferencing</a>


<h3 id="files">12.&nbsp;File Tests</h3>

<h4>File test operators</h4>

To get the list of all the file test operators type:

{% highlight bash %}perldoc -f -X{% endhighlight %}

Processing a&nbsp;list of files:

{% highlight perl %}
my @original_files = qw( fred barney betty wilma pebbles dino bamm-bamm );
my @outdated_files;  # to be put on backup tapes
foreach my $filename (@original_files) {
    push @outdated_files, $filename
        if -s $filename > 100_000 and -A $filename > 90;
}
{% endhighlight %}

<table class="book">
  <caption>Table&nbsp;12-1. File tests and their meaning</caption>
  <th>File test</th>
  <th>Meaning</th>
  <tr>
    <td><tt>-r</tt></td>
    <td>File or directory is readable by this (effective) user or group</td>
  </tr>
  <tr>
    <td><tt>-w</tt></td>
    <td>File or directory is writable by this (effective) user or group</td>
  </tr>
  <tr>
    <td><tt>-x</tt></td>
    <td>File or directory is executable by this (effective) user or group</td>
  </tr>
  <tr>
    <td><tt>-o</tt></td>
    <td>File or directory is owned by this (effective) user or group</td>
  </tr>
  <tr>
    <td><tt>-R</tt></td>
    <td>File or directory is readable by this real user or group</td>
  </tr>
  <tr>
    <td><tt>-W</tt></td>
    <td>File or directory is writable by this real user or group</td>
  </tr>
  <tr>
    <td><tt>-X</tt></td>
    <td>File or directory is executable by this real user or group</td>
  </tr>
  <tr>
    <td><tt>-O</tt></td>
    <td>File or directory is owned by this real user or group</td>
  </tr>
  <tr>
    <td><tt>-e</tt></td>
    <td>File or directory name exists</td>
  </tr>
  <tr>
    <td><tt>-z</tt></td>
    <td>File exists and has zero size (always false for directories)</td>
  </tr>
  <tr>
    <td><tt>-s</tt></td>
    <td>File exists and has nonzero size (the value is the size in bytes)</td>
  </tr>
  <tr>
    <td><tt>-f</tt></td>
    <td>Entry is a&nbsp;plain file</td>
  </tr>
  <tr>
    <td><tt>-d</tt></td>
    <td>Entry is a&nbsp;directory</td>
  </tr>
  <tr>
    <td><tt>-l</tt></td>
    <td>Entry is a&nbsp;symbolic link</td>
  </tr>
  <tr>
    <td><tt>-S</tt></td>
    <td>Entry is a&nbsp;socket link</td>
  </tr>
  <tr>
    <td><tt>-p</tt></td>
    <td>Entry is a&nbsp;named pipe (a &ldquo;fifo&rdquo;)</td>
  </tr>
  <tr>
    <td><tt>-b</tt></td>
    <td>Entry is a&nbsp;block-special file (like a&nbsp;mountable disk)</td>
  </tr>
  <tr>
    <td><tt>-c</tt></td>
    <td>Entry is a&nbsp;character-special file (like an&nbsp;I/O device)</td>
  </tr>
  <tr>
    <td><tt>-u</tt></td>
    <td>File or directory is setuid</td>
  </tr>
  <tr>
    <td><tt>-g</tt></td>
    <td>File or directory is setgid</td>
  </tr>
  <tr>
    <td><tt>-k</tt></td>
    <td>File or directory has a&nbsp;sticky bit set</td>
  </tr>
  <tr>
    <td><tt>-t</tt></td>
    <td>The filehandle is a&nbsp;TTY (as reported by the <tt>isatty()</tt> system function; filenames can't be tested by this test)</td>
  </tr>
  <tr>
    <td><tt>-T</tt></td>
    <td>File looks like a&nbsp;&ldquo;text&rdquo; file</td>
  </tr>
  <tr>
    <td><tt>-B</tt></td>
    <td>File looks like a&nbsp;&ldquo;binary&rdquo; file</td>
  </tr>
  <tr>
    <td><tt>-M</tt></td>
    <td>Modification age (measured in days)</td>
  </tr>
  <tr>
    <td><tt>-A</tt></td>
    <td>Access age (measured in days)</td>
  </tr>
  <tr>
    <td><tt>-C</tt></td>
    <td>Inode-modification age (measured in days)</td>
  </tr>
</table>

<h4>Using default filename stored in <tt>$_</tt></h4>

{% highlight perl %}
foreach (@filenames) {
    print "$_ is readable\n" if -r;  # same as -r $_
}
{% endhighlight %}

<h4>Testing Several Attributes of the Same File</h4>

Calling system's <tt>stat</tt> each time:

{% highlight perl %}
# expensive file tests
if (-r $filename and -w $filename) {
    ...
}
{% endhighlight %}

Re-using information from last file lookup:

{% highlight perl %}
# elegant file tests
# _ is a virtual filehandle
if (-r $filename and -w _) {
    ...
}
{% endhighlight %}

<h4>Stacked File Test Operators (Starting with Perl 5.10)</h4>

{% highlight perl%}
use v5.10;

if (-w -r $filename) {
    print "The file is both readable and writable!\n";
}
{% endhighlight %}

<h4>The <tt>stat</tt> and <tt>lstat</tt> Functions</h4>

Return value of a&nbsp;call to <tt>stat</tt> is a&nbsp;13-element list:

{% highlight perl %}
my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev,
    $size, $atime, $mtime, $ctime, $blksize, $blocks)
        = stat($filename);
{% endhighlight %}

<tt>$dev</tt> and <tt>$inode</tt>
: device number and inode number of the file

<tt>$mode</tt>
: set of permission bits for the file and some other bits

<tt>$nlink</tt>
: number of (hard) links to the file or directory

<tt>$uid</tt> and <tt>$gid</tt>
: numeric user-ID and group-ID showing file's ownership

<tt>$size</tt>
: size in bytes as returned by the <tt>-s</tt> file test

<tt>$atime, $mtime, $ctime</tt>
: three timestamps (access, modification, creation), tell number of seconds since the <i>epoch</i>

Use <tt>lstat</tt> function to obtain information on symbolic links.<br>
<tt>File::stat</tt> module provides a&nbsp;friendlier interface to <tt>stat</tt>.

<h4>The <tt>localtime</tt> function</h4>

Function <tt>localtime</tt> in a&nbsp;scalar context converts a&nbsp;string into human readable date-time string:

{% highlight perl %}
my $timestamp = 1454133253;
my $date      = localtime $timestamp;
{% endhighlight %}

<tt>localtime</tt> in a&nbsp;list context:

{% highlight perl %}
my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst)
    = localtime $timestamp;
{% endhighlight %}

<tt>$mon</tt>
: a&nbsp;month number [0, 11]

<tt>$year</tt>
: number of years since 1900

<tt>$wday</tt>
: weekday from Sunday to Saturday [0, 6]

<tt>$yday</tt>
: day-of-the-year from Jan 1 through Dec 31 [0, 364(5)]

Function <tt>gmtime</tt> returns time in Universal Time.<br>
Function <tt>time</tt> returns current timestamp from system clock.<br>


<h4>Bitwise Operators</h4>

<table class="book">
  <caption>Table&nbsp;12-2. Examples of bitwise operations</caption>
  <tr>
    <th>Expression</th>
    <th>Meaning</th>
  </tr>
  <tr>
    <td><tt>10&12</tt></td>
    <td>Bitwise-and&mdash;which bits are true in both operands (this gives 8)</td>
  </tr>
  <tr>
    <td><tt>10|12</tt></td>
    <td>Bitwise-or&mdashwhich bits are true in one operand or the other (this gives 14)</td>
  </tr>
  <tr>
    <td><tt>10^12</tt></td>
    <td>Bitwise-xor&mdash;which bits are true in one operand or the other but not in both (this gives 6)</td>
  </tr>
  <tr>
    <td><tt>6<<2</tt></td>
    <td>Bitwise shift left&mdash;shift the left operand the number of bits shown by the right operand, adding zero-bits at the least-significant places (this gives 24)</td>
  </tr>
  <tr>
    <td><tt>25>>2</tt></td>
    <td>Bitwise shift right&mdash;shift the left operand the number of bits shown by the right operand, discarding the least-significant bits (this gives 6)</td>
  </tr>
  <tr>
    <td><tt>~10</tt></td>
    <td>Bitwise negation, also called unary bit complement, returns the number with the opposite bit for each bit in the operand (this gives 0xFFFFFFF5)</td>
  </tr>
</table>


<h4>Using Bitstrings</h4>

Since Perl&nbsp;5.22 it is possible to use either (all) numeric bitwise operation or (all) string:

{% highlight perl %}
use v5.22;
use feature qw(bitwise);
no warnings qw(experimental::bitwise);

my $number_str = '137';
my $str        = 'Amelia';

# treat both operands as numbers
say "number_str &  str: ", $number_str & $str;

# treat both operands as strings
say "number_str &. str: ", $number_str &. $str;
{% endhighlight %}

Results in

{% highlight bash %}
number_str &  string: 0
number_str &. string: ¿!%
{% endhighlight %}


<h3 id="directories">13.&nbsp;Directory Operations</h3>

<h4>The current working directory</h4>

Obtain current working directory using the <tt>Cwd</tt> module:

{% highlight perl %}
use Cwd;
say "current working directory: ", getcwd();
{% endhighlight %}

Use the <tt>File::Spec</tt> module to convert between relative and absolute paths.


<h4>Changing the directory</h4>

Change directory with <tt>chdir</tt>:

{% highlight perl %}
chdir '/etc' or die "Cannot change to /etc: $!";
{% endhighlight %}

Module <tt>File::HomeDir</tt> helps to set and get the environment variables for <tt>chdir</tt>.


<h4>Globbing</h4>

Use the <tt>glob</tt> operator to expand a&nbsp;pattern into the matching filenames:

{% highlight perl %}
my @all_files = glob '*';
my @pm_files  = glob '*.pm';
{% endhighlight %}


<h4>An alternate syntax for globbing</h4>

Legacy Perl code uses angle-bracket syntax:

{% highlight perl %}
my @all_files = <*>;

# fetch dot and non-dot files
my $dir = '/etc';
my @dir_files = <$dir/* $dir/.*>;
{% endhighlight %}

<h4>Directory handles</h4>

Obtain list of filenames from a&nbsp;directory using a&nbsp;directory handle:

{% highlight perl %}
my $dname = '/etc';

opendir my $dhandle, $dname or die "Cannot open $dname: $!";

foreach $file (readdir $dhandle) {
    print "Processing file $file\n";
}
closedir $dhandle;
{% endhighlight %}

Alternatively use a&nbsp;bareword directory handle:

{% highlight perl %}
opendir DIR, $dname or die "Cannot open $dname: $!";

foreach $file (readdir DIR) {
    print "Processing file $file\n";
}
closedir DIR;
{% endhighlight %}

Compared to globbing this is a&nbsp;lower-level operation with more manual work. The list includes all files, not just those matching a&nbsp;given pattern. Implement a&nbsp;skip-over function to obtain the necessary files:

{% highlight perl %}
while ($name = readdir $dh) {

    # files ending with .pm
    next unless $name =~ /\.pm\z/;

    # nondot files
    next if $name =~ /\A\./;

    # everything but the dot and dotdot files
    next if $name eq '.' or $name eq '..';

    # ... more processing ..
}
{% endhighlight %}

<i class="fas fa-exclamation-circle"></i>&nbsp;Filename returned by the <tt>readdir</tt> operator has no pathname component! It's only the name within the directory. Patch up the name to get the full name:

{% highlight perl %}
opendir $dir, $dname or die "Cannot open $dname: $!";

while (my $name = readdir $dir) {

    next if $name =~ /\A./;             # skip over dot files
    $name = "$dname/$name";             # patch up the path
    next unless -f $name and -r $name;  # obtain only readable files

    # ... more processing ...
}
{% endhighlight %}

To improve portability use <tt>File::Spec::Functions</tt> module to construct the path:

{% highlight perl %}
use File::Spec::Functions;

opendir $dir, $dname or die "Cannot open $dname: $!";

while (my $name = readdir $dir) {

    next if $name =~ /\A./;             # skip over dot files
    $name = catfile($dname, $name);     # patch up the path
    next unless -f $name and -r $name;  # obtain only readable files

    # ... more processing ...
}
{% endhighlight %}


<h4>Removing files</h4>

Use the <tt>unlink</tt> operator to remove files:

{% highlight perl %}
unlink 'slate', 'bedrock', 'lava';

unlink qw(slate, bedrock, lava);
{% endhighlight %}

Combine <tt>unlink</tt> with <tt>glob</tt> to delete multiple files:

{% highlight perl %}
unlink glob '*.o';
{% endhighlight %}

Return value of a successful call to <tt>unlink</tt> is a number of files deleted:

{% highlight perl %}
my $nfiles = unlink glob '*.o';
print "$nfiles were deleted.\n";
{% endhighlight %}

To know which <tt>unlink</tt> operation failed, process files in a&nbsp;loop:

{% highlight perl %}
foreach my $file ( qw(slate bedrock lava) ) {
    unlink $file or warn "failed on $file: $!\n";
}
{% endhighlight %}

<i class="fas fa-exclamation-circle"></i>&nbsp;It is possible to remove files that you can't read, you can't write, you can't execute and you don't even own (<tt>chmod 0</tt>).<br>


<h4>Renaming files</h4>

Give a&nbsp;new name or move a&nbsp;file with the <tt>rename</tt> function:

{% highlight perl %}
rename 'old', 'new';
rename 'over_there/some/place/some_file',   'some_file';

# using a fat arrow as a separator
rename 'over_there/some/place/some_file' => 'some_file';
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Moving files to another disk partition with <tt>rename</tt> is not possible.

To batch rename a&nbsp;list of files:

{% highlight perl %}
foreach my $file (glob "*.old") {
    my $newfile = $file;
    
    # left  part of substituion is a regex
    # right part is a string, no backslash required
    $newfile =~ s/\.old$/.new/;

    if (-e $newfile) {
        warn "can't rename $file to $newfile: $newfile exists\n";
    }
    elsif (rename $file => $newfile) {
        # success, do nothing
    }
    else {
        warn "rename $file to $newfile failed: $!\n";
    }
}
{% endhighlight %}


<h4>Links and files</h4>

Create hard and soft links to files with:

{% highlight perl %}
link 'chicken', 'egg';  # hard

symlink 'dodgson', 'carroll';  # soft, symbolic link
{% endhighlight %}

To test whether a&nbsp;file is a&nbsp;symbolic link use:

{% highlight perl %}
print "$file is a symbolic link" if (-l $file);
{% endhighlight %}

To find out where a&nbsp;symbolic link is pointing, use the <tt>readlink</tt> function:

{% highlight perl %}
my $perl = readlink '/usr/local/bin/perl';
{% endhighlight %}


<h4>Making directories</h4>

Make a&nbsp;directory:

{% highlight perl %}
mkdir 'fred', 0755 or warn "Cannot make fred directory: $!";
{% endhighlight %}

Second parameter is the initial permission setting in octal. Make sure to write it with a leading zero or use the <tt>oct</tt> function:

{% highlight perl %}
mkdir $dname, oct($dpermissions);
{% endhighlight %}

Use extra call to <tt>oct</tt> function when the value comes from user input:

{% highlight perl %}
my ($dname, $permissions) = @ARGV;
mkdir $dname, oct($permissions) or die "Cannot create $dname: $!";
{% endhighlight %}


<h4>Removing directories</h4>

To remove <i>empty</i> directories use <tt>rmdir</tt> function. Although, it removes one directory at a&nbsp;time:

{% highlight perl %}
foreach my $dir ( qw(fred barney betty) ) {
    rmdir $dir or warn "cannot rmdir $dir: $!\n";
}
{% endhighlight %}

Example of writing many temporary files during the execution of a&nbsp;program:

{% highlight perl %}
# create directory based on process ID
my $temp_dir = "/tmp/scratch_$$";
mkdir $temp_dir, 0700 or die "cannot create $temp_dir: $!";
# ...
# use $temp_dir as location of all temporary files
# ...
unlink glob "$temp_dir/* $temp_dir/.*";  # delete $temp_dir contents
rmdir $temp_dir;                         # delete now-empty directory
{% endhighlight %}

Check out <tt>File::Temp</tt> module for creating temporary directories or files and <tt>remove_tree</tt> function provided by the <tt>File::Path</tt> module.


<h4>Modifying permissions</h4>

Use <tt>chmod</tt> function to change permissions on a&nbsp;file or directory:

{% highlight perl %}
chmod 0700, 'fred', 'barney';
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Symbolic permissions with <tt>ugoa</tt> and <tt>rwxXst</tt> are not supported by the <tt>chmod</tt> function. Use <tt>File::chmod</tt> module to enable symbolic mode values in <tt>chmod</tt>.


<h4>Changing ownership</h4>

Change the ownership and group membership of a list of files:

{% highlight perl %}
my $user  = 1004;
my $group =  100;
chown $user, $group glob '*.cxx';
{% endhighlight %}

Apply helper functions <tt>getpwnam</tt> and <tt>getgrnam</tt> to convert user and group names into numbers:

{% highlight perl %}
defined(my $user  = getpwnam 'jsmith') or die 'bad user:  $!\n';
defined(my $group = getgrnam 'users')  or die 'bad group: $!\n';
chwon $user, $group, glob '/home/mabalenk/*'
{% endhighlight %}


<h4>Changing timesteps</h4>

Use <tt>utime</tt> function to update the access and modification time of a list of files:

{% highlight perl %}
my $atime = time;
my $mtime = $atime - 24 * 60 * 60;   # seconds per day
utime $atime, $mtime, glob '*.tex';  # set access to now, mod to a day ago
{% endhighlight %}


<h3 id="strings">14.&nbsp;Strings and Sorting</h3>
<h4>Finding a&nbsp;substring with <tt>index</tt></h4>

To locate first occurrence of <tt>substring</tt> in a&nbsp;<tt>string</tt> use the <tt>index</tt> operator:

{% highlight perl %}
my $idx = index($string, $substring);
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;The character position returned by <tt>index</tt> is a&nbsp;zero-based value. If the substring was not found <tt>index</tt> returns -1.

The third parameter to <tt>index</tt> specifies, where to start searching for a given substring. By default <tt>index</tt> searches from the beginning of the string:

{% highlight perl %}
use v5.10;

my $string = "Welcome to Warsaw!";

my @indices = ();
my $idx     = -1;

while (1) {
    $idx = index($string, 'W', $idx+1);
    last if $idx == -1;
    push @indices, $idx;
}

say "Indices are @indices";
{% endhighlight %}

Operator <tt>rindex</tt> finds the last occurrence of the substring (i.e. scans from the end of the string).

{% highlight perl %}
my $last_slash = rindex("/etc/passwd", '/');  # value is 4
{% endhighlight %}

<i class="fas fa-exclamation-circle"></i>&nbsp;Operator <tt>rindex</tt> counts from the left, from the beginning of the string.

<h4>Manipulating a&nbsp;substring with <tt>substr</tt></h4>

Operator <tt>substr</tt> extracts a&nbsp;part of the string:

{% highlight perl %}
my $sub = substr($str, $start, $length);
{% endhighlight %}

The <tt>$length</tt> parameter may be omitted, if the end of string is required. Initial position <tt>$start</tt> can be negative, counting from the end of the string. In this case <tt>-1</tt> denotes the end of the string.

Functions <tt>index</tt> and <tt>substr</tt> work well together:

{% highlight perl %}
my $str   ="London is truly lovely!";
my $right = substr($str, index($str, "t"));
{% endhighlight %}

Change a&nbsp;given portion of the string with <tt>substr</tt> and assignment:

{% highlight perl %}
my $str = "London is truly lovely!";
substr($str, "t") = "absolutely magical!";  # -> London is absolutely magical!
{% endhighlight %}

Giving a length of 0 allows to insert text without removing anything:

{% highlight perl %}
my $str = "London is absolutely magical!";
substr($str, 0, 0) = "Old";  # -> Old London is absolutely magical!
{% endhighlight %}

Use the binding operator <tt>(=~)</tt> to restrict an&nbsp;operation to work with a&nbsp;substring:

{% highlight perl %}
# replace Fred with Barney within last 20 characters of string
substr($str, -20) =~ s/Fred/Barney/g;
{% endhighlight %}

Alternatively use a&nbsp;four argument version of <tt>substr</tt>, where the fourth argument is the replacement string:

{% highlight perl %}
my $str = "Hello, world!";
my $previous_value = substr($str, 0, 5, "Goodbye");
{% endhighlight %}


<h4>Formatting data with <tt>sprintf</tt></h4>

Function <tt>sprintf</tt> returns the requested string instead of printing it:

{% highlight perl %}
my $date_tag = sprintf "%4d/%02d/%02d %2d:%02d:%02d", $yr, $mo, $da, $h, $m, $s;
# $date_tag -> 2038/01/19 3:00:08
{% endhighlight %}


<h4>Using <tt>sprintf</tt> with &ldquo;money numbers&rdquo;</h4>

Format a&nbsp;number with two digits after the&nbsp;decimal point:

{% highlight perl %}
my $money = sprintf "%.2f", 2.49997;
{% endhighlight %}

To insert commas separating thousands in a&nbsp;number use the following subroutine:

{% highlight perl %}
sub insert_commas {
    my $number = sprintf "%.2f", shift @_;

    # add one comma each time through the do-nothing loop
    # while true insert one comma, then search again
    1 while $number =~ s/^(-?\d+)(\d\d\d)/$1,$2/;

    # put dollar sign in the right place
    $number =~ s/^(-?)/$1\$/;
    $number;
}
{% endhighlight %}

Use modules <tt>Number::Format</tt> and <tt>CLDR::Number</tt> for pre-defined operations with numbers.


<h4>Advanced sorting</h4>

Write a&nbsp;custom comparison statement to specify the sorting order:

{% highlight perl %}
# sorting subroutine, expects two variables $a, $b
sub by_number {
    if ($a < $b) { -1 } elsif ($a > $b) { 1 } else { 0 }
    # -1: a must appear before b
    #  1: b must appear before a
    #  0: order does not matter
}
{% endhighlight %}

To apply custom sorting routine:

{% highlight perl %}
my @result = sort by_number @numbers;
{% endhighlight %}

Three-way comparison for numbers is used frequently. Perl's <i>spaceship</i> operator provides a&nbsp;shortcut for it:

{% highlight perl %}
sub by_number { $a <=> $b }
{% endhighlight %}

Similarly the <tt>cmp</tt> operator defines a&nbsp;three-way comparison for strings:

{% highlight perl %}
# convert both parameters to lower case before comparing
sub case_insensitive { "\L$a" cmp "\L$b" }

my @strings = sort case_insensitive @input_strings;
{% endhighlight %}

To sort Unicode strings apply:

{% highlight perl %}
use Unicode::Normalize;

sub equivalents { NFKD($a) cmp NFKD($b) }
{% endhighlight %}

When the sorting routines are simple, use them &ldquo;inline&rdquo;:

{% highlight perl %}
my $sorted_numbers = sort { $a <=> $b } @input_numbers;
{% endhighlight %}

The reversed order sorting may be obtained either with the <tt>reverse</tt> keyword:

{% highlight perl %}
my @desc_numbers = reverse sort { $a <=> $b } @numbers;
{% endhighlight %}

or by swapping the operands:

{% highlight perl %}
my @desc_numbers = sort { $b <=> $a } @numbers;
{% endhighlight %}

<h4>Sorting a&nbsp;hash by value</h4>

Imagine bowling scores of three characters are stored in a&nbsp;hash:

{% highlight perl %}
my %score = ("barney" => 195, "fred" => 205, "dino" => 30);

# sort the winners by their scores in descending order
my @winners = sort by_score keys %score;
{% endhighlight %}

Enable numeric comparison on the scores, rather than the names:

{% highlight perl %}
# sort by scores in descending order
sub by_score { $score{$b} <=> $score{$a} }
{% endhighlight %}

<h4>Sorting by multiple keys</h4>

Consider a&nbsp;forth entry in the <tt>scores</tt> hash:

{% highlight perl %}
my %score = (
    "barney"    => 195,
    "fred"      => 205,
    "dino"      => 30,
    "bamm-bamm" => 195
);
{% endhighlight %}

If the players have the same score, sort their entries by name:

{% highlight perl %}
my @winners = sort by_score_and_name keys %score;

sub by_score_and_name {
    $score{$b} <=> $score{$a}
        or
    # break the tie with a string-order comparison
    $a cmp $b
} @winners
{% endhighlight %}

Example of a&nbsp;library program using a&nbsp;five-level sort:

{% highlight perl %}
@patron_IDs = sort {
    &fines($b) <=> &fines($a) or
    $items{$b} <=> $items{$a} or
    $family_name{$a} cmp $family_name{$b} or
    $personal_name{$a} cmp $personal_name{$b} or
    $a <=> $b   # patron's ID number
} @patron_IDs
{% endhighlight %}


<h3 id="processes">15.&nbsp;Process Management</h3>
<h4>The system function</h4>

Launch a&nbsp;child process with:

{% highlight perl %}
system 'date';
{% endhighlight %}

Use single quotes, if Perl interpolation is not needed:

{% highlight perl %}
# $HOME is not a Perl variable
system 'ls -l $HOME';
{% endhighlight %}

Use shell's facility to launch a&nbsp;background process (Perl will not wait for it to finish):

{% highlight perl %}
system './compute &';
{% endhighlight %}


<h4>Avoiding the shell</h4>

Invoke the system operator with multiple arguments to avoid the shell:

{% highlight perl %}
system 'tar', 'cvf', $tarfile, @dirs;
{% endhighlight %}

<i class="fas fa-exclamation-circle"></i>&nbsp;For security reasons choose a&nbsp;<i>multi-argument</i> call to <tt>system</tt> (vs a&nbsp;single-argument call).

System operator returns 0 on success (&ldquo;0 but true&rdquo; notion):

{% highlight perl %}
!system 'rm -frv @files' or die 'Error: unable to delete files!';
{% endhighlight %}


<h4>The environment variables</h4>

Modify environment variables to be inherited by child processes:

{% highlight perl %}
$ENV{'PATH'} = "/root/bin:$ENV{'PATH'}";
delete $ENV{'MKLROOT'};
my $result = system 'make';
{% endhighlight %}

Rely on the <tt>Config</tt> module to use a&nbsp;path separator native for the operating system:

{% highlight perl %}
use Config;
$ENV{'PATH'} = join $Config{'path_sep'}, '/root/bin', $ENV{'PATH'};
{% endhighlight %}


<h4>The exec function</h4>

The <tt>exec</tt> function causes the <i>main</i> Perl process to perform the requested action:

{% highlight perl %}
exec 'date';
die "date couldn't run: $!";
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;There is no main Perl process to return to after the <tt>exec</tt> function finishes.


<h4>Using backquotes to capture output</h4>

Place a&nbsp;command between backquotes to save its output:

{% highlight perl %}
my $dt = `date`;
print "Current time is $dt";
{% endhighlight %}

Example: invoking the <tt>perldoc</tt> command repeatedly for a&nbsp;set of functions:

{% highlight perl %}
my @func = qw{ int rand sleep length hex eof not exit sqrt umask };
my %doc;

foreach (@func) {

    $doc{$_} = `perldoc -t -f $_`;

    # alternatively: use generalised quoting operator qx()
    $doc{$_} = qx(perldoc -t -f $_);
}
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Avoid using backquotes, when output capture is not needed.


<h4>Using backquotes in a&nbsp;list context</h4>

Get the data automatically broken up by lines:

{% highlight perl %}
my @lines = `who`;
{% endhighlight %}

Use the result of a&nbsp;<tt>system</tt> command in a&nbsp;list context:

{% highlight perl %}
# each line from 'who' is placed into the default variable $_
foreach (`who`) {
    my ($usr, $tty, $date) = /(\S+)\s+(\S+)\s+(.*)/;
    $ttys{$usr} .= "$tty at $date\n";
}
{% endhighlight %}


<h4>External processes with IPC::System::Simple</h4>

This module provides simpler interface compared to Perl's built-in <tt>system</tt> utility:

{% highlight perl %}
use IPC::System::Simple qw(system);

# more robust version of system
system  'tar', 'cvf', $tarfile, @dirs;

# systemx never calls the shell
systemx 'tar', 'cvf', $tarfile, @dirs;
{% endhighlight %}

To capture output replace the <tt>system</tt> command with <tt>capture</tt>:

{% highlight perl %}
my @output = capturex 'tar', 'cvf', $tarfile, @dirs;
{% endhighlight %}


<h4>Processes as filehandles</h4>

Using processes as filehandles provides the only easy way to write to a&nbsp;process based on the results of computation.

Launch a concurrent (parallel) child process with the <i>piped open</i> command:

{% highlight perl %}
open DATE, 'date|' or die "Cannot pipe from date: $!";
open MAIL, '|mail' or die "Cannot pipe to mail: $!";
{% endhighlight %}

The three-argument form is:

{% highlight perl %}
# pipe from 'date', '-' shows placement of system command, e.g. date, mail
open my $date_fh, '-|', 'date';

# pipe to 'mail'
open my $mail_fh, '|-', 'mail';
{% endhighlight %}

Read filehandle normally to obtain data:

{% highlight perl %}
my $now = <$date_fh>;
{% endhighlight %}

Print with filehandle to send data:

{% highlight perl %}
print $mail_fh "Current time: $now";
{% endhighlight %}

Close filehandle to finish sending data:

{% highlight perl %}
close $mail_fh;

# @note: exit status is saved into special variable $?
die "mail: nonzero exit of $?" if $?;
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;In case of reading from process while data is not available, process is suspended until sending program speaks again.

For reading backquotes are easier to manage unless you want to have results as they come in.

Example of `find` command printing results as they are found:

{% highlight perl %}
open my $find_fh, '-|', 'find', qw( / -atime +90 -size +1000 -print )
    or die "fork: $!";

while (<$find_fh>) {
    chomp;
    printf "%s size %dK last accessed %.2f days ago\n",
        $_, (1023 + -s $_)/1024, -A $_;
}
{% endhighlight %}


<h4>Getting down and dirty with fork</h4>

It is possible to access low-level process management system calls directly:

Re-implementation of `system 'date'`:

{% highlight perl %}
defined(my $pid = fork) or die "Cannot fork: $!";

unless ($pid) {
    # switch to child process
    exec 'date';
    die "Cannot exec date: $!";
}

# back to parent process
waitpid($pid, 0);
{% endhighlight %}


<h4>Sending and receiving signals</h4>

Send "interrupt signal" `SIGINT` to process with known ID:

{% highlight perl %}
my $pid = 4201;

kill 'INT', $pid or die "Cannot signal $pid with SIGINT: $!";

# OR alternatively
kill INT => $pid or die "Cannot signal $pid with SIGINT: $!";
{% endhighlight %}

Check, if process is still alive:

{% highlight perl %}
# use special signal 0
unless (kill 0, $pid) {
    warn "$pid has gone away!";
}
{% endhighlight %}

Assign into the special <tt>%SIG</tt> hash to activate the signal handler:

{% highlight perl %}
sub sig_int_handler() {
    die "Caught interrupt signal: $!\n";
}

$SIG{'INT'} = 'sig_int_handler';
{% endhighlight %}

Example of a&nbsp;custom signal handler {% cite Schwartz17 --file references %} (chapter&nbsp;15, exercise&nbsp;4, pp.&nbsp;273,&nbsp;324,&nbsp;325):

{% highlight perl %}
use v5.34;
use strict;
use warnings;

sub hup_handler {
    state $n;
    printf("Caught HUP %d times.\n", ++$n);
}

sub usr1_handler {
    state $n;
    printf("Caught USR1 %d times.\n", ++$n);
}

sub usr2_handler {
    state $n;
    printf("Caught USR2 %d times.\n", ++$n);
}

sub int_handler {
    printf("\nCaught INT. Exiting.\n");
    exit;
}

foreach my $sig ( qw(int hup usr1 usr2) ) {
    $SIG{ uc $sig } = "${sig}_handler";
}

printf("My process no: $$.\n");

while (1) {
    sleep(1);
}
{% endhighlight %}


<h3 id="adv_tech">16.&nbsp;Some Advanced Perl Techniques</h3>
<h4>Slices</h4>

Imagine the following list:

{% highlight bash %}
fred flintstone:2168:301 Cobblestone Way:555-1212:555-2121:3
barney rubble:709918:299 Cobblestone Way:555-3333:555-3438:0
{% endhighlight %}

Extract a&nbsp;few elements using conventional arrays:

{% highlight perl %}
while <$fh> {
    chomp;
    my @items = split /:/;
    my ($card_num, $count) = ($items[1], $items[5]);
    ... # work with those variables
}
{% endhighlight %}

Assign the result of <tt>split</tt> to a&nbsp;list of scalars:

{% highlight perl %}
# avoid using array @items
my ($name, $card_no, $addr, $home, $work, $count) = split /:/;
{% endhighlight %}

Use <tt>undef</tt> to ignore corresponding elements of the source list:

{% highlight perl %}
my (undef, $card_no, undef, undef, undef, $count) = split /:/;
{% endhighlight %}

Extreme use case to extract <tt>mtime</tt> value from <tt>stat</tt>:

{% highlight perl %}
my (undef, undef, undef, undef, undef, undef, undef, undef, undef, $mtime) = 
    stat $filename;
{% endhighlight %}

Instead, index into a&nbsp;list as if it were an&nbsp;array (with a&nbsp;<i>list slice</i>):

{% highlight perl %}
my $mtime = (stat $filename)[9];
{% endhighlight %}

Use list slices to pull out items from the initial example:

{% highlight perl %}
my $card_no = (split /:/)[1];
my $count   = (split /:/)[5];
{% endhighlight %}

List slice in a list context (merge two operations together):

{% highlight perl %}
my ($card_no, $count) = (split /:/)[1, 5];
{% endhighlight %}

Pull the first and last items from a list:

{% highlight perl %}
my ($first, $last) = (sort @names)[0, -1];
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Use <tt>List::Util</tt> module for better, more efficient sorting.

Slice subscripts may be in any order and may repeat indices:

{% highlight perl %}
my @names = qw{ zero one two three four five six seven eight nine };
my @numbers = ( @names )[ 9, 0, 2, 1, 0 ];
print "Bedrock @numbers\n";  # says Bedrock nine zero two one zero
{% endhighlight %}


<h4>Array Slice</h4>

Parentheses may be omitted, when slicing elements from an&nbsp;array:

{% highlight perl %}
my @numbers = @names[ 9, 0, 2, 1, 0 ];
{% endhighlight %}

Interpolate a&nbsp;slice directly into a&nbsp;string:

{% highlight perl %}
my @names = qw{ zero one two three four five six seven eight nine };
print "Bedrock @names[ 9, 0, 2, 1, 0 ]";
{% endhighlight %}

Update selected elements of the&nbsp;array:

{% highlight perl %}
my $new_phone   = "555-6090";
my $new_address = "99380 Red Rock West";
@items[2, 3] = ($new_address, $new_phone);
{% endhighlight %}


<h4>Hash Slice</h4>

Pull values with a&nbsp;list of hash keys or with a&nbsp;slice:

{% highlight perl %}
my @selected_scores = ( $score{"barney"}, $score{"fred"}, $score{"dino"} );

my @selected_scores = ( @score{ qw/ barney fred dino / } );
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Slice is always a&nbsp;list. Hence, the hash slice notation uses an&nbsp;at sign.

Elegant assignment using hash slices:

{% highlight perl %}
my @players          = qw/ barney fred dino /;
my @scores           = (195, 205, 30);
@results{ @players } = @scores;
{% endhighlight %}

<h4>Key-Value Slices</h4>

Since v5.20 it is possible to extract key-value pairs with a&nbsp;key-value slice:

{% highlight perl %}
use v5.20;

my %top_results = %results{ @top_players };
{% endhighlight %}

<i class="fas fa-bell"></i>&nbsp;Sigils do not denote variable type, they communicate what you do with the variable. Key-value pairs is a&nbsp;<i>hashy</i> sort of operation, hence there is <tt>%</tt> in front of it.

<h4>Trapping Errors</h4>
<h4>Using <tt>eval</tt></h4>

Wrap code in an&nbsp;<tt>eval</tt> block to trap fatal errors:

{% highlight perl %}
my $barney = eval { $fred / $dino } // 'NaN';
{% endhighlight %}

In case of an&nbsp;error the <tt>eval</tt> block stops running, but the program doesn't crash.

<h4>More Advanced Error Handling</h4>

In basic Perl, you may throw an&nbsp;exception with <tt>die</tt> and catch it with <tt>eval</tt>:

{% highlight perl %}
eval {
    ...
    die "Bad denominator" if $deno == 0;
}
if ( $@ =~ /unexpected/ ) {
    ...
}
elseif ( $@ =~ /denominator/ ) {
    ...
}
{% endhighlight %}

Inspect value of <tt>$@</tt> to figure out what went wrong.

Dynamic scope of <tt>$@</tt> may cause problems. Use module <tt>Try::Tiny</tt> from CPAN for better error handling.

{% highlight perl %}
use Try::Tiny;

try {
    ... # code that may throw an error
}
catch {
    ... # code to handle the error
}
finally {
    ... # execute in any case
}
{% endhighlight %}

<tt>Try::Tiny</tt> puts the error message into <tt>$\_</tt> to prevent abuse of <tt>$@</tt>.

<h4>Picking Items from a&nbsp;List with <tt>grep</tt></h4>

Perl's <tt>grep</tt> operator acts as a&nbsp;filter:

{% highlight perl %}
# select odd numbers from range
my @odd_numbers    = grep { $_ % 2 } 1..1000;

# pick matching lines from file
my @matching_lines = grep { /\bfred\b/i } <$fh>;

# simpler syntax with comma
my @matching_lines = grep   /\bfred\b/i,  <$fh>;
{% endhighlight %}

In scalar context <tt>grep</tt> tells the number of items selected:

{% highlight perl %}
my $line_count = grep /\bfred\b/i, <$fh>;
{% endhighlight %}


<h4>Transforming Items from a&nbsp;List with <tt>map</tt></h4>

Use <tt>map</tt> operator to <i>change</i> every item in a&nbsp;list:

{% highlight perl %}
my @data = (4.75, 1.5, 2, 1234, 6.9456, 12345678.9, 29.95);

my @formatted_data = map { big_money($_) } @data;
{% endhighlight %}

Instead of returning a&nbsp;Boolean value as <tt>grep</tt>, <tt>map</tt> generates a&nbsp;list of values.

Simpler syntax of <tt>map</tt>:

{% highlight perl %}
print "Powers of two are:\n",
    map "\t" . ( 2 ** $_ ) . "\n", 0..16;
{% endhighlight %}


<h4>Fancier List Utilities</h4>

<tt>List::Util</tt> module from Standard Library enables high performance list processing utilities:

First occurrence:

{% highlight perl %}
use List::Util qw(first);

my $first_match = first { /\bPebbles\b/i } @characters;
{% endhighlight %}

Sum:

{% highlight perl %}
use List::Util qw(sum);

my $total = sum( 1..1000 );
{% endhighlight %}

Maximum numeric and textual:

{% highlight perl %}
use List::Util qw(max maxstr);

my $maxn = max( 3, 5, 10, 4, 6 );
my $maxt = maxstr( @strings );
{% endhighlight %}

Use <tt>shuffle</tt> to randomise order of elements in a list:
{% highlight perl %}
use List::Util qw(shuffle);

my @numbers = shuffle(1..1000);
{% endhighlight %}

Use <tt>List::MoreUtils</tt> module for more advanced subroutines.

Match a&nbsp;condition with <tt>none</tt>, <tt>any</tt>, <tt>all</tt>:

{% highlight perl %}
use List::MoreUtils qw(none any all);

if (none { $_ < 0 } @numbers) {
    print "No elements less then 0\n";
} elsif (any { $_ > 50} @numbers) {
    print "Some elements over 50\n";
} elsif (all { $_ < 10} @numbers) {
    print "All elements are less than 10\n";
}
{% endhighlight %}

Process <i>n</i> items at a&nbsp;time with <tt>natatime</tt>:

{% highlight perl %}
use List::MoreUtils qw(natatime);

my $iterator = natatime 3, @array;

while ( my @triad = $iterator->() ) {
    print "Processing triad: @triad\n";
}
{% endhighlight %}

Combine two or more lists interweaving the elements with <tt>mesh</tt>:

{% highlight perl %}
use Lists::MoreUtils qw(mesh);

my @abc ='a'..'z';
my @num = 1 .. 20;
my @din = qw( dino );

my @arr = mesh @abc, @num, @din;

# arr = ( a 1 dino b 2  c 3 ... )
{% endhighlight %}

<h3>References</h3>

{% bibliography --file references --cited %}
