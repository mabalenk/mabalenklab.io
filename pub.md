---
layout:     page
title:      Publications
permalink: /pub/
order:      5
---

{% include toc.html %}

# Refereed Journal Publications

{% bibliography --query @article %}

# Refereed Conference Proceedings[^1]

{% bibliography --query @inproceedings %}

# Technical Reports

{% bibliography --query @techreport %}

# Seminar Presentations[^1]

{% bibliography --query @conference %}

# Poster Presentations[^1]

{% bibliography --query @misc %}

# Theses

{% bibliography --query @phdthesis %}

[^1]: Symbol "*" indicates the presenter.
