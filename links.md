---
layout:     page
title:      Links
permalink: /links/
order:      10
---

<h3>Mathematics</h3>

<a href="https://khudian.net">Hovhannes Khudaverdian</a><br>
<a href="https://nhigham.com">Nick Higham</a><br>
<a href="https://personalpages.manchester.ac.uk/staff/j.montaldi">James Montaldi</a><br>
<a href="https://bazlov.uk">Yuri Bazlov</a>


<h3>Gnuplot</h3>

<a href="http://www.gnuplotting.org">Gnuplotting</a><br>
<a href="http://lowrank.net/gnuplot/index-e.html">Not so frequently asked questions</a>


<h3>Vim</h3>

<a href="https://github.com/felipec/notmuch-vim">Notmuch-Vim</a>


<h3>Manual</h3>

<a href="http://cht.sh">cht.sh</a>


<h3>Perl</h3>

<a href="https://www.learning-perl.com">Learning Perl</a><br>
<a href="https://www.intermediateperl.com">Intermediate Perl</a><br>
<a href="https://www.masteringperl.org">Mastering Perl</a><br>
<a href="http://perlpowertools.com">Perl Power Tools</a>


<h3>Community</h3>

<a href="https://stackoverflow.com">StackOverflow</a><br>
<a href="https://tilde.club">tilde.club</a><br>
<a href="https://drop.com">Drop</a>


<h3>Mac</h3>

<a href="https://ports.macports.org">MacPorts</a>


<h3>Linux</h3>

<a href="https://archlinux.org">ArchLinux</a>


