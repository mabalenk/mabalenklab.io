---
layout:    page
title:     Interview
permalink: /interview/
order:     9
---

<script type="text/javascript">
<!--
  function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
      e.style.display = 'none';
    else
      e.style.display = 'block';
  }
//-->
</script>

Below are solutions to the technical interview questions with <a href="http://www.intel.com">Intel.</a> I failed this interview on April&nbsp;25th, 2019.

<ol>
  <li>Write a&nbsp;function to calculate the depth of a&nbsp;linked list.</li>

  <li>Given a&nbsp;range of numbers specified by two integers \(a\) and \(b,\) write a&nbsp;program to calculate the number of <i>whole squares</i> in this range. A&nbsp;whole square is a&nbsp;number, whose square is an&nbsp;integer. For example, a&nbsp;range from \(1\) to \(4\) contains two whole squares \(1\) and \(4,\) since \(1^2=1\) and \(2^2=4\) are both integers.</li>
  
  <a href="#task2" onclick="toggle_visibility('task2');">Solution&nbsp;<i style="text-indent: 0" class="fa fa-angle-double-right" aria-hidden="true"></i></a>

  <div id="task2" style="display: none;">
  {% highlight c %}
  #include <math.h>
  #include <stdlib.h>
  #include <stdio.h>
  
  int main(int argc, char **argv) {
  
      if (argc == 3) {
  
          int const base = 10;
          char *endptr;
  
          // read whole squares range from command-line arguments
          long f0 = strtoul(argv[1], &endptr, base);
          long f1 = strtoul(argv[2], &endptr, base);
      
          long g0 =  ceil(sqrt(f0));
          long g1 = floor(sqrt(f1));
      
          printf("No. of whole squares in [%d,%d]: %d", f0, f1, g1-g0+1);
          return EXIT_SUCCESS;
      }
      else {
          fprintf(stderr, "Error: Incorrect no. of arguments supplied!");
          return EXIT_FAILURE;
      }
  }
  {% endhighlight %}
  </div>

  <li>Develop a&nbsp;function to rotate a&nbsp;given matrix 90&deg; clockwise. The matrix rotation should be performed in-place. The matrix elements are stored columnwise in a&nbsp;dynamically allocated one-dimensional array.</li>
</ol>
