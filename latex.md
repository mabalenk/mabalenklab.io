---
layout:     page
title:      LaTeX
permalink: /latex/
order:      8
---

# Introduction to [\\(\LaTeX\\)](http://www.latex-project.org) [<i class="fa fa-download" aria-hidden="true"></i> (pdf)]({{ site.url }}/assets/latex/abalenkovs--intro_latex_v1.2.pdf)

I developed and delivered a&nbsp;practice-oriented one day course "Introduction to [\\(\LaTeX\\)](http://www.latex-project.org)" for postgraduate students. The lecture notes and handouts are available below:

<table>
<tr>
  <td>1.</td>
  <td>Introduction to <a href="http://www.latex-project.org">\(\LaTeX\)</a></td>
  <td><a href="{{ site.url }}/assets/latex/01_intro.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>2.</td>
  <td>Mathematics in <a href="http://www.latex-project.org">\(\LaTeX\)</a></td>
  <td><a href="{{ site.url }}/assets/latex/02_math.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>3.</td>
  <td>Graphics and Tables in <a href="http://www.latex-project.org">\(\LaTeX\)</a></td>
  <td><a href="{{ site.url }}/assets/latex/03_graph.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>4.</td>
  <td>Bibliography Management </td>
  <td><a href="{{ site.url }}/assets/latex/04_bib.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>5.</td>
  <td>Using the <a href="https://www.ctan.org/pkg/muthesis"><code>muthesis</code></a> <a href="http://www.latex-project.org">\(\LaTeX\)</a> Class</td>
  <td><a href="{{ site.url }}/assets/latex/05_thesis.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>6.</td>
  <td>Using the <a href="https://www.ctan.org/tex-archive/macros/latex/contrib/IEEEtran"><code>IEEEtran</code></a> <a href="http://www.latex-project.org">\(\LaTeX\)</a> Class</td>
  <td><a href="{{ site.url }}/assets/latex/06_tran.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>7.</td>
  <td>Creating Presentations with <a href="https://www.ctan.org/tex-archive/macros/latex/contrib/beamer"><code>beamer</code></a></td>
  <td><a href="{{ site.url }}/assets/latex/07_beamer.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>8.</td>
  <td>Creating Posters with <a href="https://www.ctan.org/pkg/sciposter"><code>sciposter</code></a></td>
  <td><a href="{{ site.url }}/assets/latex/08_sciposter.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>9.</td>
  <td>Creating Indices with <a href="https://www.ctan.org/pkg/makeidx"><code>makeidx</code></a></td>
  <td><a href="{{ site.url }}/assets/latex/09_index.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>10.</td>
  <td>Creating Glossaries with <a href="https://www.ctan.org/pkg/glossaries"><code>glossaries</code></a></td>
  <td><a href="{{ site.url }}/assets/latex/10_glossaries.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
<tr>
  <td>11.</td>
  <td>Conclusion</td>
  <td><a href="{{ site.url }}/assets/latex/11_concl.pdf"><i class="fa fa-download" aria-hidden="true"></i> (pdf)</a></td>
</tr>
</table>
