---
layout:     page
title:      How to
permalink: /howto/
order:      9
---

This is a collection of "command line wisdom"—short manuals with practical advice on how to accomplish certain tasks with <i class="fa fa-linux" aria-hidden="true"></i> [Linux](http://www.linux.org)/Unix <i class="fa fa-terminal" aria-hidden="true"></i> command-line utilities:

# A
[a2ps]({{ site.url }}/howto/a2ps)<br/>
[accurev]({{ site.url }}/howto/accurev)<br/>
[ape2flac]({{ site.url }}/howto/ape2flac)<br/>
[ape2wav]({{ site.url }}/howto/ape2wav)<br/>
[arch-restore]({{ site.url }}/howto/arch-restore)<br/>
[audiocd2wav]({{ site.url }}/howto/audiocd2wav)<br/>
[awk]({{ site.url }}/howto/awk)

# B
[backup-data]({{ site.url }}/howto/backup-data)<br/>
[bash]({{ site.url }}/howto/bash)<br/>
[bc]({{ site.url }}/howto/bc)<br/>
[biblatex]({{ site.url }}/howto/biblatex)<br/>
[blkid]({{ site.url }}/howto/blkid)<br/>
<i class="fa fa-bluetooth" aria-hidden="true"></i> [bluetooth]({{ site.url }}/howto/bluetooth)

# C
[check-hdd]({{ site.url }}/howto/check-hdd)<br/>
[cmake]({{ site.url }}/howto/cmake)<br/>
[cmp]({{ site.url }}/howto/cmp)<br/>
[convert]({{ site.url }}/howto/convert)<br/>
[cpu-throttling]({{ site.url }}/howto/cpu-throttling)<br/>
[ctags]({{ site.url }}/howto/ctags)<br/>
[curl]({{ site.url }}/howto/curl)

# D
[detex]({{ site.url }}/howto/detex)<br/>
[du]({{ site.url }}/howto/du)

# E
[escputil]({{ site.url }}/howto/escputil)

# F
[fdisk]({{ site.url }}/howto/fdisk)<br/>
[ffmpeg]({{ site.url }}/howto/ffmpeg)<br/>
[flac]({{ site.url }}/howto/flac)<br/>
[fsck]({{ site.url }}/howto/fsck)

# G
<i class="fa fa-git-square" aria-hidden="true"></i> [git]({{ site.url }}/howto/git)

# H
[hg]({{ site.url }}/howto/hg)

# K
[kde-taskbar]({{ site.url }}/howto/kde-taskbar)

# L
[lowercase]({{ site.url }}/howto/lowercase)

# Z
<a href="{{ site.url }}/howto/zypper">zypper</a>
