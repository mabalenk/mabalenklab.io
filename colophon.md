---
layout:     page
title:      Colophon
permalink: /colophon/
order:      12
---

This website has been created with [Jekyll](https://jekyllrb.com) using the theme [Minima](https://github.com/jekyll/minima). [\\(\LaTeX\\)](https://www.latex-project.org) is supported by means of [MathJax](https://www.mathjax.org). Bib\\(\TeX\\) bibliography is included via the [Jekyll-Scholar](https://github.com/inukshuk/jekyll-scholar) Gem. Preliminary plain text and [\\(\LaTeX\\)](https://www.latex-project.org) to [Markdown](https://daringfireball.net/projects/markdown) conversion has been performed with [`pandoc`](http://www.pandoc.org). Text is typeset with the [Helvetica Neue](https://www.myfonts.com/fonts/linotype/neue-helvetica), [Helvetica](https://www.myfonts.com/fonts/linotype/helvetica), [Arial](https://www.myfonts.com/fonts/mti/arial) and [Sans-Serif](https://www.myfonts.com/fonts/adobe/rotis-sansserif) fonts. Icons are from the [<i class="fa fa-font-awesome" aria-hidden="true"></i>Font Awesome](http://fontawesome.io) collection and little flags in blog posts&mdash;from the <a href="http://flag-icon-css.lip.is">Flag-Icon-CSS</a> collection. Blog pagination is enabled via the [Jekyll-Paginate](https://github.com/jekyll/jekyll-paginate) Gem.
