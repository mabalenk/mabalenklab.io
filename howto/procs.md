CPU specifications
==================

Kilburn: Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz, Sandy Bridge (Desktop)
Galerkin: Intel(R) Core(TM) i7-4770HQ CPU @ 2.20GHz, Haswell-H, Crystal
Well (Mobile) Dancer: Intel(R) Xeon(R) CPU E5520 @ 2.27GHz, Nehalem EP
Arc01: Intel(R) Xeon(R) CPU E5-2650 v3 @ 2.30GHz, Haswell Wonder2:
Intel(R) Xeon(R) CPU E5-2697 v2 @ 2.70GHz, Ivy Bridge

