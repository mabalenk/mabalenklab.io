HOWTO: Install missing binaries of LLVM
=======================================

a)  Run from 'build' directory of LLVM:

    sudo cp -frv count FileCheck lli-child-target llvm-lit\
     llvm-PerfectShuffle not yaml-bench /opt/llvm/bin/.


