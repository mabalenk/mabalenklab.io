HOWTO: Print text file from command line
========================================

1)  Convert a document into a PS-file before printing:

    pdf2ps <file>.pdf

2)  Print normal text on a printer:

    a2ps -1 -P <printer_name>\_duplex -s duplex <file_name>.ps
    --pages=\<1-5\>

3)  List printer queue---documents waiting to be printed out:

    lpq -P <printer_name>

4)  Send document to a printer:

    lpr -P <printer_name>\_duplex <file>.ps

5)  Print a range of pages:

    lpr -o page-ranges=1-4,7,9-12 <file>.ps

6)  Print even pages only:

    lpr -o page-set=even <file>.ps

7)  Print odd pages only:

    lpr -o page-set=odd <file>.ps

8)  Print test page (on openSUSE 11.3):

    lpr /usr/share/cups/data/testprint

http://www.cups.org/documentation.php/options.html

HOWTO: Print 4x4 presentation slides @ IMP-MO-001
=================================================

a)  long side, 4x4, left--right bottom--top

