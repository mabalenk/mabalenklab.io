HOWTO: Install Linux on UEFI computers
======================================

1)  Set BIOS to use default optimised settings:

    boot type UEFI, secured boot off

2)  Press F12 to enter boot device menu and select UEFI DVD

3)  Create separate partition for boot loader '/boot/efi'

4)  Install EFI version of GRUB2 (GRUB2-EFI)


