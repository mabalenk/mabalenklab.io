HOWTO: Install parallel HDF5
============================

a)  Configure HDF5 with configuration script
    '\~/bin/h5-configure-par.sh' or

    MPICH=/opt/mpich/bin PREFIX=/opt/hdf5

    export F77=$MPICH/mpifort  export F90=$MPICH/mpifort export
    F9X=$MPICH/mpifort  export FC=$MPICH/mpifort export
    CC=$MPICH/mpicc  export GCC=$MPICH/mpicc export
    FFLAGS="-I/usr/include/" export CFLAGS="-I/usr/include/" export
    LDFLAGS="-L/opt/mpich/lib/" export CPPFLAGS="-I/opt/mpich/include/"
    export LIBS="-lmpi"

    echo "F77=$F77"  echo "F90=$F90" echo "F9X=$F9X"  echo "FC=$FC" echo
    "CC=$CC"  echo "GCC=$GCC" echo
    "FFLAGS=$FFLAGS"  echo "CFLAGS=$CFLAGS" echo
    "LDFLAGS=$LDFLAGS"  echo "CPPFLAGS=$CPPFLAGS" echo -e
    "LIBS=\$LIBS\n"

    ./configure --prefix=\$PREFIX --enable-fortran --enable-fortran2003\
     --enable-production --build=x86\_64\
     --enable-parallel 2\>&1 | tee c.txt

    \# --with-zlib=/opt/zlib/include,/opt/zlib/lib\
     \# --with-szlib=/opt/szip/include,/opt/szip/lib

b)  Build HDF5

    make -j -l6 2\>&1 | tee m.txt // use parallel compillation (GNU make
    only)

c)  Check HDF5 compillation

    make check 2\>&1 | tee ch.txt

d)  Install HDF5

    sudo make install 2\>&1 | tee mi.txt

e)  Verify installation

    sudo make check-install 2\>&1 | tee chi.txt

f)  Add HDF5 libraries to environment variables // Add to '.bashrc'

    PATH=/opt/hdf5/bin:$PATH ; export PATH  LD_LIBRARY_PATH=/opt/hdf5/lib:$LD\_LIBRARY\_PATH
    ; export LD\_LIBRARY\_PATH

g)  Find out HDF5 configuration settings in file 'libhdf5.settings' in
    '/lib' directory of HDF5 installation


