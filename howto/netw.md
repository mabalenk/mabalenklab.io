HOWTO: Find out network configuration
=====================================

1)  IP address, MAC address and netmask

/sbin/ifconfig

ip arr show // on Arch Linux

cat /sys/class/net/device\_name/address // on Arch Linux

2)  DNS server, hosts file and DNS look up order

cat /etc/resolve.conf cat /etc/hosts

3)  Gateway

ip route show

4)  Hostname

cat /etc/sysconfig/network

http://fclose.com/b/linux/2350/finding-out-linux-network-configuration-information/

HOWTO: Setup direct LAN between two Linux computers
===================================================

1)  Execute on first computer as root

    ifconfig eth0 up ifconfig eth0 inet 192.168.0.1 netmask
    255.255.255.0 arp

2)  Execute on second computer as root

    ifconfig eth0 up ifconfig eth0 inet 192.168.0.2 netmask
    255.255.255.0 arp

3)  If necessary update routing table on both computers

    route add -net 192.168.0.0 netmask 255.255.255.0 dev eth0 route add
    -net 192.168.0.0 netmask 255.255.255.0 dev eth0

http://www.linuxquestions.org/questions/linux-networking-3/setting-up-a-direct-lan-between-2-pcs-358657/

