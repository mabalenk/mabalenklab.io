HOWTO: Install MPICH:
=====================

a)  Use 'mpich-configure.sh' script or configure with following switches

    FC=ifort CC=icc CXX=icpc\
     CFLAGS="-D\_LARGEFILE\_SOURCE -D\_LARGEFILE64\_SOURCE
    -D\_FILE\_OFFSET\_BITS64"   // parallel HDF5 requirements
    ./configure --prefix=/opt/mpich --enable-fast=O3,ndebug\
     --disable-error-checking --without-timing --without-mpit-pvars\
     --enable-fortran=all --enable-romio --build=x86\_64\
     --enable-sharedlibs=gcc 2\>&1 | tee c.txt

b)  Compile

    make 2\>&1 | tee m.txt OR make -j 8 2\>&1 | tee m.txt // use
    parallel compillation (GNU make only)

c)  Install

    sudo make install 2\>&1 | tee mi.txt

d)  Check installation

    make installcheck 2\>&1 | tee mic.txt

e)  Add MPICH binary and library directories to environment variables

    export
    PATH=/opt/mpich/bin:$PATH  // put into '~/.bashrc' OR '~/.profile'  export (DY)LD_(FALLBACK)_LIBRARY_PATH=/opt/mpich/lib:$(DY)LD\_(FALLBACK)*LIBRARY*PATH

f)  Refresh environment variables

    source \~/.bashrc

g)  Check MPICH compilers are found

    which mpicc | mpiexec

    OR

    mpicc | mpicxx | mpifort --version

h)  Let linker know about custom location of MPI libraries. Create

    /etc/ld.so.conf.d/mpich.conf containing '/opt/mpich/lib'

i)  Refresh linker cache

    sudo ldconfig

j)  Check MPI libraries are found in custom location

    ldconfig -p | grep "libmpi.so"

k)  Compile tests

    make testing 2\>&1 | tee t.txt OR make -j 8 testing 2\>&1 | tee
    t.txt // use parallel compillation (GNU make only)

l)  Launch MP process daemon, compile and test example

    mpd & mpicc examples/cpi.c -o examples/cpi

    mpiexec -n 4 ./cpi mpiexec -f machinefile -n <number> ./examples/cpi

m)  Machine file format

    host1 host2:2 host3:4 \# Random comments host4:1

n)  Uninstall MPICH

    cd \~/install/mpich/mpich-3.2b3 // source directory make uninstall
    make clean


