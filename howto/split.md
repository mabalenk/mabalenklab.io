HOWTO: Split large files into chunks of 2GB
===========================================

a)  Split large file

    split -b 2147483648 file.iso

b)  Stitch chunks together to restore original file

    cat xaa xab xac \> file.iso


