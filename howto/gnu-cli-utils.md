HOWTO: Install and use GNU command line utilities on Mac OS X
=============================================================

Install the GNU Command Line Tools
----------------------------------

sudo port install coreutils

most essential UNIX commands
----------------------------

binutils diffutils ed +with\_default\_names findutils
+with\_default\_names gawk gnu-indent +with-default-names gnu-sed
+with-default-names gnu-tar +with-default-names gnu-which
+with-default-names gnutls grep +with-default-names gzip screen watch
wdiff wget

Some command line tools already exist on OS X, but you may wanna a newer
version:

brew install bash brew install emacs brew install gdb \# gdb requires
further actions to make it work. See `brew info gdb`. brew install
gpatch brew install m4 brew install make brew install nano

As a complementary set of packages, the following ones are not from GNU,
but you can install and use a newer version instead of the version
shipped by OS X:

brew install file-formula brew install git brew install less brew
install openssh brew install perl518 \# must run "brew tap
homebrew/versions" first! brew install python brew install rsync brew
install svn brew install unzip brew install vim --override-system-vi
brew install macvim --override-system-vim --custom-system-icons brew
install zsh

https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

