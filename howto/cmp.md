---
layout: page
permalink: /howto/cmp/
---

HOWTO: Check if two files are identical[^1]
===========================================

a) `cmp --silent <file1> <file2> || echo "files are different"`

[^1]: Based on [A](http://stackoverflow.com/questions/12900538/unix-fastest-way-to-tell-if-two-files-are-the-same).
