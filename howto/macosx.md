HOWTO: Change system language in Mac OS X
=========================================

1)  Apple Logo --\> System Preferences

2)  Icon 'International'

3)  Edit list of languages

4)  Place 'English' above 'French'

5)  Logout, login

OR

5)  Close international setings window

6)  Apple Logo --\> Force Quit Finder

7)  Select 'Finder', Click 'Relaunch'

http://davidtse916.wordpress.com/2007/11/19/changing-the-system-language-in-mac-os-x/

