HOWTO: Install Tau Profiler
===========================

1)  Install PDT

    ./configure -GNU -prefix=\$SERT/software/pdt -compdir=intel gmake
    gmake install

2)  Modify environment variables to contain PDT binary and library files

    export PATH=$SERT/software/pdt/apple/intel/bin:$PATH export
    DYLD\_FALLBACK\_LIBRARY\_PATH=$SERT/software/pdt/apple/intel/lib:$DYLD\_FALLBACK\_LIBRARY\_PATH

3)  Install PAPI

    CC=gcc F77=gfortran CPPFLAGS=-I/usr/include/malloc\
     ./configure --prefix=\$SERT/software/papi make make test
    ./run\_tests.sh -v make install; make install-man; make
    install-tests OR make install-all

4)  Install Cube

    md vpath cd vpath ../configure --prefix=\$SERT/software/cube

5)  Install Tau

    configure


