---
layout: page
permalink: /howto/bash/
---

HowTo: Use bash effectively
===========================

a) Create loop with variable range in bash:

{% highlight bash %}
for (( s=1; s<=${#sources[*]}; s++ ))
do
  ...
done
{% endhighlight %}

b) Perform action for each file of given type:

{% highlight bash %}
for texfile in *.tex; do ./makedoc; done
{% endhighlight %}
