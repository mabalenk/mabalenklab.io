HOWTO: Use Maple over a network
===============================

1)  Login via SSH with your Telecom username and password:

ssh -X mabalenk@vnc.svc.enst-bretagne.fr

2)  Setup Maple environment:

SETUP MAPLE17

3a) Launch standard worksheet Maple:

xmaple

3b) Launch classic worksheet Maple:

maple -cw

3c) Launch command line Maple:

maple

HOWTO: Use Maple Keyboard Bindings
==================================

Ctrl + B Cursor Left Ctrl + F Cursor Right Ctrl + A Move to the
Beginning of the Line Ctrl + E Move to the End of the Line Ctrl + W Move
One Word Right Ctrl + Y Move One Word Left Ctrl + ] Move to Matching
Parenthesis, Brace, or Square Bracket Ctrl + D Delete (to Right of
Cursor), or Exit Maple (if on a Blank Line) Ctrl + H Backspace (to Left
of Cursor) Ctrl + X or Clear the Line Ctrl + G Ctrl + K Clear to the End
of Line Ctrl + U Undo Changes to the Line Ctrl + P Previous Command From
the History Ctrl + N Next Command From the History Ctrl + R Find
Matching Command From the History Ctrl + Space or Command Completion Tab
Ctrl + T Show Completion Matches Ctrl + C Interrupt the Currently
Executing Command Ctrl + \_ Stop the Currently Executing Command in the
Debugger Ctrl + V Toggle Insert or Overwrite Mode Ctrl + L Redraw the
Current Prompt and Any Text Entered

http://www.maplesoft.com/support/help/Maple/view.aspx?path=commandline/reference/shortcutkeys

HOWTO: Use Maple
================

1)  End statement with colon : or semicolon ;

2)  Use \^ or \*\* for exponentiation

3)  Use % to operate on the last result or %% to operate on the
    next-to-last result.

4)  Type 'quit' to quit

5)  Use evalf(%,3) for evaluation using floating-point arithmetic, where
    3---no. of significant digits

6)  Help for a particular command ? evalf OR ? plot, color

7)  Use := to define variables, e.g. A := 5; B := 2;

8)  Variable names are case sensitive

9)  Set number of significant figures Digits := 4;

10) Value of built-in function evalf(ln(10), 4);

11) Expression: string of constants, variables, mathematical operators,
    e.g. 5*x\^2 - 2*y\^2 = 3*cos(x*y);

12) Function: relationship for a variable z(x,y) = 2\*x/y;

13) Use := to define an expression, f := x\^2; and 'subs()' to
    substitute a value into it, subs(x = 5, f).

14) Define function g := x -\> 1/(x+1), evaluate function at x = 4,
    g(4);

15) Convert expression into function f := unapply(f,x); Evaluate
    function at x = 5, f(5);

16) Convert function back into expression, f := f(x);

17) Define equations

    eq1 := 2*x1 - 5*x2 = 12; eq2 := 12*x1 + 4*x2 = 17;

18) Solve sets of linear algebraic equations

    sol := solve({eq1, eq2}, {x1,x2}); sol := fsolve({eq1, eq2},
    {x1,x2});

19) Extract solutions

    sol[2]; OR rhs(sol[2]);

20) Specify range(s) for unknown(s) in nonlinear equation(s)

    eq := x = exp(-x); sol := fsolve(eq, x, x=0..1);

    f := sin(x+y) - y\*exp(x) = 0; g := x\^2 - y = 2;

    solfg := fsolve({f,g},{x,y},{x = -1..1, y = -2..0});

    Use infinity OR -infinity to specify semi-infinite range limit.

21) Find roots of polynomials

    p := 6*x\^4 - 7*x\^3 + 6\*x\^2 - 1 = 0;

    sol := solve(p,x); sol := fsolve(p, x, complex);

22) Imaginary unit is 'I'

23) Access plotting package

    with(plots);

24) Plot tabular data

    T(C) 9.7 20.4 31.0 41.5 51.1 V(m\^3/kg) 108.7 56.4 31.1 18.1 11.5

    VTdata := [ [9.7, 108,7], [20.4, 56.4], [31.0, 31.1], [41.5, 18.1],
    [51.1, 11.5] ]

    plot(VTdata, T = 0..60, V = 0.120, style = point)

25) Plot two functions

    f := x -\> sin(x); g := x -\> 0.25\*x - 1;

    plot( [f(x), g(x)], x=0..4, color=[red,blue], style=[point,line] );

26) Insert text on a plot

    fggraph := plot( [f(x), g(x)], x=0..4, color=[red,blue],
    style=[point,line] );

    labelf := textplot( [0.8, 0.5, `f(x)`] ); labelg := textplot( [2,
    -0.5, `g(x)`], align = {ABOVE, LEFT} );

    display( [fggraph, labelf, labelg] );

27) Unassign a variable 'k'

    k := 'k';

28) Perform algebraic simplification

    k := x\*y + y\^2; p := k/(x+y);

    simplify(p);

29) Differentiate an expression with diff()

    f := x\^3 + 5*exp(-2*x);

    df := diff(f,x); d2f := diff(f,x,x);

    e1 := evalf(subs(x=1,df), 4); e2 := evalf(subs(x=1,d2f), 4);

30) Differentiate a function with D()

    g := x -\> x\^3 + 5*exp(-2*x);

    dg := D(g); d2g := D(D(g)); OR d2g := D(dg);

    evalf(dg(1), 4); evalf(d2g(1), 4);

31) Indefinite integration of an expression with int(f,x)

    f := 3*x\^2 - 10*exp(-2\*x);

    F := int(f,x);

    evalf(subs(x=1,F), 4);

    diff(F, x);

    F := unapply(F,x);

    D(F);

32) Definite integration of an expression with int(f,x=a..b)

    f := 3*x\^2 - 10*exp(-2\*x);

    A := int(f, x=0..1);

    evalf(A,4);

33) Numerical integration

    f := exp(-x\^3);

    F := int(f, x=0..2);

34) Ordinary differential equations

    // equations deqs := diff(Ca(t),t) = -(1/10)*Ca(t), diff(Cb(t),t) =
    (1/10)*Ca(t) - (2/10)*Cb(t), diff(Cc(t),t) = (2/10)*Cb(t);

    // initial conditions inits := Ca(0) = 1, Cb(0) = 0, Cc(0) = 0;

    // list of dependent variable names fcns := [Ca(t), Cb(t), Cc(t)];

    // solve equations analytically dsolve({deqs, inits}, fcns);

    // create functions for Ca(t), Cb(t) and Cc(t) Ca := t -\>
    exp(-1/10*t); Cb := t -\> exp(-1/10*t) - exp( -1/5*t); Cc := t -\> 1
    - 2*exp(-1/10*t) + exp(-1/5*t);

    // generate a plot of the concentrations vs. time from t=0 to t=40 s
    with(plots);

    P := plot([Ca(t), Cb(t), Cc(t)], t=0..40, title =
    `CONCENTRATION (MOL/L) VS. TIME(SEC)`, titlefont = [HELVETICA, BOLD,
    14], labels = [`t`, `C`], style=[point, point, point],
    symbol=[circle, box, diamond], color=[red,green,blue]):

    // label curves on the plot labela := textplot( [.8, .5, `Ca`] ):
    labelb := textplot( [4, .26, `Cb`] ): labelc := textplot( [20, .8,
    `Cc`] ):

    // display plot with curve labels display([P, labela, labelb,
    labelc]);

    // generate numerical solutions of the equations and plot them //
    turn Ca, Cb and Cc from functions back into variable names Ca :=
    'Ca': Cb := 'Cb': Cc := 'Cc':

    // add differential equations package with(DEtools):

    // create and store separate plots of the dependent variables vs.
    the independent variable Caplot := DEplot( [deqs], fcns, t=0..40,
    [[inits]], scene=[t,Ca], linecolor=red, stepsize=0.2, arrows=none ):
    Cbplot := DEplot( [deqs], fcns, t=0..40, [[inits]], scene=[t,Cb],
    linecolor=green, stepsize=0.2, arrows=none ): Ccplot := DEplot(
    [deqs], fcns, t=0..40, [[inits]], scene=[t,Cc], linecolor=blue,
    stepsize=0.2, arrows=none ):

    display( [Caplot, Cbplot, Ccplot, labela, labelb, labelc] );

    // generate and store numerical solutions of the three equations
    soln := dsolve({deqs, inits}, fcns, numeric);

    // display the concentrations at a specified time (t=5), each with
    four significant figures. evalf(soln(5), 4);

    // display a table of concentrations at times 0, 2, 4,..., 20 with
    four significant figures. for k from 0 to 10 do evalf(soln(2\*k),
    4); od;

http://www4.ncsu.edu/unity/lockers/users/f/felder/public/tutorials/maple1.htm
http://www4.ncsu.edu/unity/lockers/users/f/felder/public/tutorials/maple2.htm
http://rt.uits.iu.edu/visualization/analytics/math/maple-getting-started.php

