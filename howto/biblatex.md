---
layout: page
permalink: /howto/biblatex/
---

HowTo: Use [Bib\\(\LaTeX\\)](https://www.ctan.org/pkg/biblatex)[^1]
===================================================================

a) Document structure:

{% highlight latex %}
\documentclass{...}

\usepackage[...]{biblatex}

\addbibresource{references.bib}

   
\begin{document}

\cite{...}

...

\printbibliography

\end{document}
{% endhighlight %}

b) Compilation procedure:

    (pdf)latex article.tex
         biber article.bcf
    (pdf)latex article.tex

[^1]: Based on [Bib\\(\LaTeX\\)](https://www.ctan.org/pkg/biblatex) [manual](http://mirrors.ctan.org/macros/latex/contrib/biblatex/doc/biblatex.pdf), ver. 2.8, 21/10/2013, Sec. 3.11, p. 100.
