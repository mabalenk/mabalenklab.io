---
layout: page
permalink: /howto/bluetooth/
---

HowTo: Setup and use [Bluetooth](https://www.bluetooth.com) devices (e.g. [Apple Magic Mouse](https://www.apple.com/uk/shop/product/MLA02Z/A/magic-mouse-2)) on [Linux](http://www.linux.org)[^1]
====================================================================================================

a) Install `bluez` and `bluez-firmware` packages.

b) Start bluetooth `systemd` service:

    systemctl start bluetooth

c) Enable bluetooth service at boot time:

    systemctl enable bluetooth

d) Configure bluetooth device via command line:

    /usr/bin/bluetoothctl

e) Setup bluetooth device with `bluetoothctl`:

    power on # power on bluetooth controller
    devices  # view MAC addresses of device with which to pair
    scan on  # enable device discovery mode
    agent on # enable agent
    pair    <MAC address> # pair with given device
    trust   <MAC address> # manually trust device without PIN
    connect <MAC address> # establish connection

f) Create `udev` rule in `/etc/udev/rules.d/10-local.rules` to keep
    device active after reboot:

    # Set bluetooth power up
    ACTION=="add", KERNEL=="hci0", RUN+="/usr/bin/hciconfig hci0 up"

g) Enable automatic switch on for device after suspend/resume-cycle by
    creating systemd service in `/etc/systemd/system/bluetooth-auto-power@.service`:

    [Unit]
    Description=Bluetooth auto power on
    After=bluetooth.service sys-subsystem-bluetooth-devices-%i.device suspend.target

    [Service]
    Type=oneshot
    ExecStart=/usr/bin/hciconfig %i up

    [Install]
    WantedBy=suspend.target

[^1]: Based on [Arch Linux](http://www.archlinux.org) [Bluetooth](http://bluetooth.com) [wikipage](https://wiki.archlinux.org/index.php/Bluetooth).
