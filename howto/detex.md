---
layout: page
permalink: /howto/detex/
---

HowTo: Use [Detex](https://ctan.org/tex-archive/support/detex) options
======================================================================

`-n`  # do not follow `\input` and `\include` commands<br/>
`-l`  # force \\(\LaTeX\\) mode<br/>
`-t`  # force \\(\TeX\\) mode<br/>
`-e`  # comma-separated list of environments to ignore (defaults: `array`, `eqnarray`, `equation`, `figure`, `mathematica`, `picture`, `table`, `verbatim`)<br/>
`-c`  # echo arguments to `\cite`, `\ref` and `\pageref`<br/>
`-w`  # word list
