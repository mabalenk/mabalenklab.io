HOWTO: Install Intel Fortran compiler
=====================================

1)  Create folder /opt/intel/licenses

    mkdir -p /opt/intel/licenses

2)  Copy the file USE\_SERVER.lic to /opt/intel/licenses

    cp -rvf use\_server.lic /opt/intel/licenses

3)  On ArchLinux relink /usr/bin/python to /usr/bin/python2

4)  Execute installation script as root

    sudo ./install.sh

5)  In 'Step 3: Activation' choose 'Option 1: Use existing license'

6)  Add Intel compiler initialisation to '.bashrc' file

    source /opt/intel/bin/compilervars.sh intel64


