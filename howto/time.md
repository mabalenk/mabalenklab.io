HOWTO: Measure program's execution time and save it into file
=============================================================

1)  Launch program executable

    { time <prog_name>; } 2\> <time_file>

HOWTO: Interpret time utility output
====================================

1)  Real is wall clock time---time from start to finish of the call.
    This is all elapsed time including time slices used by other
    processes and time the process spends blocked (for example if it is
    waiting for I/O to complete).

2)  User is the amount of CPU time spent in user-mode code (outside the
    kernel) within the process. This is only actual CPU time used in
    executing the process. Other processes and time the process spends
    blocked do not count towards this figure.

3)  Sys is the amount of CPU time spent in the kernel within the
    process. This means executing CPU time spent in system calls within
    the kernel, as opposed to library code, which is still running in
    user-space. Like 'user', this is only CPU time used by the process.

4)  User+Sys is the actual CPU time the process used.

http://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1

