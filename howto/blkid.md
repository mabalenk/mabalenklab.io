---
layout: page
permalink: /howto/blkid/
---

HOWTO: Use `blkid` effectively
==============================

a) Find out UUID of USB drive:

    sudo blkid -t TYPE=vfat -sUUID
