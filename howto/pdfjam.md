HOWTO: Prepare PDF handouts of presentation slides
==================================================

pdfjam --nup 2x2 msc\_slides.pdf --landscape --frame true --a4paper
--delta "0.25cm 0.25cm" --scale 0.95 --outfile msc\_slides\_4x4.pdf

HOWTO: Extract certain pages from PDF document
==============================================

pdfjam <input file> <page ranges> -o
<output file>

pdfjam input.pdf 1,10-13,19 -o output.pdf

