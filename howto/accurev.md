---
layout: page
permalink: /howto/accurev/
---

HowTo: Use AccuRev
==================

a)  Update local workspace:

    accurev update \<-i\>

b)  Keep modified files:

    accurev keep -m -c@comment.txt

c)  Promote pending files:

    accurev promote -p -c@comment.txt

d)  Revert files to the previous version from the backend stream:

    accurev purge \*.srcf90

e)  Find overlapping files:

    accurev stat -o

f)  Merge overlapping files:

    accurev merge -o

g)  Promote files from the integration to the development stream:

    accurev promote -c@comment.txt -s integration\_username -d
