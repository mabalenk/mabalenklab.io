---
layout: page
permalink: /howto/bc/
---

HowTo: Use `bc` calculator efficiently
======================================

a) Raise number to fractional power or take \\(n\\)th root of number \\(x^y\\):

    echo "e(y*l(x))" | bc -l
