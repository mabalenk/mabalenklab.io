HOWTO: Choose correct size of swap partition
============================================

Amount of RAM in the system Recommended swap spaceRecommended swap space
if allowing for hibernation ---------------------------
---------------------------- --------------------------- 2GB of RAM or
less 2 times the amount of RAM 3 times the amount of RAM 2GB to 8GB of
RAM Equal to the amount of RAM 2 times the amount of RAM 8GB to 64GB of
RAM 0.5 times the amount of RAM 1.5 times the amount of RAM 64GB of RAM
or more 4GB of swap space No extra space needed Original table:

Amount of RAM in the System Recommended Amount of Swap Space 4GB of RAM
or less a minimum of 2GB of swap space 4GB to 16GB of RAM a minimum of
4GB of swap space 16GB to 64GB of RAM a minimum of 8GB of swap space
64GB to 256GB of RAM a minimum of 16GB of swap space 256GB to 512GB of
RAM a minimum of 32GB of swap space

