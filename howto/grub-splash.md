HOWTO: Replace standard GRUB splash screen
==========================================

1)  Install additional packages

    zypper in gfxboot, gfxboot-devel, qemu, syslinux

2)  Unpack gfxboot archive

    cpio -i \</boot/message

3)  Store backup copy of standard GRUB splash image

    cp back.jpg /tmp

4)  Create new splash image (800x600, JPG, back.jpg)

    cp custom.jpg back.jpg

5)  Replace splash screen background image

    gfxboot --add-files back.jpg

6)  Test changes made

    gfxboot --preview

http://en.opensuse.org/SDB:Gfxboot

