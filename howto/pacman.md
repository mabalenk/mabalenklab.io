HOWTO: Clean pacman package cache on ArchLinux
==============================================

a)  Delete all cached versions of each package, except most recent 3

    paccache -r

b)  Remove all cached versions of uninstalled packages

    paccache -ruk0

c)  Query to which package a file belongs

    sudo pacman -Qo time


