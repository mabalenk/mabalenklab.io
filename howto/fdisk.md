---
layout: page
permalink: /howto/fdisk
---

HowTo: Format USB drive
=======================

a) Unmount the USB drive:

    umount /media/disk

b) Delete all previous partitions:

    fdisk /dev/sdb, d

c) Create new partition and change its type to 6 (FAT16) or b (FAT32):

    fdisk /dev/sdb, n, p, 1, t, 6/b, a, 1, w

d) Create FAT{16,32} file system on new partition:

    mkfs -t vfat -F {16,32} /dev/sdb1 -n <disk_label>

e) Create target mounting directory:

    mkdir -p /media/disk

f) Mount USB drive:

    mount /dev/sdb1 /media/disk
