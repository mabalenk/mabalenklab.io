HOWTO: Fit a curve with gnuplot
===============================

1)  define the function to be fit:

    f1(x) = a*exp(b*x+c)

2)  propose an initial guess for parameters

    a = 0.1; b = 0.2; c = 0.3

3)  launch gnuplot to fit the data from file

    fit f1(x) 'force.dat' using 1:2 via a, b, c

HOWTO: Skip header in data file
===============================

1)  plot "file.dat" every ::1 t "title" w l lw 8;

