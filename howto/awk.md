---
layout: page
permalink: /howto/awk
---

HowTo: Work with Awk[^1]
========================

a) Use tab as column separator:

    awk -v OFS='\t' '{print $1, $2}' file.txt

b) Downsample 'anterior/posterior' sources locations:

    awk '($1 % 5 == 0) && ($2 % 5 == 0) && ($3 % 5 == 0) \
    {printf("%5.0f %5.0f %5.0f\n", $1/5, $2/5, $3/5);}' anterior_b > file

c) Skip first two fields and print the rest of line:

    awk '{print substr($0, index($0,$3))}' file.txt

d) Generate multiple consecutive column numbers:

    seq -s ", $" 1 8 | sed 's/.*/{print $&}/'

[^1]: Based on [A](http://unstableme.blogspot.de/2009/09/print-except-few-columns-using-awk-bash.html).
