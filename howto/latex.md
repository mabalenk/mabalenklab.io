HOWTO: Install Fourier-GUT font package
=======================================

a)  Install Adobe Utopia fonts // only on Mac OS X

    sudo port install font-adobe-utopia-type1 OR

    sudo cp -frv \~/install/latex/utopia/
    /opt/local/share/texmf-texlive/fonts/type1/adobe/ sudo mktexlsr

b)  Set environment variable pointing to main TeX MF directory

    export TEXMFMAIN=/usr/share/texmf-dist OR export
    TEXMFMAIN=/opt/local/share/texmf/tex/latex/ // on Mac OS X

c)  Use Makefile to perform installation

    sudo make TGM=TEXMFMAIN install-all

d)  Refresh LaTeX package tree

    sudo texhash

e)  Configure font map

    updmap --enable Map fourier.map

HOWTO: Install new LaTeX packages
=================================

1)  Login as root

    su

2)  Extract files

    latex \*.ins

3)  Create documentation

    (pdf)latex \*.dtx

4)  Create a new directory for a new package

    mkdir -p /usr/share/texmf/doc/latex/<package> mkdir -p
    /usr/share/texmf/tex/latex/<package>

5)  Move all new package files into directories

    mv -v <package>/*.pdf /usr/share/texmf/doc/<package> mv -v
    <package>/* /usr/share/texmf/tex/latex/<package>

6)  Refresh LaTeX package tree

    texhash

HOWTO: Count words in LaTeX document
====================================

1)  detex -ln article.tex | wc -w

HOWTO: Enable Latvian language support
======================================

1)  Install files latv\_base.tar.bz2 and latv\_fonts.tar.bz2:

    sudo tar -xjvf latv\_base.tar.bz2 -C /usr/share/texmf sudo tar -xjvf
    latv\_fonts.tar.bz2 -C /usr/share/texmf

2)  Refresh LaTeX package tree

    sudo texhash

3)  Complement file /usr/share/texmf/tex/generic/babel/babel.sty with
    the folowing line

    \DeclareOption{latvian}{\input{latvian.ldf}}

4)  Complement file /usr/share/texmf/web2c/updmap.cfg
    (/etc/texmf/web2c/updmap.cfg) with the following lines at the end

    \# TTF fonts for LV by Arnis Voitkans, arnis.voitkans@lu.lv Map
    timesnew.map Map arial.map Map cournew.map Map georgia.map Map
    verdana.map Map bookmanold.map Map comic.map Map gothic.map Map
    cumberland.map Map palatino.map

5)  Renew LaTeX font files (\*.map)

    updmap

6)  Activate word hyphenation

    a)  Use TeTeX setup utility program 'texconfig'
    b)  Select 'HYPHENATION'
    c)  Select 'latex'
    d)  Add line 'latvian lvhyphen.tex' to the file
        '/usr/share/texmf/tex/generic/config/language.def'
    e)  Configuration will take place after the text editor closes
    f)  Finish configuration by selecting 'EXIT' in 'texconfig' menu

7)  Correct Unicode definition of letter 'ģ' in package 'inputenc'

    a)  In file '/usr/share/texmf/tex/latex/unicode/data/uni-1.def' OR
        '/usr/share/texmf/tex/latex/ucs/data/uni-1.def' find line

        \uc@dclc{291}{dirty}{\leavevmode\\vbox{\baselineskip ...

    b)  Replace it with

        \uc@dclc{291}{default}{\v g}%

8)  Add to preamble of Latvian documents

    % enable font encodings \usepackage[LV,T1,T2A]{fontenc}

    % enable unicode support \usepackage[utf8]{inputenc}

    % enable support for Latvian, Russian
    \usepackage[latvian,russian]{babel}

http://home.lu.lv/\~drikis/LaTeX-Latviski/latex-latviski.html

