---
layout: page
permalink: /howto/fsck/
---

HowTo: Force `fsck` on boot[^1]
===============================

a) Create `/forcefsck` file to force Linux to perform a full file system check:

    sudo touch /forcefsck; /sbin/reboot

OR

b) Force `fsck` on next boot using `shutdown` command:

    sudo shutdown -rF now

HowTo: Check partitions for errors[^2]
======================================

a) Boot into rescue mode.

b) Login as `root`:

c) Run:

    fsck -p /dev/sda{1..7} badblocks -v /dev/sda{1..7} > bad-blocks

d) Pass `bad-blocks` file to `fsck` command to record bad blocks:

    fsck -t ext4 -l bad-blocks /dev/sda1

HowTo: Fix NTFS partitions under [Linux](http://www.linux.org)
==============================================================

a) `ntfsfix /dev/sda1`

HowTo: Check partitions for errors interactively[^3]
====================================================

a) `testdisk`

[^1]: Based on [A](http://www.cyberciti.biz/faq/linux-force-fsck-on-the-next-reboot-or-boot-sequence).
[^2]: Based on [B](http://www.redhat.com/advice/tips/rescue_mode.html).
[^3]: Based on [C](http://linuxpoison.blogspot.fr/2008/01/howto-check-disk-drive-for-errors-and.html).
