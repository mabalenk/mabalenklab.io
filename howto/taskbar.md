HOWTO: Restore deleted taskbar in KDE
=====================================

kquitapp plasma-desktop rm -fv \~/.kde4/share/config/plasma\*
plasma-desktop &

