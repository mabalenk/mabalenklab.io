HOWTO: Convert SVN reopsitory to Git
------------------------------------

1)  Set up Git

    \# Set default name for git to use when you commit git config
    --global user.name "John Smith"

    \# Set default email for git to use when you commit git config
    --global user.email "john.smith@example.com"

    \# Set git to use the credential memory cache git config --global
    credential.helper cache

2)  Create new project repository 'prj'

    mkdir prj cd prj git init

3)  Extract SVN users information, store it in "users.txt" file

    svn log --xml | grep author | sort -u | perl -pe
    's/.*\>(.*?)\<.\*/\$1 = /' \> users.txt

4)  Complement authors information

    jsmith = John Smith <john.smith@gmail.com> schacon = Scott Chacon
    <schacon@gmail.com>

5)  Perform SVN to Git import. Import from SVN repository on web

    git svn clone https://svn.telecom-bretagne.eu/repository/atreyu
    --authors-file=users.txt --no-metadata -s \~/repository/git/atreyu

6)  Convert SVN tags to proper Git tags

    cp -Rf .git/refs/remotes/origin/tags/\* .git/refs/tags/ rm -Rf
    .git/refs/remotes/origin/tags

7)  Convert rest of references to local Git branches

    cp -Rf .git/refs/remotes/\* .git/refs/heads/ rm -Rf
    .git/refs/remotes

8)  Add new Git server as remote

    git remote add origin git@gitlab.com:mabalenk/atreyu.git

9)  Push Git repository incl. all branches and tags

    git push origin --all

http://git-scm.com/book/es/v2/Git-and-Other-Systems-Migrating-to-Git
https://gitlab.com

