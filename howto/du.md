---
layout: page
permalink: /howto/du/
---

HowTo: Use `du` effectively
===========================

a) Find top 10 largest files or directories[^1]:

    du -a /var | sort -n -r | head -n 10

[^1]: Based on [A](http://www.cyberciti.biz/faq/how-do-i-find-the-largest-filesdirectories-on-a-linuxunixbsd-filesystem).
