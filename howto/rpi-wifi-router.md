HOWTO: Setup software access point (WiFi router)
================================================

1.  Install additional packages

    dhcp sudo vim rfkill zd1211-firmware hostapd hostap-utils iw dnsmasq
    make zip unzip pkgfile gcc htop iptables

2.  Install hostapd that supports RTL8188CUS device

a)  Download patched version of hostapd from

        https://www.dropbox.com/s/fnz0au6nxe3lohy/hostapd-rtl8192cu-0.8-1-armv6h.pkg.tar.xz

b)  Install patched version of hostapd with

        pacman -U hostapd-rtl8192cu-0.8-1-armv6h.pkg.tar.xz

OR

a)  Download Realtek driver for RTL8188CUS from

        http://www.realtek.com.tw/downloads/downloadsView.aspx?Langid=1&PNid=21&PFid=48&Level=5&Conn=4&DownTypeID=3&GetDown=false

b)  Extract files from archive

        unzip RTL8188C_8192C_USB_linux_v4.0.2_9000.20130911.zip

c)  Change to given directory

        cd RTL8188C_8192C_USB_linux_v4.0.2_9000.20130911/wpa_supplicant_hostapd

d)  Extract hostapd files

        tar -xzvf wpa_supplicant_hostapd-0.8_rtw_r7475.20130812.tar.gz

e)  Change to given directory

        cd wpa_supplicant_hostapd-0.8_rtw_r7475.20130812/hostapd

f)  Compile hostapd with Realtek driver

        sudo make
        sudo make install

g)  Copy hostapd files and set permissions

        sudo mv hostapd /usr/sbin/hostapd
        sudo chown root.root /usr/sbin/hostapd
        sudo chmod 755 /usr/sbin/hostapd

3.  Edit /etc/hostapd/hostapd.conf

    ssid=<ssid> wpa\_passphrase=<passwd> interface=wlan0 bridge=br0
    auth\_algs=3 channel=7 driver=rlt871xdrv hw\_mode=g
    logger\_stdout=-1 logger\_stdout\_level=2 max\_num\_sta=5
    rsn\_pairwise=CCMP wpa=2 wpa\_key\_mgmt=WPA-PSK wpa\_pairwise=TKIP
    CCMP

4.  Enable hostapd.service to automatically start hostapd

    sudo systemctl enable hostapd.service sudo systemctl start
    hostapd.service

5.  Setup bridge with netctl Copy example bridge configuration

    cp -rvf /etc/netctl/examples/bridge /etc/netctl/bridge

6.  Edit '/etc/netctl/bridge'

    Description="RPi bridge connection" Interface=br0 Connection=bridge
    BindsToInterfaces=(eth0) IP=dhcp

7.  Start and enable newly created bridge profile

    sudo netctl start bridge sudo netctl status bridge sudo netctl
    enable bridge

8.  Setup Network Address Translation (NAT)

a)  Assign static IPv4 address to interface connected to other machines
    (wlan0)

        ip link set up dev wlan0
        ip addr add 192.168.0.1/24 dev wlan0

    TODO: Use netctl to have static IP assigned at boot

b)  Enable packet forwarding

        sysctl -a | grep forward      # check current packet forwarding settings
        sysctl net.ipv4.ip_forward=1  # temporarily enable packet forwarding

        OR

c)  Edit '/etc/sysctl.d/30-ipforward.conf' to enable packet forwarding
    permanently

        net.ipv4.ip_forward=1
        net.ipv6.conf.default.forwarding=1
        net.ipv6.conf.all.forwarding=1

9.  Enable NAT with iptables

    iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE iptables -A
    FORWARD -i wlan0 -o eth0 -j ACCEPT iptables -A FORWARD -m conntrack
    --ctstate RELATED,ESTABLISHED -j ACCEPT

TODO: Read iptables article to find out how to save the rule and apply
it on boot

10. Install and setup DHCP server

    a)  Edit configuration file /etc/dhcpd.conf to contain

    \# Use Google DNS, change to 139.96.30.100, if a local DNS server is
    installed option domain-name-servers 8.8.8.8; option subnet-mask
    255.255.255.0; option routers 139.96.30.100; subnet 139.96.30.0
    netmask 255.255.255.0 { range 139.96.30.150 139.96.30.250; }

    b)  Start dhcpd daemon with 'dhcpd4.service' using 'systemctl'.
        Enable its automatic start on boot.

        sudo systemctl enable dhcpd4.service sudo systemctl start
        dhcpd4.service

https://wiki.archlinux.org/index.php/Software\_Access\_Point
http://anarsoul.blogspot.com.es/2013/08/access-point-with-raspberry-pi-and.html
https://wiki.archlinux.org/index.php/Bridge\_with\_netctl
http://forums.adafruit.com/viewtopic.php?f=19&t=47716
https://wiki.archlinux.org/index.php/Internet\_sharing
https://wiki.archlinux.org/index.php/Dhcpd

iw list, lstusb, iwconfig

