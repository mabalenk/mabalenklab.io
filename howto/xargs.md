HOWTO: Perform action for every line of input
=============================================

a)  Extract all TAR archives in current directory

    find -type f -name \*.tar.gz -printf "%f\n" | xargs -I % tar -xzvf %


