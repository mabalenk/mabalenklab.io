---
layout: page
permalink: /howto/ffmpeg
---

HowTo: Install `ffmpeg` from source
===================================

    ./configure --enable-gpl --enable-postproc --enable-pthreads \
      --enable-libfaac --enable-libmp3lame --enable-libtheora \
      --enable-libx264 --enable-libxvid --enable-shared --enable-nonfree

    make

    sudo make install
