---
layout: page
permalink: /howto/backup-data/
---

HowTo: Backup data
==================

a) Synchronise directories with office machine using `rsync`:

    backup
    Bilder
    bin
    Dokumente
    Hörbücher
    Musik
    Quellkoden
    Videos
    web_art
    workspace

b) Export Konqueror bookmarks, KeePassX passwords and Thunderbird
   address book.

c) Clean up and organise directories on home machine:

    backup
    Bilder
    bin
    Dokumente
    Hörbücher
    Musik
    Quellkoden
    Videos
    workspace
    web_art
    mobile

d) Copy directories as backup to secondary location:

    backup
    Bilder
    bin
    Dokumente
    Hörbücher
    Musik
    Quellkoden
    Videos
    workspace
    web_art
    mobile

    .purple .Skype .thunderbird .vim

    .bashrc .profile .vimrc

    *.kdb

e) Restrict access to backup directories on office machine:

    chmod -R 700 ./*
