---
layout: page
permalink: /howto/cmake/
---

HowTo: Enable support for Fortran 2003 in CMake
===============================================

a) Edit file `/opt/local/share/cmake-x.y/Modules/CMakeFortranCompiler.cmake.in`.

b) Add `f03;F03` to variable `CMAKE_Fortran_SOURCE_FILE_EXTENSIONS`:

    set(CMAKE_Fortran_SOURCE_FILE_EXTENSIONS f;F;f77;F77;f90;F90;for;For;FOR;f95;F95;f03;F03)
