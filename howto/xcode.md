HOWTO: Uninstall Xcode
======================

1)  sudo rm -frv \~/Library/Developer/Xcode/DerivedData/\*

2)  sudo rm -frv "\$(getconf
    DARWIN\_USER\_CACHE\_DIR)/org.llvm.clang/ModuleCache"

3)  sudo rm -frv \~/Library/Caches/com.apple.dt.Xcode/\*

4)  Remove Xcode from Applications folder via Finder

5)  rm -frv \~/Library/Developer/\*

6)  rm -frv \~/Library/Preferences/com.apple.dt.Xcode.plist


