---
layout: page
permalink: /howto/flac/
---

HowTo: Split FLAC file using CUE[^1]
====================================

a) Split flac file:
   
    shnsplit -o flac -f file.cue file.flac

b) Fill in tag fields:

    cuetag file.cue split-track*.flac

[^1]: Based on [A](https://coderwall.com/p/6ydyoq/how-to-split-flac-files-by-cue-and-convert-to-alac-on-mac-os-x).
