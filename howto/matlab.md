HOWTO: Launch MATLAB under Linux form office computer
=====================================================

ssh -X mabalenk@vnc.svc.enst-bretagne.fr SETUP MATLAB2013a matlab &

HOWTO: Convert Octave scripts to MATLAB
=======================================

1)  for ... endfor --\> for ... end

2)  function ... endfunction --\> function ... end

3)  if ... end if --\> if ... end

4)  "..." --\> '...'

5)  i++, ++i, i+=1 --\> i = i + 1

6)  Run Octave in traditional mode (errors for Octave-only syntax will
    be given)

octave --traditional

HOWTO: Use MATLAB
=================

1)  Use Unicode

    feature('DefaultCharacterSet', 'UTF8') in startup.m

2)  Fill in a vector

    vec = 1 : 0.25: 100 % min : step : max

3)  Set precision

    format longg % fixed or floating point format, 15/7 digits
    (double/single)

4)  Normalise

    V = V/norm(V)

5)  Axis scaling

    axis( [xmin xmax ymin ymax] )

6)  Read data from file

    load('file.dat', '-ascii') importdata('file.dat', '\t', 5)

7)  Save variables to file

    save('file.dat', var)

OR

     % open a file for writing
     fid = fopen('exptable.txt', 'w');

     % print a title, followed by a blank line
     fprintf(fid, 'Exponential Function\n\n');

     % print values in column order
     % two values appear on each row of the file
     fprintf(fid, '%f  %f\n', y);

     fclose(fid);

8)  Find maximum in multidimensional array

    max(arr(:))

HOWTO: Use MATLAB Editor shortcuts on Linux (emacs mode)
========================================================

Cut Ctrl+W Paste Ctrl+Y Copy Alt+W

Indent \>\> Ctrl+] Indent \<\< Ctrl+[ Smart Indent Ctrl+Alt+/

Comment Ctrl+/ Uncomment Ctrl+T

Open Ctrl+F, O Save Ctrl+X, S Close Ctrl+X, K

Undo Ctrl+Shift+- Redo Ctrl+Shift+Z

Quit Ctrl+Q

HOWTO: Parse numerical data files
=================================

You can push textscan a lot harder than this. For example, consider the
following parameters settings to textscan.

data = textscan(anyLine,'%f','delimiter','[]()
,','MultipleDelimsAsOne',true);

Now this command will work for either of the lines above.

anyLine= '(0,7)'; data = textscan(anyLine,'%f','delimiter','[]()
,','MultipleDelimsAsOne',true); disp(data{1});

anyLine= '[ 0 0.0174533 0.0349066 0.0523599 0.0698132 ]'; data =
textscan(anyLine,'%f','delimiter','[]() ,','MultipleDelimsAsOne',true);
disp(data{1});

HOWTO: Close all open figures
=============================

delete(findall(0,'Type','figure'))

