HOWTO: Restore GRUB
===================

1)  Boot from openSUSE CD into recovery console

2)  Type following commands:

    grub find /boot/grub/menu.lst root (hd0,5) setup (hd0) quit

HOWTO: Restore GRUB2
====================

1)  Boot from openSUSE DVD

2)  Choose Rescue System

3)  Log in as root

4)  Find out root partition

    fdisk -l

5)  Mount root partition

    mount /dev/sda2 /mnt

6)  Mount other devices

    mount --bind /dev /mnt/dev mount --bind /proc /mnt/proc mount --bind
    /sys /mnt/sys

7)  Change root partition

    chroot /mnt OR chroot /mnt /bin/bash

8)  Install grub2

    grub2-install /dev/sda

9)  Exit chroot and reboot

    exit; reboot

http://forums.opensuse.org/content/128-re-install-grub2-dvd-rescue.html

HOWTO: Add Windows 8 entry to GRUB2 manually
============================================

1)  Find out location of Windows 8 boot partition

    fdisk -l

2)  Find out UUID of Windows 8 boot partition

    grub2-probe -t fs\_uuid -d /dev/sda3

3)  Add following lines to '/etc/grub.d/40\_custom'

    menuentry "Windows 8" { insmod part\_gpt insmod chain set
    root='(hd0,gpt1)' chainloader /EFI/Microsoft/Boot/bootmgfw.efi }

OR

     menuentry "Windows 8" {
     insmod part_gpt
     insmod fat
     insmod search_fs_uuid
     insmod chain
     search --fs-uuid --no-floppy --set=root 080F-E6DA
     chainloader (${root})/efi/Microsoft/Boot/bootmgfw.efi
     }

4)  Refresh GRUB2 configuration

    grub2-efi-mkconfig -o /boot/grub2-efi/grub.cfg

https://forums.opensuse.org/english/get-technical-help-here/install-boot-login/482214-manually-adding-new-entry-e-g-windows-grub2-efi-boot-loader-menu.html\#post2517246

