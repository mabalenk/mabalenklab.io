HOWTO: Manipulate a PNM image
=============================

1)  Extract image edge

    pamedge skin00109.pgm \> skin.pgm

2)  Composite (overlay) two Netpbm images together

    pamcomp -plain -opacity=0.5 heart00195.pgm skin00109.pgm mix.pgm

3)  Enforce new maxval (convert to black and white)

    pnmdepth -plain 1 mix.pgm \> mix\_bw.pgm

4)  Edge-enhance of image

    pgmenhance -plain mix\_bw.pgm \> mix\_enh\_bw.pgm

5)  Flip image left to right

    pamflip -plain -lr mix\_enh\_bw.pgm \> mix\_flip.pgm

6)  Invert colours in image

    pnminvert mix\_flip.pgm \> mix\_inv.pgm

7)  Cut an image from bottom

    pamcut -bottom 824 mix\_inv.pgm \> mix\_cut.pgm

8)  Create index image containing thumbnail versions of supplied images
    (contact sheet)

    pnmindex -across=8 heart\*.pgm \> heart\_idx.pgm


