HOWTO: Use Subversion (svn)
===========================

1)  Import files and directories into newly created remote repository

    svn import /home/mabalenk/repository/svn/radar/\
     https://svn.telecom-bretagne.eu/repository/radar/trunk/\
     -m "Initial project import."

2)  List files in remote repository

    svn list https://svn.telecom-bretagne.eu/repository/radar/trunk/

3)  Create directory on local machine for svn repository

    mkdir -p repository/svn/radar

4)  Change to directory of local machine repository

    cd repository/svn/radar

5)  Check out entire project directory from internet (create copy of
    project directory on local machine)

    svn checkout --username <username>
    https://svn.telecom-bretagne.eu/repository/radar/trunk/ .

6)  Update repository

    svn update

7)  Modify files with MATLAB or text editor in usual manner

    ...

8)  Check status of files in the local repository

    svn status OR svn diff

9)  Resolve problems if necessary

    ...

10) Commit your local changes to internet repository

    svn commit -m "Enchanced GUI functionality for ULA and UPA
    creation." svn commit -F comment.txt

11) Continue working cycle with bringing your local repository up to
    date with the network repository

    svn update

12) View log (commit comments) for past 5 entries

    svn log -l 5

13) View log for given date period

    svn log -r {2014-03-10}:{2014-03-16} \> log.txt

14) Disable use of password managers (KWallet)

    \~/.subversion/config -\> password-stores=

15) Disable plaintext password stores

    \~/.subversion/servers -\> store-plaintext-passwords = no

16) Create new branch

    svn copy http://svn.example.com/repos/calc/trunk\
     http://svn.example.com/repos/calc/branches/stable-1.0\
     -m "Creating stable branch of calc project."

17) Revert repository to previous version (revision 10)

    svn update -r 10

18) Revert single file to previous version (revision 10)

    svn update -r 10 main.F90

19) Get current revision number

    svn info | grep Revi

20) Add all unversioned files to repository

    svn add . --force

21) Take file/directory off version control (remove it in repository,
    keep local copy in working directory)

    svn delete --keep-local main.F90

22) Recover accidentially deleted .svn directory

    a)  Create temporary backup directory

    mkdir -p puma.bak

    b)  Switch to newly created directory

    cd puma.bak

    c)  Checkout repository from network location

    svn checkout --username <username>
    https://svn.telecom-bretagne.eu/repository/radar/trunk/ .

    d)  Move .svn directory from backup to current project directory

    mv .svn ../puma/.

    e)  Remove backup directory

    rm -frv ../puma.bak

http://svnbook.red-bean.com/en/1.0/ch03s05.html
http://stackoverflow.com/questions/5891256/list-of-files-changed-since-a-certain-date-using-svn
http://svn.haxx.se/users/archive-2004-08/0554.shtml

