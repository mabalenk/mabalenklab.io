---
layout: page
permalink: /howto/cpu-throttling
---

HowTo: Disable CPU throttling on [Arch Linux](http://www.archlinux.org)[^1]
===========================================================================

a) Install `cpupower`:

    sudo pacman -S cpupower

b) Enable CPU power service at boot:

    sudo systemctl enable cpupower.service

c) View current CPU power settings:

    cpupower frequency-info

d) Set maximum available CPU frequency:

    sudo cpupower frequency-set -g performance

[^1]: Based on [Arch Linux](http://archlinux.org) CPU throttling [wikipage](https://wiki.archlinux.org/index.php/CPU_frequency_scaling).
