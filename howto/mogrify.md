HOWTO: Resize multiple files with ImageMagick tool 'mogrify'
============================================================

a)  Pass all JPG-files in current directory (including subdirectories)
    to mogrify:

    find -name '\*.jpg' -print0 | xargs -0 -r mogrify -resize 800x600

    Options:

    -print0 -- used to process spaces in file names -r -- suspends
    mogrify when there is nothing to do

b)  Convert multiple PNG files into JPEG:

    mogrify -format jpg \*.png

c)  Resize to 50% percent of original:

    mogrify -resize 50% \*.jpg

d)  Resize to resolution YxZ only if image is greater than YxZ:

    mogrify -resize 2880x1800\> \*.jpg


