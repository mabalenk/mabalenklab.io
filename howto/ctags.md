---
layout: page
permalink: /howto/ctags/
---

HowTo: Use `ctags` with source code and deep directory hierarchy
================================================================

a)  Create shell script `dirtags.sh`:

{% highlight bash %}
#!/bin/bash
cd $1
ctags *
{% endhighlight %}

b)  Create `tags` file for each subdirectory:

    find * -type d -exec ~/bin/dirtags.sh {} \;

c)  Rebuild tag file for each directory, when working create keymap and save into
    `~/.vimrc`:

{% highlight vim %}
nmap ,t :!(cd %:p:h;ctags *.[ch])&
{% endhighlight %}

d)  Build global tag file:

    cd ~/<project_dir> ctags --file-scope=no -R

e)  Configure vim to read local tag file first, if not found, search
    global tag file:

{% highlight vim %}
:set tags=./tags,tags,~/project/tags
{% endhighlight %}
