HOWTO: Perform file integrity check with gpg
============================================

1)  Import GNU keys

    gpg --import ftp://ftp.gnu.org/gnu/gnu-keyring.gpg // GNU

2)  Verify downloaded file

    gpg --verify gnupg-1.4.15.tar.bz2.sig // GNU

OR

1)

     gpg --verify ./octave-3.8.1.tar.gz.sig 
     gpg --no-default-keyring --keyring vendors.gpg --keyserver pgp.mit.edu --recv-key 5D36644B
     gpg --verify --verbose --keyring vendors.gpg ./octave-3.8.1.tar.gz.sig

http://www.gnupg.org/download/integrity\_check.en.html

