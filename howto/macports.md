HOWTO: Perform daily maintenacne operations
===========================================

a)  Install package

    sudo port install <pkg_name>

b)  Uninstall package

    sudo port uninstall <pkg_name>

c)  Query package dependencies

    sudo port deps <pkg_name>

d)  List all installed and active packages

    sudo port list installed

HOWTO: Clean up MacPorts installation
=====================================

a)  Remove source and module files

    port clean --all all

b)  Uninstall inactive ports

    sudo port -f uninstall inactive

c)  Remove leftover build files

    sudo rm -rf /opt/local/var/macports/build/\*

d)  Remove downloaded files

    sudo rm -rf /opt/local/var/macports/distfiles/\*

e)  Remove archives

    sudo rm -rf /opt/local/var/macports/packages/\*

f)  Delete TAR archives

    sudo rm -rf /opt/local/var/macports/software

HOWTO: Select and set GNU compiler
==================================

a)  View available compiler versions

    port select --list gcc

b)  Choose required compiler version

    sudo port select --set gcc mp-gcc49

http://superuser.com/questions/165652/how-can-i-clean-up-my-macports-installation

HOWTO: Resolve MacPorts installation 'conflict'
===============================================

a)  sudo port deactivate wxwidgets
b)  sudo port install wxwidgets-devel

c)  sudo port deactivate wxwidgets-devel
d)  sudo port activate wxwidgets

http://stackoverflow.com/questions/8561355/resolving-a-macports-installation-conflict

HOWTO: Patch MacPort
====================

a)  sudo rm -rf \$(port dir qt4-mac)
b)  sudo port sync
c)  sudo port clean qt4-mac
d)  cd \$(port dir qt4-mac)
e)  sudo patch -p0 \< PATH\_TO/qt4-mac.10.11.diff
f)  sudo port install qt4-mac

https://trac.macports.org/ticket/48129

HOWTO: Install MacPort from manually downloaded TAR archive
===========================================================

a)  sudo wget -O
    /opt/local/var/macports/distfiles/mesa/mesa-10.5.4.tar.xz\
     ftp://ftp.freedesktop.org/pub/mesa/10.5.4/mesa-10.5.4.tar.xz

b)  sudo wget -O
    /opt/local/var/macports/distfiles/mesa/mesa-10.5.4.tar.xz.sig\
     ftp://ftp.freedesktop.org/pub/mesa/10.5.4/mesa-10.5.4.tar.xz.sig

c)  sudo port install mesa

http://trac.macports.org/ticket/47314

