HOWTO: Synchronise data between multiple computers
==================================================

1)  rsync -arvuzP -e "ssh -p <custom_port_no>" install/
    amyjones@130.88.254.11:\~/install

Options:

     -a   archive mode
     -r   recurse into directories
     -v   increase verbosity
     -u   skip files, that are newer on the receiver
     -z   compress data during file transfer
     -P   print information about transfer progress

HOWTO: Clone entire 'root' partition
====================================

1)  Mount partition to be cloned

    sudo mount /dev/sdb2 /mnt/root

2)  Clone data to new partition '/clone'

    sudo rsync -aAXv /mnt/root/\* /clone\

    --exclude={/mnt/root/dev/*,/mnt/root/proc/*,/mnt/root/sys/*,/mnt/root/tmp/*,/mnt/root/run/*,/mnt/root/mnt/*,/mnt/root/media/*,/mnt/root/lost+found,/mnt/root/var/cache/zypp,/mnt/root/var/log/*}


