---
layout: page
permalink: /howto/lowercase/
---

HowTo: Rename multiple files from upper to lowercase
====================================================

    f in *; do
      mv "$f" "$f.tmp";
      mv "$f.tmp" "`echo $f | tr "[:upper:]" "[:lower:]"`";
    done

1. Based on [A](https://stackoverflow.com/questions/7787029/how-do-i-rename-all-files-to-lowercase).
