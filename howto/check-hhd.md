---
layout: page
permalink: /howto/check-hdd/
---

HowTo: Check hard drive for errors[^1]
======================================

a) `sudo smartctl -t long /dev/sda`

b) `sudo touch /forcecheck; /sbin/reboot`

OR

b) `sudo shutdown -rF now` See [`fsck`]({{ site.url}}/howto/fsck) for details.

c) Check for bad blocks:

    sudo badblocks -v /dev/sda1
    sudo badblocks /dev/sda > ~/badblocks.txt
    sudo fsck -l badblocks.txt /dev/sda

[^1]: Based on [A](http://www.linuxquestions.org/questions/linux-hardware-18/how-do-i-check-my-hard-disk-for-errors-possible-hard-disk-failure-887618) and [B](http://www.howtogeek.com/howto/37659/the-beginners-guide-to-linux-disk-utilities).
