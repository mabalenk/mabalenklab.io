HOWTO: Submit and run jobs on HPDC systems
==========================================

a)  Login interactively, compile and run code with normal 'mpiexec'
    commands

    srun --pty -p jacob -c 96 \$SHELL

b)  Submit job for batch execution

    srun -c 4 -p crete -t 30 <exec_name>

c)  Ask for exclusive nodes allocation

    srun --exclusive -p lesvos --pty -t 4:0:0 \$SHELL

d)  View jobs submitted to system

    squeue -l

e)  View information about jobs and nodes

    sinfo -lNe

http://hpdc-wiki.eeecs.qub.ac.uk/wikis/systems/doku.php?id=slurm\_resource\_manager

