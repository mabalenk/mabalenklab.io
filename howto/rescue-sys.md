HOWTO: Rescue system:
=====================

1)  Boot from DVD and login as root

    root

2)  Mount root partition

    mount /dev/sda2 /mnt

3)  Make installed system aware of available hardware

    for i in /sys /proc /run /dev; do mount --bind "$i" "/mnt$i"; done

4)  Change root to installed system

    chroot /mnt

5)  Mount DVD (See 'mount\_dvd.txt')

    mkdir -p /media/dvd mount /dev/sr0 /media/dvd

6)  Activate network

    dhcpcd -N eth0

7)  Start yast, zypper to repair system

https://forums.opensuse.org/content/146-using-livecd-take-over-repair-installed-system.html
http://blog.pdark.de/2010/08/01/starting-the-network-with-opensuses-rescue-system/

