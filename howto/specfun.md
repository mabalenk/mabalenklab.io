HOWTO: Use Fortran 77 special functions (specfun) from NetLib library (http://www.netlib.org/specfun/)
======================================================================================================

1)  Download source code from www.netlib.org/specfun

2)  Add Fortran 77 file extension (.f) to each file

    find . -type f -exec mv '{}' '{}'.f ;

3a) For single precision code uncomment all lines beginning with 'CS' in
all files (requires gawk 4.1.0)

     gawk -i inplace '{sub(/^CS/, "  "); print}' *.f

3b) For double precision code uncomment all lines beginning with 'CD' in
all files (requires gawk 4.1.0)

     gawk -i inplace '{sub(/^CD/, "  "); print}' *.f

4)  Create CMakeLists.txt files for compilation

Base directory --------------

     project(specfun Fortran)
     cmake_minimum_required(VERSION 2.6)
     add_subdirectory(src)
     
     

Source directory ----------------

     set(CMAKE_INCLUDE_CURRENT_DIR ON)
     file(GLOB specfun_SRC *.f)
     add_executable(specfun ${specfun_SRC})

5)  
