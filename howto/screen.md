HOWTO: Use GNU Screen
=====================

a)  Kill given screen session:

screen -X -S [session \# you want to kill] quit

b)  Detach from screen session:

Ctrl+a+d

c)  Re-attach to screen session:

screen -r [session \#]

d)  Obtail list of sessions available:

screen -ls

https://www.linode.com/docs/networking/ssh/using-gnu-screen-to-manage-persistent-terminal-sessions
http://stackoverflow.com/questions/1509677/kill-detached-screen-session

