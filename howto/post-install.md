HOWTO: Setup openSUSE 13.1 after fresh installation
===================================================

1)  Activate community repositories

2)  Install additional packages from repositories

    aspell-{de, en, fr, lv, ru} bchunk blender cdparanoia cmake conky
    crafty cuetools darktable djvulibre djvulibre-djview4 djvu2pdf
    dropbox easytag fbreader feh ffmpeg figlet filelight
    findutils-locate flac fortune gcc gcc-c++ git git-daemon gnuchess
    gnuplot htop ispell-{british, french, german, russian}
    java-1\_7\_0-openjdk-{devel, javadoc} k3b-codecs kate kchmviewer
    kernel-source kile lame lftp libdvdcss2 libkde4-devel libxine2
    libxine2-codecs make mono mplayer octave octave-devel plplot-octave
    qtoctave p7zip pavucontrol pdf2djvu pdftk phalanx pidgin sensors
    shntool texlive-{adjustbox, algorithmicx, algorithms, amsmath-doc,
    bbm, biber, biblatex-ieee, cleveref, collection-langcyrillic,
    cyrillic, doublestroke, dtk, fourier, glossaries, IEEEtran, import,
    multibib, muthesis, tocloft, tools, sciposter, SIunits, siunitx,
    verse} MozillaThunderbird tree umbrello vim-plugin-latex virtualbox
    virtualbox-qt virtualbox-guest-x11 w32codec-all wine xfig xindy xli
    xscreensaver xzgv yakuake

3)  Update newly installed system

4)  Install NVIDIA graphics card driver (See 'nvidia-graphics.txt')

5)  Install IDE

    geany 14.2 M kdevelop4 23.1 M netbeans 7.3 192.9 M eclipse 252.0 M

6)  Install additional packages from source

    fftw, keepassx, prince, txt2html

7)  Install Adobe Acrobat Reader from BIN package

8)  Install research software

    Intel C, C++, Fortran compilers Intel MKL MPICH SCALAPACK LINPACK
    BLAS LAPACK GiD MeshLab freesurfer ParaView

9)  Give users access to new data partitions

    chgrp -R users /data chmod -R g+w,o= /data

10) Copy backed up files to new system (including .purple, .Skype,
    .thunderbird, .vim, .bashrc, .profile, .vimrc, \*.kdb) (See
    'backup\_data.txt')

11) Setup Konqueror:

    a)  Set as standard browser

        Applications Menu -\> Configure Desktop -\> Default Applications
        -\> Web Browser -\> Application based on contents of URL

    b)  Enable mouse gestures with 'khotkeys'

    c)  Define new search engine shortcuts {@}: google scholar,
        amazon.co.uk, ebay.co.uk, lib.ru, ya.ru, lingvo.ru, ozon.ru,
        rylands lib wikibooks

    d)  Import bookmarks from backup

12) Setup Thunderbird

    a)  Set as default e-mail application

    b)  Setup all e-mail accounts and aliases

    c)  Import address book from backup

    d)  Add spell checking dictionaries for English, German, Russian and
        Latvian

    e)  Change default sound for incoming message to KDE-Im-New-Mail

    f)  Set default encoding to UTF-8

        Preferences -\> Display -\> Advanced -\> Character Encodings -\>
        Outgoing/Incoming Mail

13) Install and setup Skype:

    a)  Install dependent packages

        libqt4-x11-32bit libQtWebKit4-32bit libXss1-32bit libXv1-32bit
        xorg-x11-libs

    b)  Install Skype from skype-4.2.0.13-suse.i586.rpm

    c)  Disable smileys, default login and logout sounds

    d)  Change default sound for incoming call to Nokia 8110 ringtone

    e)  Fix issue with microphone

14) Setup Pidgin

15) Add webcam repository and install package luvcview

16) Import passwords to KeePassX

17) Add Russian, German, French and Latvian as input languages

18) Setup timezones, calendar and automatic NTP time updates

    ntp2c.mcc.ac.uk ntp2d.mcc.ac.uk

19) Setup (departmental) printer(s)

20) Setup digikam

21) Alter default user password settings to request password change
    after 4 weeks

22) Enable external device automount

    Configure Desktop -\> Hardware -\> Removable Devices -\>
    Automatically mount removable media when attached

23) Setup xscreensaver:

    a)  Disable original KDE screensaver

        Yast -\> Configure Desktop -\> Hardware -\> Display and Monitor
        -\> Screen Locker -\> Disable checkbox 'Start Automatically'

    b)  Create file \~/.kde4/Autostart/xscreesaver.desktop with
        permissions 755

        [Desktop Entry] Exec=xscreensaver Name=XScreenSaver
        Type=Application X-KDE-StartUpNotify=false

    c)  Create new file /usr/lib64/kde4/libexec/kscreenlocker\_greet
        with text

        \#!/bin/sh xscreensaver-command -lock

    d)  Set permissions 755 on /usr/lib64/kde4/libexec/kscreenlocker

    e)  Choose screensaver via xscreensaver-demo

24) Setup kernel source screensaver

    a)  Choose phosphor via xscreensaver-demo

    b)  Set command line settings to

        phosphor -root -scale 2 -delay 10000 -program
        /home/amyjones/bin/kernel-src.sh\
         -font '-xos4-terminus-medium-*-*-*-24-*-*-*-*-*-iso10646-\*'

25) Change default GRUB splash screen to Fallout 3

26) Disable KDE audio notifications

    Kickoff-Anwendungsstarter -\> Systemeinstellungen -\>
    Benachrichtungen -\> Wiedergabe-Einstellungen -\> Keine Audioausgabe
    -\> Anwenden

27) Install LaTeX font fourier

28) Install Microsoft Trebuchet font

29) Set up vim-latex

30) Set English as default language for GIMP and Inkscape by setting
    aliases in .bashrc

    \# change GIMP language to english alias gimp="LANGUAGE=en gimp"

    \# change Inkscape language to english alias inkscape="LANGUAGE=en
    inkscape"

31) Add high pass filter to GIMP

    cp -rvf \~/Quellkoden/gimp/high-pass.scm /usr/share/gimp/2.0/scripts

32) Install rename.pl to /usr/bin/prename


