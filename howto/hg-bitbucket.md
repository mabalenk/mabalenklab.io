---
layout: page
permalink: /howto/hg/
---

HowTo: Use [Mercurial](https://www.mercurial-scm.org) with [BitBucket](https://bitbucket.org/product)
=====================================================================================================

a) Setup [Mercurial](https://www.mercurial-scm.org) (edit `~/.hgrc`):

    [ui]
    username = John Smith <j.smith@gmail.com>
    editor   = vim

b) Basic working cycle:

    hg clone https://mabalenk@bitbucket.org/mabalenk/tutorials.bitbucket.org
    hg status
    hg commit -m "Added a quote by Erich Maria Remarque."
    hg push
    hg update default
    hg pull -r default https://mabalenk@bitbucket.org/mabalenk/tutorials.bitbucket.org
    hg status
    hg history

c) Accept all "other" changes:

    hg resolve -t internal:other --all

d) Accept all "local" changes:

    hg resolve -t internal:local --all

e) Revert file to last commited version:

    hg revert <file_name>

f) Create new branch:

    hg branch 'feature'

g) Switch to another branch:

    hg up <branch_name>

h) Closing branch after successfull pull request:

    hg up <branch_name>
    hg commit --close-branch -m "* Closing branch after successfull pull request"
    hg up default
    hg merge <branch_name>

i) Revert previous merge:

    hg update -C -r .

j) Merge latest changes from branch 'default' into branch 'feature':

    hg up 'feature'
    hg merge 'default'
