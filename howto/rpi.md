HOWTO: Perform initial setup of RaspberryPi
===========================================

1.  Prepare Raspberry Pi SD card

a)  Check for mounted SD cards

        df -h   OR   fdisk -l

b)  Copy ArchLinux image to SD card

        sudo dd bs=4M if=~/Quellkoden/archlinux/archlinux-arm-2014.03-rpi.img of=/dev/mmcblk0

c)  Syncronise and unmount safely

        sudo sync; sudo eject /dev/mmcblk0

2.  Resize Linux partition to take entire space available on SD card

a)  Enter fdisk for partition resizing

        fdisk /dev/mmcblk0

b)  Delete Linux and Extended partitions

        d, 5, d, 2

c)  Re-create new Extended and Linux partitions starting from the same
    sector ending at the farthest available sector.

        n, e, n, l

d)  Write partitions to disk

        w

e)  Reboot

        shutdown -r now  OR  reboot

f)  Resize filesystem

        resize2fs /dev/mmcblk0p5

g)  Check root partition has increased size

        df -h

3.  Change root password

a)  passwd

4.  Add normal user

b)  useradd -m -g users -s /bin/bash archie usermod -aG wheel archie

    OR

    useradd -m -G wheel -s /bin/bash archie

5.  Set password for new normal user

a)  passwd archie

6.  Upgrade Arch Linux distribution

a)  pacman -Syu

7.  Install specific packages by name given

a)  pacman -S <pkg_name>

8.  Find out IP address of RPi on LAN

a)  sudo nmap -sP 10.29.231.0/24

9.  Setup sudo

a)  Edit /etc/sudoers

        EDITOR=vim visudo -f /etc/sudoers

b)  Uncomment

        wheel ALL=(ALL) ALL

c)  Set timezone

        timedatectl list-timezones
        timedatectl set-timezone Europe/Paris
        date

d)  Set date and time

        sudo timedatectl set-time "2014-03-21 10:39:00"

10. View logs

    journalctl

http://elinux.org/RPi\_Easy\_SD\_Card\_Setup
http://elinux.org/RPi\_Resize\_Flash\_Partitions
https://wiki.archlinux.org/index.php/Beginners'*guide
https://wiki.archlinux.org/index.php/Users*and\_groups
http://www.pi-point.co.uk/documentation/
http://obihoernchen.net/wordpress/770/plug\_computer\_arch\_linux/

