HOWTO: Prepare Gparted live USB stick
=====================================

1)  Create FAT32 partition on USB stick with fdisk and mkfs (See
    fdisk.txt for details)

2)  Extract gparted-\*.zip archive to USB stick

    unzip \~/Quellkoden/gparted/gparted-live-0.19.0-1-i686-pae.zip -d
    /var/run/media/amyjones/GPARTED/

3)  Make USB stick bootable:

    sudo bash makeboot.sh /dev/sdb1

http://gparted.org/liveusb.php\#linux-method-b

