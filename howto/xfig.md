HOWTO: Import Xfig figure into LaTeX document
=============================================

1)  Start Xfig with LaTeX options

    xfig -specialtext -latexfonts -startlatexFont default

2)  Incorporate mathematical notation between $...$ into normal textbox

3)  Export figure as combined PS/LaTeX

4)  Proceed manually

a)  Create template TeX file (template.tex) and input Xfig figure
    (figure.pstex\_t)

        \usepackage{epsfig}
        ...
        \input{figure.pstex_t}

b)  Process template TeX file with LaTeX

        latex template.tex

c)  Convert DVI output to PS

        dvips template.dvi -o template.ps

d)  Convert from PS to EPS

        ps2eps -ignoreBB template.ps

e)  Convert from EPS to PDF

        epstopdf template.eps

OR

5)  Use wrapper scripts by Håvard Berland
    (http://www.math.ntnu.no/\~berland)

    figtex2eps figure.xfig figtex2pdf figure.xfig

    figtex2eps -pdf figure.xfig

http://epb.lbl.gov/xfig/latex\_and\_xfig.html

