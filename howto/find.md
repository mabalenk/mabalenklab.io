HOWTO: Find files
=================

1)  Type in command line:

    find / -name 'linux'

http://www.computerhope.com/issues/ch000623.htm

HOWTO: Add file extension to all files within current directory
===============================================================

1)  find . -type f -exec mv '{}' '{}'.jpg ;

Find all files (-type f) starting from the current directory (.), apply
move command (mv) to each of them. Quotes around {} ensure that
filenames with spaces and newlines are properly handled.

http://stackoverflow.com/questions/1108527/recursively-add-file-extension-to-all-files

HOWTO: Find and replace pattern in all files of given file type
===============================================================

find -print0 . -type f -name '\*.m' | xargs -0 sed -i
's/a.example.com/b.example.com/g'

http://stackoverflow.com/questions/1585170/how-to-find-and-replace-all-occurrences-of-a-string-recursively-in-a-directory-t

HOWTO: Find files and directories that were modified today
==========================================================

sudo find /mnt/root/ -mtime -1 -print

HOWTO: Print out only filenames of given file type in current directory only
============================================================================

find ./ -maxdepth 1 -name "\*.f90" -printf "%f\n"

HOWTO: Avoid output of error messages "Permission denied..."
============================================================

find 2\>/dev/null \<...\>

HOWTO: Find executable files only
=================================

find ./ -executable -type f

