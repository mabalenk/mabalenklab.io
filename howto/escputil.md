---
layout: page
permalink: /howto/escputil
---

HowTo: Perform printer maintenance tasks
========================================

a) Clean printer head:

    escputil -r /dev/usblp0 -c

b) Show remaining ink levels:

    escputil -r /dev/usblp0 -i
