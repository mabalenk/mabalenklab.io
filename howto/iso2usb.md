HOWTO: Prepare bootable USB disk from ISO image
===============================================

1)  Find out name of USB disk

    sudo /sbin/fdisk -l

2)  Umount device

    umount /dev/sdb1

3)  Format drive in FAT32

    sudo /sbin/mkdosfs -n 'openSUSE-13.1-NET-x86\_64' -I /dev/sdb -F 32

4)  Enable ISO recognition by BIOS from hard drive

    isohybrid \~/Quellkoden/opensuse/openSUSE-13.1-NET-x86\_64.iso

5)  Copy ISO file

    sudo dd if=\~/Quellkoden/opensuse/openSUSE-13.1-NET-x86\_64.iso
    of=/dev/sdb bs=4k

6)  Unplug device correctly

    sync sudo eject /dev/sdb

HOWTO: Prepare bootable Windows 7 USB disk from ISO image
=========================================================

1)  Create a new partition and change its type to 7 (HPFS/NTFS/exFAT)

    fdisk /dev/sdc, n, p, 1, t, 7, a, 1, w

2)  Partition USB drive with parted

    sudo parted /dev/sdc

    mklabel msdos mkpart primary ntfs 1 -1 set 1 boot on quit

3)  Format new partition as NTFS

    sudo mkfs.ntfs -f /dev/sdc1

4)  Install ms-sys

    tar xvzf ms-sys-2.3.0.tar.gz cd ms-sys-2.3.0 sudo make sudo make
    install

5)  Write Windows 7 MBR to USB

    sudo ms-sys -7 /dev/sdc

6)  Mount USB drive

    mkdir -p /mnt/disk sudo mount /dev/sdc1 /mnt/disk

7)  Mount ISO image file

    mkdir -p /mnt/iso sudo mount -o loop
    \~/Quellkoden/win/7/en\_windows\_7\_professional\_x64\_dvd\_X15\_65805.iso
    /mnt/iso

8)  Copy contents of ISO file to USB drive

    cp -av /mnt/iso/\* /mnt/disk/.

9)  Unmount USB drive and ISO file

    sudo umount /mnt/disk sudo umount /mnt/iso

http://thornelabs.net/2013/06/10/create-a-bootable-windows-7-usb-drive-in-linux.html

HOWTO: Prepare bootable Windows 8 USB disk from ISO image on Mac OS X
=====================================================================

1)  Convert *.iso to *.img using hdiutil

hdiutil convert -format UDRW -o /path/to/target.img /path/to/source.iso

2)  Rename *.img.dmg file into *.img

mv <image>.img.dmg <image>.img

3)  Get list of currently connected devices

diskutil list

4)  Unmount target USB flash disk

diskutil unmountDisk /dev/disk2

5)  Copy \*.img file to USB flash disk

sudo dd if=<image>.img of=/dev/disk2 bs=1m

6)  Eject USB flash disk

diskutil eject /dev/disk2

http://apple.stackexchange.com/questions/103874/creating-a-bootable-usb-of-windows-8-1-on-os-x

