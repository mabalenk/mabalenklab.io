HOWTO: Keep and maintain multiple kernel versions
=================================================

1)  Enable multiversion support by uncommenting the following line in
    /etc/zypp/zypp.conf

    \# multiversion = provides:multiversion(kernel)

2)  Setup automatic deletion of unused kernels in /etc/zypp/zypp.conf

    multiversion.kernels = latest,latest-1,oldest

3)  Display a list of all kernel packages available for installation

    zypper se -s 'kernel\*'

4)  Install specific kernel version

    zypper in kernel-default-2.6.32.10-0.4.1

5)  List all kernels installed

    zypper se -si 'kernel\*'

6)  Remove unnecessary kernel package

    zypper rm PACKAGENAME-VERSION

http://doc.opensuse.org/documentation/html/openSUSE/opensuse-tuning/cha.tuning.multikernel.html

