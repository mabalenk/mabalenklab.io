HOWTO: Convert RAW brain image to TIFF
======================================

General procedure: brain web RAW --\> MATLAB script --\> TIFF file

1)  Download RAWB file

2)  Use RAWBOPEN to open and convert to TIFF

3)  Use interfaceHeadVolume('file.tif') Y/N

4)  Press 'Compute' and wait \~5 minutes

5)  Output produced by sauvegardeTetra(nodes, elems, 'volume'):
    tetrahedral (volumetric) mesh and triangular (surface) mesh will be
    stored in the same directory

6)  Use GiD for visualising the mesh file \*.msh

    GiD --\> Preprocessing part --\> Load \*.msh file

7)  Switch to postprocessing, use 'Layer' button to view different brain
    parts in different colours


