HOWTO: Mount a DVD
==================

1)  Login as root

    su -l root

2)  Create target directory

    mkdir -p /media/dvd

3)  Mount DVD under target directory

    mount /dev/sr0 /media/dvd

HOWTO: Mount a DVD ISO image
============================

4)  Mount ISO-file via a loop device

    mount -o loop image.iso /media/dvd

http://www.cyberciti.biz/tips/how-to-mount-iso-image-under-linux.html

