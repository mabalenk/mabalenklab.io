---
layout: page
permalink: /howto/audiocd2wav
---

HowTo: Rip Audio CD to WAV
==========================

1)  Rip all audio tracks with `cdparanoia`:

    cdparanoia -B
