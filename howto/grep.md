HOWTO: Combine find and grep efficiently
========================================

a)  find ./ -name "\*.tex" -exec grep -qi "endnote" {} ; -print

http://ailoo.net/2008/07/mit-find-und-grep-in-dateien-suchen/

HOWTO: Search in files with given extension
===========================================

a)  Find all occurences of «»

    grep -rn --include \*.m '\\'

b)  Find all occurences of «''»

    grep -rn --include \*.m ''\\''

http://stackoverflow.com/questions/12516937/grep-but-only-certain-file-extensions

