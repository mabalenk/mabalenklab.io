HOWTO: Add network printer in office
====================================

1)  Open CUPS administration page

    http://localhost:631/

2)  Select 'Adding Printers and Classes' under 'CUPS for Administrators'

3)  'Add Printer'

4)  'Windows Printer via SAMBA'

5)  'Connection'
    smb://campus/username:password@vss-cups-03.priv.enst-bretagne.fr/IMP-MO-001

6)  HP LaserJet P3015, C02-134 PPD: HP LaserJet 3015 - CUPS+Gutenprint
    v5.2.9.

7)  Set default printer from command line:

    lpoptions -d printer

8)  Delete printer from command line:

    lpadmin -x <printer_name>

http://support.telecom-bretagne.eu/stylesheets/archivedTicketView.faces
Ticket (archivé) n°21038 : install network printer (MO department) under
Linux

