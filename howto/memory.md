HOWTO: Partially free RAM memory
================================

1)  Flush file system buffers

    sync

2)  Free page cache

    echo 1 \> /proc/sys/vm/drop\_caches

3)  Free dentries and inodes

    echo 2 \> /proc/sys/vm/drop\_caches

4)  Free page cache, dentries, inodes

    echo 3 \> /proc/sys/vm/drop\_caches


