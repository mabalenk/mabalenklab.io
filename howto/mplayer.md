HOWTO: Play video with audio stream in separate file
====================================================

     mplayer <video_file>.avi -audiofile <audio_file>.mp3

HOWTO: Play MP3-files in mplayer
================================

1)  Create playlist and play music

    find <music_dir> -iname '\*.mp3' | mplayer -shuffle -playlist -

HOWTO: Play web stream in mplayer
=================================

     mplayer -cache 1024 -playlist <http://www.webstream.com>

HOWTO: Specify encoding for Russian subtitles
=============================================

     mplayer -subcp enca:ru:utf8 film_fr.srt film_fr.mp4

HOWTO: Play an audio CD
=======================

     mplayer -cdrom-device /dev/cdrom cdda:// -cache 5000 -ao alsa

1)  Add default options to the configuration file (\~/.mplayer/config):

    \# cd-rom name cdrom="/dev/cdrom"

    \# cache setting cache=5000

    \# audio driver ao=alsa

2)  Playback an audio CD

    mplayer cdda://

http://www.cyberciti.biz/faq/linux-unix-mplayer-playing-audio-dvd-cd-using-bash-shell/

HOWTO: Use mplayer shortcuts
============================

Keyboard control
----------------

\<- and -\> Seek backward/forward 10 seconds. up and down Seek
forward/backward 1 minute. pgup and pgdown Seek forward/backward 10
minutes. [ and ] Decrease/increase current playback speed by 10%. { and
} Halve/double current playback speed. backspace Reset playback speed to
normal. \< and \> Go backward/forward in the playlist. ENTER Go forward
in the playlist, even over the end. HOME and END next/previous playtree
entry in the parent list INS and DEL (ASX playlist only) next/previous
alternative source. p / SPACE Pause (pressing again unpauses). . Step
forward. Pressing once will pause movie, every consecutive press will
play one frame and then go into pause mode again (any other key
unpauses). q / ESC Stop playing and quit. + and - Adjust audio delay by
+/- 0.1 seconds. / and \* Decrease/increase volume. 9 and 0
Decrease/increase volume. ( and ) Adjust audio balance in favour of
left/right channel. m Mute sound. \_ (MPEG-TS and libavformat only)
Cycle through the available video tracks. \# (DVD, MPEG, Matroska, AVI
and libavformat only) Cycle through the available audio tracks. TAB
(MPEG-TS only) Cycle through the available programs. f Toggle fullscreen
(also see -fs). T Toggle stay-on-top (also see -ontop). w and e
Decrease/increase pan-and-scan range. o Toggle OSD states: none / seek /
seek + timer / seek + timer + total time. d Toggle frame dropping
states: none / skip display / skip decoding (see -framedrop and
-hardframedrop). v Toggle subtitle visibility. j Cycle through the
available subtitles. y and g Step forward/backward in the subtitle list.
F Toggle displaying forced subtitles. a Toggle subtitle alignment: top /
middle / bottom. x and z Adjust subtitle delay by +/- 0.1 seconds. r and
t Move subtitles up/down. i (-edlout mode only) Set start or end of an
EDL skip and write it out to the given file. s (-vf screenshot only)
Take a screenshot. S (-vf screenshot only) Start/stop taking
screenshots. I Show filename on the OSD. ! and @ Seek to the beginning
of the previous/next chapter. D (-vo xvmc, -vf yadif, -vf kerndeint
only) Activate/deactivate deinterlacer.

Hardware accelerated video output
---------------------------------

1 and 2 Adjust contrast. 3 and 4 Adjust brightness. 5 and 6 Adjust hue.
7 and 8 Adjust saturation.

SDL Video Output Driver
-----------------------

c Cycle through available fullscreen modes. n Restore original mode.

Multimedia Keyboard
-------------------

PAUSE Pause. STOP Stop playing and quit. PREVIOUS, NEXT Seek
backward/forward 1 minute.

GUI Support
-----------

ENTER Start playing. ESC Stop playing. l Load file. t Load subtitle. c
Open skin browser. p Open playlist. r Open preferences.

TV, DVB Support
---------------

h and k Select previous/next channel. n Change norm. u Change channel
list.

Navigate menus (if DVNAV is supported)
--------------------------------------

keypad 8 Select button up. keypad 2 Select button down. keypad 4 Select
button left. keypad 6 Select button right. keypad 5 Return to main menu.
keypad 7 Return to nearest menu (the order of preference is:
chapter-\>title-\>root). keypad ENTER Confirm choice.

Teletext support (if compiled)
------------------------------

X Switch teletext on/off. Q and W Go to next/prev teletext page.

Mouse control
-------------

button 3 and button 4 Seek backward/forward 1 minute. button 5 and
button 6 Decrease/increase volume.

Joystick Control
----------------

left and right Seek backward/forward 10 seconds. up and down Seek
forward/backward 1 minute. button 1 Pause. button 2 Toggle OSD states:
none / seek / seek + timer / seek + timer + total time. button 3 and
button 4 Decrease/increase volume.

http://www.keyxl.com/aaa2fa5/302/MPlayer-keyboard-shortcuts.htm

