HOWTO: Perform maintenance tasks for ListServ mailing list
==========================================================

1)  All commands should be send by email to ListServ batch address

2)  Replace default headers file

PUT CHESS-CLUB LIST PW=Chess%Club9 * * The University of Manchester
Chess Club * * .HH ON \* Review=Owner \* Subscription=Open,Confirm \*
Owner=abalenkovs@cs.man.ac.uk \* Owner=Quiet: \* Owner=jleksas@gmail.com
\* Owner=ownerall@listserv.manchester.ac.uk \* Errors-To=Owner \*
Reply-to=Sender,Respect \*
Notebook=Yes,/home/listserv/lists/chess-club,Monthly,Private \*
Confidential=No \* Send=Private \*
Editor=abalenkovs@cs.man.ac.uk,(CHESS-CLUB) \*
Default-Options=NOACK,NOREPRO,NOHTML \* Subject-Tag=chess-club \*
Auto-Delete=No \* .HH OFF * * This is an open discussion list on the
Chess Club activities, \* event announcements and all chess related
subjects. \*

3)  Replace default welcome message

PUT CHESS-CLUB WELCOME PW=Chess%Club8 SUBJECT: Welcome to
CHESS-CLUB@listserv.manchester.ac.uk

Welcome to The University of Manchester Chess Club email list. This is
an open discussion list on the Chess Club activities, event
announcements and all chess related subjects.

Your list owner is: Maksims Abalenkovs <abalenkovs@cs.man.ac.uk>

Use the list email address CHESS-CLUB@listserv.manchester.ac.uk to send
your message to all members of the list.

To receive an email list command reference send a following email:
Recipient: listserv@listserv.manchester.ac.uk Subject: list
unsubscription request Message: INFO REFCARD

To unsubscribe: Recipient: listserv@listserv.manchester.ac.uk Subject:
list unsubscription request Message: SIGNOFF CHESS-CLUB

For a general introduction on Listserv visit:
http://listserv.manchester.ac.uk/listserv/intro.shtml

There is a Facebook group related to the Chess Club:
http://www.facebook.com/group.php?gid=58564702456

