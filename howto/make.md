HOWTO: Redirect output from program compilation with 'make'
===========================================================

make &\> output.txt

HOWTO: Print out make variable to screen
========================================

$(info $$NAGOBJ is [${NAGOBJ}])
===============================

HOWTO: Check make file for validity (tabs, end of line symbols)
===============================================================

cat -e -t -v <makefile>

