HOWTO: Obtain consecutive index numbers out of triple loop indices
==================================================================

1)  Given three index loop over {i,j,k}

int idx = 0;

for (int i = 0, i \< ni, ++i) { for (int j = 0, j \< nj, ++j) { for (int
k = 0, k \< nk, ++k) {

         idx = k + j*nk + i*nj*ni;

         ...

       }
     }

}

