---
layout: page
permalink: /howto/ape2flac/
---

HowTo: Convert APE to FLAC and Split Using CUE[^1]
==================================================

a) Convert and split:

    cuebreakpoints *.cue | shnsplit -o flac {*.ape, *.flac}

b) Fill in tag fields:

    cuetag *.cue split-track*.flac

[^1]: Based on [A](http://www.webupd8.org/2009/04/split-ape-and-flac-files-in-ubuntu-and.html).
