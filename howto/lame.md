HOWTO: Encode WAV-files to MP3
==============================

1)  lame -V0 -h -b 160 --vbr-new input.wav output.mp3

Options:

-V enables variable bitrate (VBR), specifies quality level, 0--highest,
4--default, 9--lowest -h enables quality improvements, slower higher
quality encoding, same as -q 2 -b indicates min bitrate to use with VBR
--vbr-new invokes newest VBR algorithm, very fast (twice as fast as
--vbr-old)

