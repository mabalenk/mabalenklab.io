HOWTO: Generate random numbers in command line
==============================================

a)  Generate random number:

    od -An -N2 -i /dev/random

    od -- object dump tool -An -- turns off address displaying -N2 --
    number of bytes to extract -i -- use integer as display format
    /dev/random -- pure random bytes generator, built on time delay
    between user input actions

b)  Create file with random number as file name:

    touch `od -An -N2 -i /dev/random`.tmp

c)  Random number within given range (100--1000):

    echo \$(( 100+(`od -An -N2 -i /dev/random` )%(1000-100+1) ))

    where echo
    $((...)) is a simple calculation construct for bash, e.g.  echo $((3+2))

    random number within range is given by min+( seed%(max - min + 1) )

    larger number of bytes can be obtained using /dev/urandom instead of
    /dev/random

http://linux.byexamples.com/archives/128/generating-random-numbers/

