---
layout: page
permalink: /howto/convert/
---

HowTo: Convert, resize and center PNG into PDF
==============================================

    convert puma1.png -gravity center -background white -compress jpeg \
      -resize 1755x2475 -extent 1755x2475 -units PixelsPerInch \
      -density 150x150 puma1.pdf

HowTo: Join multiple PNG files together
=======================================

a) Horizontal join:

    convert ifile1.png ifile2.png +append ofile3.png 

b) Vertical join:

    convert ifile1.png ifile2.png -append ofile3.png
