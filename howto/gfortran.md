HOWTO: Compile Fortran code with 'gfortran'
===========================================

a)  Debug flags:

    -O0 -Og -Wall -Wextra -fimplicit-none -Wline-truncation\
     -Wcharacter-truncation -Wsurprising -Waliasing
    -Wimplicit-interface\
     -Wunused-parameter fwhole-file -check=all -std=f2008 -pedantic
    -fbacktrace

Redundant flags
===============

RFLAGS=-fimplicit-none -Wline-truncation -std=f2008
===================================================

Warning flags
=============

WFLAGS=-Waliasing -Wampersand -Wconversion -Wsurprising -Wc-binding-type\
 -Wintrinsics-std -Wno-tabs -Wintrinsic-shadow -Wtarget-lifetime\
 -Wreal-q-constant -Wunused -Wextra -Wcharacter-truncation\
 -Wunused-parameter -Wimplicit-interface
=========================================================================

b)  Production flags:

    -O3 -march=native -fimplicit-none -Wall -Wline-truncation
    -fwhole-file\
     -std=f2008

http://stackoverflow.com/questions/3676322/what-flags-do-you-set-for-your-gfortran-debugger-compiler-to-catch-faulty-code

