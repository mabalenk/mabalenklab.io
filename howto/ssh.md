HOWTO: Setup SSH host aliases and paswordless access with RSA key pairs
=======================================================================

a)  Generate RSA public-private key pair

    ssh-keygen -t rsa -C "<email_address>"

b)  Skip passphrase entering (do not enter passphrase)

c)  Copy public SSH key to remote server

    scp -v \~/.ssh/<keyname>.pub
    <username>@<servername>:\~/.ssh/authorized\_keys

d)  If necessary add new public keys by copying public key into new line
    of \~/.ssh/authorized\_keys

e)  Configure host aliases in '\~/.ssh/config' file

    Host \* PreferredAuthentications publickey,keyboard-interactive
    ServerAliveInterval 120 ServerAliveCountMax 30

    Host hpdc User <username> HostName hpdc.eeecs Port <port_no>
    IdentityFile \~/.ssh/<keyname> // no '.pub' extension required

f)  Use ssh and scp as

    ssh hpdc OR scp file.txt hpdc:\~


