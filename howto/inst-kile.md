HOWTO: Install Kile 2.1b5 from source
=====================================

1)  Install prerequisite packages:

    KDE 4.4 incl. devel. packages Qt 4.5 or better incl. devel. packages
    CMake 2.6.2 or better GCC 4.4 or better

2)  Obtain Kile source files from:

    http://kile.sourceforge.net/download.php

3)  Extract files from tar ball archive:

    tar -xjvf kile-2.1b5.tar.bz2

4)  Change to Kile source directory:

    cd kile-2.1.b5

5)  Create build directory and configure build:

    mkdir build

    cmake .. -DCMAKE\_INSTALL\_PREFIX=/usr/share/kde4/apps/kile\
     -DCMAKE\_BUILD\_TYPE="Debug"

    make -j 2 make install -j 2

6)  Set KDE path in \~/.bashrc file:

    KDEDIRS=/usr/share/kde4/apps/kile:\$KDEDIRS

7)  Create soft link:

    ln -s /usr/share/kde4/apps/kile/bin/kile /usr/bin/kile


