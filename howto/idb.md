HOWTO: Make Intel Debugger (idb) work with openSUSE 13.1
========================================================

1)  Change GTK2 and GTK3 themes from default Oxygen to Adwaita in

    Start --\> Favorities --\> Configure Desktop --\> Application
    Appearance --\> GTK

2)  Select a GTK2 theme: Adwaita Select a GTK3 theme: Adwaita

3)  Change key bindings to avoid conflicts with Yakuake

Inside idb: Options --\> GUI Preferences --\> Keys

     Step             --> F1
     Run Until Caller --> Shift+F1
     Next             --> F2

HOWTO: Launch Intel Debugger in parallel mode
=============================================

mpirun -np 1 xterm -e idb main

