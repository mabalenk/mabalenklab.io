HOWTO: Highlight C source code for coloured LaTeX output
========================================================

source-highlight -n -i input.c -o output.tex -s C/F90 -f latexcolor

