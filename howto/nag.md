HOWTO: Link Fortran programs against NAG library
================================================

a)  nagfor -I[INSTALL\_DIR]/nag\_interface\_blocks driver.f90\
     [INSTALL\_DIR]/lib/libnag\_vl.a -framework vecLib\
     -framework IOKit -framework CoreFoundation

b)  nagfor -I[INSTALL\_DIR]/nag\_interface\_blocks driver.f90\
     [INSTALL\_DIR]/lib/libnag\_vl.dylib -framework vecLib\
     -framework IOKit -framework CoreFoundation

c)  nagfor -I[INSTALL\_DIR]/nag\_interface\_blocks driver.f90\
     [INSTALL\_DIR]/lib/libnag\_nag.a\
     -framework IOKit -framework CoreFoundation

d)  nagfor -I[INSTALL\_DIR]/nag\_interface\_blocks driver.f90\
     [INSTALL\_DIR]/lib/libnag\_nag.dylib\
     -framework IOKit -framework CoreFoundation

to set DYLD\_LIBRARY\_PATH, or
DYLD\_LIBRARY\_PATH=[INSTALL\_DIR]/lib:\${DYLD\_LIBRARY\_PATH} export
DYLD\_LIBRARY\_PATH

