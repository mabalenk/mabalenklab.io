HOWTO: Mount remote Windows partition
=====================================

a)  Create mount point

    mkdir -p /mnt/scan

b)  Mount partition

    mount -t cifs //Printserver.svc.enst-bretagne.fr/scan -o
    username=<username>,password=<password> /mnt/scan

http://www.cyberciti.biz/tips/how-to-mount-remote-windows-partition-windows-share-under-linux.html

