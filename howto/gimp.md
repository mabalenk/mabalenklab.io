HOWTO: Round image corners in GIMP
==================================

1)  Start GIMP and load an image file

2)  Lower resolution to 500x500 if necessary:

    Image -\> Scale Image

3)  Add transparency if necessary:

    Layer -\> Transparency -\> Add alpha channel

4)  Select -\> All

5)  Select -\> Rounded Rectangle

6)  Set corner radius between 10--20%, confirm with O.K.

7)  Invert selection:

    Select -\> Invert (Inverse)

8)  Cut corners in inverted selection:

    Edit -\> Cut (Ctrl+x OR Cmd+x)

9)  Export image in PNG or JPG

http://www.tekstadventure.nl/branko/blog/2008/12/how-to-make-rounded-corners-with-the-gimp

HOWTO: Correct white balance
============================

1)  Launch "Layers" menu

2)  Duplicate layer

3)  Make all future modifications to duplicated layer

4)  Adjust white balance automatically

    Colors -\> Auto -\> White Balance

http://pareandfocus.com/index.htm/white-balance-with-gimp/

