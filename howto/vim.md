HOWTO: Use vim efficiently
==========================

Search for upper case letters
-----------------------------

/\u
/\w\u

Search for abbreviations
------------------------

/\v<[A-Z]+\>

Copy text to clipboard buffer in visual mode
--------------------------------------------

a)  Select text for copying

b)  "+y

Copy word under cursor
----------------------

If cursor in middle of word

yiw OR biw

If cursor ar beginning of word

yw

Sorting
-------

Sort selected text in file

select with visual mode (v, V) !sort

Replacement
-----------

Replace multiple consecutive spaces with single space in Vim

%s![\^ ]\zs  +! !g

Many substitutions can be done in Vim easier than with other regex
dialects by using the \zs and \ze meta-sequences. What they do is to
exclude part of the match from the final result, either the part before
the sequence (\zs, “s” for “start here”) or the part after (\ze, “e” for
“end here”). In this case, the pattern must match one non-space
character first ([\^ ]) but the following \zs says that the final match
result (which is what will be replaced) starts after that character.

Highlighting
------------

Highlight current line

:set cursorline

Scrolling
---------

Ctrl + e -- move screen up one line at a time Ctrl + y -- move screen
down one line at a time Ctrl + u -- move screen up 1/2 page Ctrl + d --
move screen down 1/2 page Ctrl + b -- move screen up 1 page Ctrl + f --
move screen down 1 page

zz -- move current line to the middle of the screen zt -- move current
line to the top of the screen zb -- move current line to the bottom of
the screen

H -- move to the top of the screen M -- move to the middle of the screen
L -- move to the bottom of the screen

\^ -- move to the first non-blank character of the line g\_ -- move to
the last non-blank character of the line

Bookmarking
-----------

mg -- create bookmark called 'g' \`g -- go to bookmark called 'g'

Scrollbinding
-------------

:set scrollbinding // :set scb :set noscrollbinding // :set noscb

Move to matching bracket
------------------------

%

Search for current word (word under cursor)
-------------------------------------------

-   Search forwards for next occurence of current word \# Search
    backwards for previous occurence of current word

Managing large code base with vim and ctags
-------------------------------------------

1)  Run ctags recursively over entire source code directory

    ctags -R ../.. \# tags file is created on output

2)  Open source code file with vim

3)  Navigate to subroutine

4)  Press 'Ctrl+]' to jump to subroutine definition

5)  Press 'Ctrl+t' to jump back to original location

6)  Other commands to try:

:tag <identifier> :tags Ctrl+] Ctrl+t :tselect :stjump <identifier>

http://www.scholarslab.org/research-and-development/code-spelunking-with-ctags-and-vim/

Save file as
------------

1)  Write current state of 'hello.txt' into file 'world.txt', keep
    working on original file 'hello.txt':

:w world.txt

2)  Write current state of 'hello.txt' into file 'world.txt', start
    working on new file 'world.txt':

:sav world.txt

http://stackoverflow.com/questions/4980168/vim-save-as-file-and-keep-working-on-original

Enable persistent undo (maybe placed into .vimrc file)
------------------------------------------------------

set undodir=\~/.vim/undodir set undofile set undolevels=1000 set
undoreload=10000

Enable code folding (place into .vimrc)
---------------------------------------

set foldmethod=manual

Enable autosave of folds (place into .vimrc)
--------------------------------------------

au BufWinLeave ?\* mkview au BufWinEnter ?\* silent loadview

Commands over folds
-------------------

z{c,C} -- close {all} folds z{o,O} -- open {all} folds z{a,A} -- toggle
{all} folds z{r,R} -- reduce {all} folds z{m,M} -- close one more level
of folds

Mappings to control folds with <F9> (place into .vimrc)
-------------------------------------------------------

inoremap <F9> <C-O>za nnoremap <F9> za onoremap <F9> <C-C>za vnoremap
<F9> zf

Title case
----------

:s/\<(\w)(\w*)\>/\u\1\L\2/g

