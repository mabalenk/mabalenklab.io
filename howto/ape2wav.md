---
layout: page
permalink: /howto/ape2wav/
---

HowTo: Convert APE to WAV and Split Using CUE
=============================================

a) Convert:

    ffmpeg -i input.ape output.wav

b) Split:

    bchunk -w input.wav input.cue track
