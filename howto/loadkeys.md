HOWTO: Change keyboard layout in text mode
==========================================

1)  List available layouts

    ll /usr/share/kbd/keymaps/i386/

2)  Change to specific layout

    loadkeys /usr/share/kbd/keymaps/i386/qwerty/us.kmap.gz

http://how-to.wikia.com/wiki/Howto\_change\_the\_keyboard\_layout\_in\_text\_mode\_on\_a\_linux-based\_operating\_system

