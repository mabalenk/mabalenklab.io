HOWTO: Set up vim-latex
=======================

1)  Create configuration file \~/.vim/ftplugin/tex.vim:

" set viewer rules let g:Tex\_ViewRule\_dvi = 'okular' let
g:Tex\_ViewRule\_ps = 'okular' let g:Tex\_ViewRule\_pdf = 'okular'

" set compile rules let g:Tex\_CompileRule\_dvi = 'latex -src-specials
-interaction=nonstopmode $*' let g:Tex_CompileRule_ps  = 'ps2pdf $*' let
g:Tex\_CompileRule\_pdf = 'pdflatex -interaction=nonstopmode \$*'

" set default target format let g:Tex\_DefaultTargetFormat = 'dvi'

" disable use of makefile let g:Tex\_UseMakefile = 0

" set intendation for LaTeX files set sw=2

" use <C-n> to automatically cycle through all the labels set
iskeyword+=:

2)  Add to \~/.vimrc:

" make vim to invoke LaTeX-Suite when opening a TeX file filetype plugin
on

" set grep to always generate a file name set grepprg=grep -nH \$\*

" enable automatic indentation as you type filetype indent on

" load vim-latex for empty TeX files let g:tex\_flavor='latex'

