HOWTO: Convert MP4 into AVI
===========================

1)  mencoder -oac pcm -ovc copy persepolis\_fr.mp4 -o persepolis\_fr.avi

Options:

     -oac pcm ---encodes audio stream into an uncompressed PCM format 
     -ovc copy---does not encode video stream, just copy
