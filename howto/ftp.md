HOWTO: Use command-line utility "ftp"
=====================================

1)  Initiate an FTP connection with FTP-server

    ftp; open <site.com> or ftp <site.com>

2)  Enter username on the FTP server

    <username>

3)  Enter password on the FTP server

    <password>

4)  Change to target directory with

    cd

5)  List directory contents with

    ls -l

6)  Toggle down/upload confirmation for each file with

    prompt

7)  Download file or multiple files with

    get/mget <filename.ext>

8)  If prompt is on it is possible to manually select files for download

    mget \*.avi

9)  Confirm each download

    y/n

10) Upload file or multiple files with

    put/mput <filename.ext>

11) Change directory or view current directory on local machine

    lcd lpwd


