HOWTO: Use RPM utility
======================

1)  Install package

    rpm -Uvh <pkg_name>.rpm

2)  Reinstall already installed package

    rpm -ivh --replacepkgs <pkg_name>.rpm

3)  Install package replacing conflicing files

    rpm -ivh --replacefiles <pkg_name>.rpm

4)  Force installation ignoring dependencies

    rpm -ivh --nodeps <pkg_name>.rpm

5)  Uninstall package

    rpm -e <pkg_name>

6)  Uninstall ignoring dependencies

    rpm -e --nodeps <pkg_name>

7)  Upgrade package

    rpm -Uvh <pkg_name>.rpm

8)  Downgrade package

    rpm -Uvh --oldpackage <pkg_name>.rpm

9)  Querying installed packages

    rpm -qa | grep <package_name>

10) Verify package containing particular file

    rpm -Vf /bin/vi

11) Use another root file tree, e.g. /dev/sda2 mounted to /mnt in
    recovery mode

    rpm --root /mnt ...

https://access.redhat.com/site/documentation/en-US/Red\_Hat\_Enterprise\_Linux/3/html/System\_Administration\_Guide/s1-rpm-using.html

