---
layout: page
permalink: /howto/git/
---

HowTo: Use `git` effectively[^1]
================================

a) Setup `git`:

    Set default name for git to use when you commit
    git config --global user.name "Your Name Here"

    # Set default email for git to use when you commit
    git config --global user.email "your_email@example.com"

    # Set git to use the credential memory cache
    git config --global credential.helper cache

    # Line ending preferences
    git config --global core.autocrlf false
    git config --global core.safecrlf true

b) Add aliases to `git` configuration file `~/.gitconfig`:
{% highlight git %}
[alias]
  co   = checkout
  ci   = commit
  st   = status
  br   = branch
  hist = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short
  type = cat-file -t
  dump = cat-file -p
{% endhighlight %}

c) Add aliases to bash profile file `~/.bashrc` OR `~/.profile`:

{% highlight bash %}
  # Git aliases
  alias gs='git status '
  alias ga='git add '
  alias gb='git branch '
  alias gc='git commit'
  alias gd='git diff'
  alias go='git checkout '
  alias gk='gitk --all&'
  alias gx='gitx --all'

  alias got='git '
  alias get='git '
{% endhighlight %}

d) Clone repository:

    git clone https://gitlab.com/mabalenk/rightsizer.git

e) Add files to local repository:

    git add <files>

f) Stage changes for commit:

    git add <files_changed>

    git add -A  # stage all
    git add .   # stage new and modified without deleted
    git add -u  # stage modified and deleted without new

g) Unstage file for commit:

    git reset <file_name>

h) Commit changes:

    git commit -m "<commit_message>" OR
    git commit -F <log_file>

i) Push changes to `git` server:

    git push

j) Obtain latest changes from `git` server:

    git checkout git pull

k) Amend comment for last commit:

    git commit --amend -m "<new_commit_message>"

l) View branches:

    git branch

m) Create new branch:

    git checkout -b <branch_name>
    git push origin <branch_name>

n) Swith to another branch:

    git checkout <branch_name>

o) Delete local branch:

    git branch -d <branch_name> OR
    git branch -D <branch_name>

p) Delete remote branch:

    git push origin --delete <branch_name>

q) Merge one branch with another branch:

    git checkout <branch1>
    git merge    <branch2>
    git push

r) Get version of file from given commit

    git checkout <commit> <file_name>

s) Revert to state of previous commit (everything done since then will be lost):

    git reset --hard <commit_id>

t) Discard changes in working copy that are not in index:

    git stash save --keep-index
    git stash drop

u) Recover deleted files, if no commit was made after delete:

    git checkout .

v) Rename project on GitLab:

    git remote set-url origin git@gitlab.com:<username>/<projname>.git

[^1]: Based on [A](https://help.github.com/articles/set-up-git),
               [B](http://myprogrammingblog.com/2012/01/20/github-how-clone-github-repo-how-to-push-to-github-how-to-get-files-from-github-ubuntu), 
               [C](http://stackoverflow.com/questions/179123/how-do-i-edit-an-incorrect-commit-message-in-github) and
               [D](http://stackoverflow.com/questions/11956710/git-recover-deleted-file-where-no-commit-was-made-after-the-delete).
