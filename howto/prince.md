HOWTO: Pretty print HTML/XML documents (Webpages)
=================================================

1)  Download and install Prince XML from http://www.princexml.com

2)  Modify provided Wikipedia CSS: decrease infobox width field to 84%

3)  Print Wikipedia articles from command-line

    prince --no-author-style -s \~/Quellkoden/prince/wiki3.css\
     http://en.wikipedia.org/wiki/Miami -o miami\_wiki.pdf


