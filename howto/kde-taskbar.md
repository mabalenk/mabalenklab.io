---
layout: page
permalink: /howto/kde-taskbar/
---

HowTo: Restore default [KDE](https://www.kde.org) taskbar settings
==================================================================

a) Type in terminal window:

    kquitapp plasma
    rm -rvf ~/.kde4/share/config/plasma-appletsrc
    rm -rvf ~/.kde4/share/config/plasma-desktop*
    plasma &
