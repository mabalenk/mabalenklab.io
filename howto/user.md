HOWTO: Manage users
===================

1)  usermod -l <new-username> <old-username>

2)  usermod -u <UID> <new-username>, where UID in (5000, 10000)

3)  id <new-username>

4)  Add a existing user to existing group

    usermod -a -G ftp tony

http://www.cyberciti.biz/faq/howto-change-rename-user-name-id/
http://www.cyberciti.biz/faq/howto-linux-add-user-to-group/

