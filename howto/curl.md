---
layout: page
permalink: /howto/curl/
---

HowTo: Use `curl` effectively
=============================

a) Download multiple files with `curl`:

    curl -u <username>:<passwd> ftp://address.com/files/file[1-5].ext -o file#1.ext
