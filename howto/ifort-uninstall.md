HOWTO: Uninstall the Intel Compilers for Linux
----------------------------------------------

0)  Use 'uninstall.sh' script if possible

To uninstall the Intel compilers for Linux, the best and recommended
method is to use the uninstall.sh script in the /bin directory under
your compiler installation, typically
/opt/intel/composer\_xe\_<version>.<update>.<build>/bin/uninstall.sh

1)  Determine if your installation host has RPM records for the
    installation(s)

a)  Check to see if there are Intel compiler RPM entries:

        rpm -qa | grep intel-

b)  Find your build number, such as "192" or "038" in the names above as
    an example. We will use that build number to remove all RPMs for
    that compiler build.

c)  Execute the following command to remove all RPMS associated with
    your compiler build number:

        rpm -e --allmatches `rpm -qa | grep intel- | grep <build>`

d)  Verify your work with

        rpm -qa | grep intel

    to confirm that all Intel compiler RPMs are removed from the RPM
    database.

2)  Remove Intel compiler files left over in the installation directory
    (if any)

If there are any left over Intel Compiler files in the installation
directory, /opt/intel by default or wherever you installed the compiler,
remove all of those EXCEPT FOR the licenses directory, typically
/opt/intel/licenses. Licenses kept in the <installation dir>/licenses
should be kept, do not delete that directory.

Use 'rm -Rf <install dir>/<subdirs>' to remove all directories in
<install dir> except for the licenses/ directory. Remove all symbolic
links in <install dir>

3)  Remove \~/intel directory (if it exists)

Non-root installations will create a \~/intel (your home directory/intel
) subdirectory by default. Here, the installer will keep track of files
installed by the Intel compiler installer. Remove this directory as it
can contain past history of the non-root compiler installation.

https://software.intel.com/en-us/articles/uninstall-intel-compiler-for-linux-when-uninstallsh-is-missing-or-not-functioning

