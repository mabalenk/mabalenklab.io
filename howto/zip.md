HOWTO: Create multiple archives with a limited maximum size
-----------------------------------------------------------

a)  zip -Z bzip2 -s 1850m archive \*

OR

b)  zip -Z store -s 1850m archive *, zip -0 -s 1850m archive *

in case no compression is required

c)  Archive entire directory structure

    zip -R -Zb archive \*


