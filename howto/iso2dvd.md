HOWTO: Burn DVD-ISO image
=========================

1)  Use growisofs to write a pre-mastered ISO-image to DVD:

    growisofs -dvd-compat -Z /dev/dvd=image.iso OR /dev/sr0=image.iso


