HOWTO: Get system information on Linux
======================================

1)  Summary table of CPU, Memory, Disk, OS and Graphics card information

    Launch Konqueror with sysinfo:/ in location bar Same as clicking on
    My Computer on Desktop

2)  Operating system version information

    cat /etc/SuSE-release

OR

     ls -l /etc/*release
     cat   /etc/*release

3)  Linux Kernel Version

    uname -r

4)  Detailed CPU information

    cat /proc/cpuinfo

5)  Detailed GPU information

    /sbin/lspci /sbin/lspci -v -s "<slot_name>"

6)  Detailed Memory information

    cat /proc/meminfo

7)  Simple Memory information

    free -m

HOWTO: Get CPU information on macOS
===================================

     sysctl -n machdep.cpu.brand_string
