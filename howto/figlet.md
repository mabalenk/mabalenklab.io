HOWTO: Print a phrase with every available FIGlet font on a system
------------------------------------------------------------------

find /usr/share/figlet -name '*.flf' \> figlet-fonts.txt find
/usr/share/figlet -name '*.flc' \> figlet-fonts.txt awk '{ print \$0,
system("figlet -f " \$0 " Hello, World!") }' figlet-fonts.txt \>
output.txt

