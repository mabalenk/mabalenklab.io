---
layout: page
permalink: /howto/arch-restore/
---

HowTo: Restore [Arch Linux](https://www.archlinux.org) after kernel panic
=========================================================================

a)  Boot from [live USB/CD](https://www.archlinux.org/download)

b)  Find out root partition:

    fdisk -l

c)  Mount root partition:

    mount /dev/sda6 /mnt

d)  Mount other devices:

    mount --bind /dev /mnt/dev mount --bind /proc /mnt/proc mount --bind
    /sys /mnt/sys

e)  Change root partition (requires `arch-install-scripts`):

    chroot /mnt /bin/bash OR arch-chroot /mnt /bin/bash

f)  Add alternative DNS 8.8.8.8 in `/etc/resolv.conf`, in case internet is
    not working:

g)  Roll back to previous kernel version:

    find /var/cache/packman/pkg -name "linux-4\*"

h)  Downgrade kernel with `pacman`:

    pacman -U /var/cache/pacman/pkg/linux-4.xx-x.pkg.tar.xz

i)  Update system (optionally):

    pacman -Suy

j) Re-install key packages responsible for system boot (optionally):

    pacman -S udev kernel mkinitcpio mkinitcpio -p

k) Exit `chroot` and reboot:

    exit; reboot
