HOWTO: Get process information based on process name
====================================================

1)  Get process information by process name. Returns process ID.

    ps -def | grep <kpdf>

2)  Processes that belong to specific user.

    top -U <username>


