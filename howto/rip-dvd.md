HOWTO: Rip a DVD
================

1)  Copy all VOB-files from a DVD

    vobcopy -i /media/dvd -m

2)  Start "three pass encoding" using mencoder

a)  Identify desired audio stream

        mplayer -dvd 1 -v

    Look for values of "aid" (audio id) and "language" (two letter
    language code)

b)  Encode audio stream with mencoder

        nice -+19 cat *.VOB | nice -+19 mencoder -ovc frameno -o frameno.avi -oac mp3lame -lameopts abr:br=128 -aid 137 -

c)  Generate a preview (first pass)

        nice -+19 mencoder -oac copy -o /dev/null -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=2817:vhq:vpass=1 \
             -vf scale=720:576 -npp lb -ss 0:20 -endpos 0:30 VTS_02_1.VOB

d)  Generate a preview (second pass)

        nice -+19 mencoder -oac copy -o preview.avi -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=2817:vhq:vpass=2 \
             -vf scale=720:576 -npp lb -ss 0:20 -endpos 0:30 VTS_02_1.VOB

http://www.bunkus.org/dvdripping4linux/single/ Moritz Bunkus, August 23,
2002

