HOWTO: Use vimdiff
==================

a)  Basic control

ctrl+w ctrl+w - switch windows do - diff obtain dp - diff put [c -
previous difference ]c - next difference :diffupdate - diff update ;)
:syntax off - syntax off zo - open folded text zc - close folded text

diffoff - disablehighlighting windo diffthis - enable highlighting

only two files you can copy differences with: :diffget and :diffput

b)  Obtain all differences from other file

:%diffget OTHER

http://andrejk.blogspot.fr/2008/04/vimdiff-howto.html

