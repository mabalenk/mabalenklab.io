HOWTO: Use command-line utility "rename"
========================================

1)  Syntax

    rename 's/look\_for/end\_result/' target files

2)  Rename all files ending with ".jpg.jpg" to ".jpg"

    rename 's/.jpg.jpg/.jpg/' \*.jpg.jpg

3)  Add prefix "prfx\_" to all files

    rename 's/\^/prfx\_/' \*

4)  Remove "A B C " prefix from all files = Replacing prefix with
    nothing

    rename 's/A B C //' \*

5)  Remove spaces

    rename 's/ //g' \*

6)  Replace spaces with "\_"

    rename 's/ /\_/g' \*

7)  Remove minus sign globally in all DOC-files

    rename "-" "" g \*.doc

8)  Convert all FLAC filenames to lowercase:

    rename 'y/A-Z/a-z/' \*.flac


