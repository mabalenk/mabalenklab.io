Gauss: NVIDIA Corporation G96 [Quadro FX 580] (rev a1) (prog-if 00 [VGA
controller])

HOWTO: Install NVIDIA graphics card on openSUSE 12.3 (64-bit)
=============================================================

a)  Find out graphics card version

    sudo /sbin/update-pciids /sbin/lspci -v | less

b)  Install dependencies for the NVIDIA driver

    gcc, kernel-{desktop-devel, devel, source, syms, xen-devel}, make

c)  Download the NVIDIA binary driver from

    http://www.nvidia.com/drivers

d)  Log out, login in console mode as root

e)  Blacklist nouveau driver

    echo "blacklist nouveau" \>\> /etc/modprobe.d/50-blacklist.conf

f)  Run NVIDIA installer

    sh /home/amyjones/nvidia/NVIDIA-Linux-x86\_64-319.17.run

g)  After installer has finished type:

    modprobe nvidia

h)  Restart X server

    rcxdm start

i)  Configure graphics card with

    nvidia-settings

j)  Check driver works correctly by running

    glxgears

k)  Increase system font sizes

    Configure Desktop -\> Application Appearance -\> Fonts

    9 --\> 10, 8 --\> 9

http://www.cyberciti.biz/faq/linux-tell-which-graphics-vga-card-installed/
http://en.opensuse.org/SDB:NVIDIA\_the\_hard\_way

