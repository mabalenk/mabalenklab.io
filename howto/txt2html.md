HOWTO: Install txt2html Perl script
===================================

1)  Install Perl 5.8.1++

    sudo zypper in perl

2)  Install required Perl packages (See perl.txt)

    cpan

    install Module::Build Getopt::Long Getopt::ArgvFile Pod::Usage
    File::Basename Test::More YAML::Syck

3)  Install txt2html

    sudo perl Build.PL sudo ./Build sudo ./Build test sudo ./Build
    install

txt2html README file

