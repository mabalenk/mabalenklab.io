HOWTO: Convert files from CP-1251 HTML to UTF-16 MOBI
=====================================================

1)  uniconv -out book\_unicode.html -in book\_non\_unicode.html\
     -decode cp-1251 -encode utf-16

2)  kindlegen -unicode book\_unicode.html


