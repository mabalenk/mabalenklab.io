HOWTO: Install ScaLAPACK on Mac OS X
====================================

a)  Install OpenBLAS-devel (based on GotoBLAS2) from MacPorts

b)  Use ScaLAPACK Python installer script with following options

    SERTDIR=$HOME/repository/git/sert  MPIBINDIR=$SERTDIR/software/mpich-gnu/bin
    MPILIBDIR=$SERTDIR/software/mpich-gnu/lib  MPIINCDIR=$SERTDIR/software/mpich-gnu/include

    ./setup.py --prefix=$SERTDIR/software/scalapack \
                --mpibindir=$MPIBINDIR --mpicc=$MPIBINDIR/mpicc \
                --mpif90=$MPIBINDIR/mpifort
    --mpirun=$MPIBINDIR/mpiexec \
                --mpiincdir=$MPIINCDIR
    --blaslib=/opt/local/lib/libopenblas.a\
     --downlapack

    OR bash script 'scalapack-install.sh' in '\~/bin'

c)  Resulting library locations:

    BLAS: /opt/local/lib/libopenblas.a

    LAPACK: -L/Users/mabalenk/repository/git/sert/software/scalapack/lib
    -ltmg -lreflapack

    BLACS/ScaLAPACK:
    -L/Users/mabalenk/repository/git/sert/software/scalapack/lib
    -lscalapack


