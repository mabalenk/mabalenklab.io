---
layout: page
title:  About me
order:  1
---

<style>
  .reverse {
    unicode-bidi: bidi-override;
    direction:    rtl;
  }
</style>

<img style="float: left; margin-right: 24pt; margin-bottom: 12pt; width: 35%" src="{{ site.url }}/assets/userpic.jpg"
  onmouseover="this.src='{{ site.url }}/assets/userpic-bw.jpg'"
  onmouseout ="this.src='{{ site.url }}/assets/userpic.jpg'">

<table>
<tr>
  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
  <td><span class="reverse">{{ site.email }}</span></td>
</tr>
<tr>
  <td style="vertical-align:top; text-align:center"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
  <td>{{ site.address }}</td>
</tr>
<tr>
  <td><i class="fa fa-phone" aria-hidden="true"></i></td>
  <td><span class="reverse">{{ site.phone }}</span></td>
</tr>
</table>

My name is Maksims Abaļenkovs (Maxim Abalenkov, Максим Абаленков). I&nbsp;am
a&nbsp;Senior R&amp;D Software Engineer at
<a href="https://www.keysight.com/us/en/home.html">Keysight Technologies</a>.

I&nbsp;lived in five European countries (Russia, Latvia, Germany, France and
the UK), studied in four different universities (the 
<a href="http://www.lu.lv">University of Latvia,</a> 
<a href="http://www.rtu.lv">Riga Technical University,</a> 
<a href="http://www.uni-duesseldorf.de">Heinrich-Heine Universität Düsseldorf</a>
and the <a href="http://www.manchester.ac.uk">University of Manchester</a>) and
speak four and a&nbsp;half languages (&#x1F1EC;&#x1F1E7;&nbsp;English,
&#x1F1E9;&#x1F1EA;&nbsp;German, &#x1F1F7;&#x1F1FA;&nbsp;Russian,
&#x1F1F1;&#x1F1FB;&nbsp;Latvian and &#x1F1EB;&#x1F1F7;&nbsp;French).
I&nbsp;studied Mathematics and Statistics, (Advanced) Computer Science, Physics
and Electrical Engineering. By education I&nbsp;am a&nbsp;Computer Scientist,
but Mathematician at heart. Most of all I&nbsp;am excited and inspired by
Mathematics applied to the solution of real-world problems: be it engineering
or science. I&nbsp;truly believe it is the mother of all sciences.

In my free time I&nbsp;like to swim, learn to play <i class="fas fa-guitar"
aria-hidden="true"></i> guitar and ride my <i class="fa fa-bicycle"
aria-hidden="true"></i> bicycle.
