---
layout:     page
title:      CV
permalink: /cv/
order:      2
---

<style>
nice {
    color: grey;
}
</style>

<!-- {% include toc.html %} -->

<h1>Curriculum Vitae&nbsp;[<i class="fa fa-download" aria-hidden="true"></i> (pdf)]({{ site.url }}/assets/abalenkovs--cv.pdf)</h1>
# Main Areas of Interest

Computational electromagnetics, Numerical linear algebra, High-performance computing, Linux/Unix


# Skills Profile

* Specialist in developing and implementing computational electromagnetics techniques (Finite-Difference Method, frequency dependence, subgridding and Fast Multipole Method).
* High-performance computing engineer with focus on shared-, distributed- and GPU-memory development.
* Technical leader on cross-functional projects guiding junior developers.
* Expert in numerical approximation methods and parallel programming runtimes obtained during industrial and academic projects.
* Professional in&nbsp;designing, profiling and optimising computational workflows aimed at solution of specific challenges.
* Numerical linear algebra advocate.

# <i class="fa fa-building" aria-hidden="true"></i> Research and Development Work Experience

04/2022&ndash;present **Senior High Performance Software Engineer, [STFC](https://www.ukri.org/councils/stfc), Didcot, UK**

* Extending Integrated Flood Modelling for distributed-memory and GPU-enabled operation <nice>(C, StarPU, OpenMP, MPI, Perl).</nice>
* Designing and implementing an&nbsp;energy saving xApp for a&nbsp;practical OpenRAN deployment in Liverpool <nice>(Python, Perl, Docker).</nice>
* Profiled and optimised Markov Chain Monte Carlo method for shared-, distributed- and GPU-memory execution <nice>(C++, OpenMP, MPI, CUDA, Intel {Advisor, VTune Profiler}).</nice>
* Planned and delivered a&nbsp;hands-on one-day course on Rust for experienced developers <nice>(Rust).</nice>

09/2019&ndash;03/2022 **Research Software Engineer, [STFC](https://www.ukir.org/councils/stfc), Warrington, UK**

* Applied custom arbitrary float number formats for QR decomposition on CPU and FPGA architectures <nice>(C, C++, Perl, GNU Octave).</nice>
* Integrated parallel data format support for a&nbsp;computational chemistry suite. This work produced a&nbsp;sixfold reduction in output file size and twofold reduction in average memory utilisation <nice>(Fortran, MPI, NetCDF, HDF5).</nice>
* Trained machine learning algorithm to detect earthworm casts <nice>(Python, TensorFlow&nbsp;2).</nice>
* Created a&nbsp;novel method for validation and verification of an&nbsp;industrial artificial intelligence system <nice>(Python, Docker).</nice>
* Conducted performance tests of implicit-factorisation preconditioners on a&nbsp;range of system matrices <nice>(BOUT++, Nektar++, C++, PETSc).</nice>
* Prototyped graph-based algorithm to model power, sewage and mobile network outage due to flooding <nice>(Python, Podman, Docker).</nice>
* Containerised molecular dynamics and neutron reflectometry workflow <nice>(Perl, C++, MATLAB, Apptainer, Nextflow).</nice>

05/2015&ndash;10/2018 **Research Associate, [The University of Manchester](https://www.manchester.ac.uk), Manchester, UK**

* Parallelised iterative refinement algorithms for linear systems solution
in mixed precision. Please see [PLASMA&nbsp;24.8.7](https://github.com/icl-utk-edu/plasma) distribution.
* Developed
  *[Rightsizer](https://gitlab.com/mabalenk/rightsizer)*—an open-source
software package to detect an&nbsp;optimal tile size and a&nbsp;number of
[OpenMP](https://www.openmp.org) threads for parallel matrix
factorisations <nice>(C, OpenMP, PLASMA).</nice>
* Expanded [2DRMP](https://www.sciencedirect.com/science/article/pii/S0010465509002380) package to apply [PLASMA](https://github.com/icl-utk-edu/plasma) routines for matrix multiplication, eigenvalue decomposition, LU-factorisation and linear systems solution <nice>(Fortran, PLASMA).</nice>
* Derived a&nbsp;generic algorithm for calculation of finite-difference expressions of any order \\( n \\). Developing *[X-Stencil](https://gitlab.com/mabalenk/x-stencil)*—an&nbsp;open-source package to validate theoretical results in an&nbsp;FDTD-based method up to the&nbsp;10th order <nice>(Fortran, MATLAB).</nice>
* Developed a&nbsp;matrix casting approach to re-formulate the update equations of stencil-based methods in {1, 2, 3}D and speed-up the computation <nice>(Fortran).</nice>

03/2013&ndash;05/2015 **Research Engineer, [Télécom Bretagne](https://www.imt-atlantique.fr), Brest, France**

* Prototyped the&nbsp;Fast Multipole Method with Fast Fourier Transform software to speed-up solution of the forward problem in Electroencephalography applications. This project included the study of efficient convolution of circulant and block Toeplitz
tensors <nice>(Fortran, C++, FFTW, MATLAB, GNU&nbsp;Octave).</nice>
* Provided computational expertise to a&nbsp;team developing an&nbsp;automotive radar
  simulation platform in&nbsp;collaboration with [Renault](https://group.renault.com/en). Major contributions to the&nbsp;project included development and application of beamforming and Multiple Signal Classification algorithms for Angle-of-Arrival estimation in realistic scenarios <nice>(MATLAB).</nice>


# <i class="fa fa-university" aria-hidden="true"></i> Education

09/2007&ndash;12/2011 **<i class="fa fa-graduation-cap" aria-hidden="true"></i> [PhD in Electrical and Electronic Engineering](https://www.eee.manchester.ac.uk/study/postgraduate-research),** [The University of Manchester](https://www.manchester.ac.uk), UK

*Huygens Subgridding for the Frequency-Dependent Finite-Difference Time-Domain Method* [<i class="fa fa-download" aria-hidden="true"></i> (pdf)]({{ site.url }}/assets/abalenkovs--hsg-fd-fdtd-v1.2.pdf)

* Developed various Frequency-Dependent Finite-Difference Time-Domain algorithms with the Huygens Subgridding technique <nice>(Fortran, OpenMP, MPI, MATLAB).</nice>
* New solver allowed efficient simulation of electromagnetic wave propagation in multilayered media, *e.g.* human body.
* Proposed the solver application to optimise defibrillators by simulating defibrillation current in a&nbsp;human torso.

09/2006&ndash;09/2007 **<i class="fa fa-graduation-cap" aria-hidden="true"></i> [MSc in Advanced Computer Science](https://www.manchester.ac.uk/study/masters/courses/list/02069/msc-advanced-computer-science)**, [The University of Manchester](https://www.manchester.ac.uk), UK

*Large-Scale Finite-Difference Time-Domain Data Processing Using High Performance Systems* [<i class="fa fa-download" aria-hidden="true"></i> (pdf)]({{ site.url }}/assets/abalenkovs--fdtd_data_proc_using_hpc.pdf)

* Adapted the&nbsp;[HDF5](https://support.hdfgroup.org/HDF5) library for a&nbsp;parallel in-house Finite-Difference Time-Domain solver. The [HDF5](https://support.hdfgroup.org/HDF5) enabled logical data storage and efficient data processing <nice>(Fortran, HDF5).</nice>

10/2004&ndash;09/2006 **<i class="fa fa-graduation-cap" aria-hidden="true"></i> [BSc in Computer Science](https://www.uni-duesseldorf.de/home/studium-und-lehre-an-der-hhu/studium/alle-studiengaenge-von-a-z/studiengang-informationen/studiengaenge/informatik.html) with minor in Physics**, [Heinrich-Heine-Universität Düsseldorf](https://www.uni-duesseldorf.de), Germany

*Extraction and Storage of Web Structures* [<i class="fa fa-download" aria-hidden="true"></i> (pdf)]({{ site.url }}/assets/abalenkovs--extraction_storage_web_structures.pdf)


# <i class="fa fa-desktop" aria-hidden="true"></i> Programming Skills

[C](https://www.cprogramming.com)&nbsp;<nice>(5&nbsp;years),</nice>
[Fortran](https://fortranwiki.org/fortran/show/HomePage)&nbsp;<nice>(5y),</nice>
[Python](https://www.python.org)&nbsp;<nice>(2y),</nice>
[C++](https://www.cprogramming.com)&nbsp;<nice>(2y),</nice>
[Java](https://www.oracle.com/technetwork/java/index.html)&nbsp;<nice>(2y),</nice>
[Perl](https://www.perl.org)&nbsp;<nice>(1y),</nice>
[Rust](https://www.rust-lang.org)&nbsp;<nice>(6&nbsp;months),</nice>
[Julia](https://julialang.org)&nbsp;<nice>(6m),</nice>  
[OpenMP](https://www.openmp.org)&nbsp;<nice>(5y),</nice>
[MPI](https://mpi-forum.org)&nbsp;<nice>(2y),</nice>
[StarPU](https://starpu.gforge.inria.fr)&nbsp;<nice>(1y),</nice>
[CUDA](https://developer.nvidia.com/cuda-toolkit)&nbsp;<nice>(6m),</nice>
[oneAPI SYCL](https://www.intel.com/content/www/us/en/developer/tools/oneapi/data-parallel-c-plus-plus.html)&nbsp;<nice>(6m),</nice>  
[MATLAB](https://uk.mathworks.com/products/matlab.html)&nbsp;<nice>(2y),</nice>
[GNU Octave](https://www.gnu.org/software/octave)&nbsp;<nice>(2y).</nice>


# Extra Positions of Responsibility

08/2013&ndash;present **Scientific Journal Reviewer**

Active reviewer for the [IEEE Transactions on Antennas and Propagation](https://www.ieeeaps.org/publications/ieee-transactions-on-antennas-and-propagations/ieee-tap-home), and [Antennas and Wireless Propagation Letters](https://awpl.eleceng.adelaide.edu.au) journals.


# <i class="fa fa-users" aria-hidden="true"></i> Professional Affiliations

Member of
* [The Institute of Electrical and Electronics Engineers (IEEE)](https://www.ieee.org/index.html) [since 2010],
* [The Society for Industrial and Applied Mathematics (SIAM)](https://www.siam.org) [since 2010] and
* [The Applied Computational Electromagnetics Society (ACES)](https://www.aces-society.org) [since 2013].


# <i class="fa fa-globe" aria-hidden="true"></i> Languages

Speak four European languages fluently: &#x1F1EC;&#x1F1E7;&nbsp;English, &#x1F1E9;&#x1F1EA;&nbsp;German, &#x1F1F7;&#x1F1FA;&nbsp;Russian, &#x1F1F1;&#x1F1FB;&nbsp;Latvian and know &#x1F1EB;&#x1F1F7;&nbsp;French at an&nbsp;intermediate level.
