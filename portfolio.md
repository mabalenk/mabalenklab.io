---
layout:     page
title:      Portfolio
permalink: /portfolio/
order:      3
---

<ol reversed>
  <li><a href="#treesai++">TreesAI++ Integrated Climate Resilience Modelling</a></li>
  <li><a href="#name">Next Generation Modelling System: NAME</a></li>
  <li><a href="#liverpool_hdd">Liverpool City Region High Demand Density</a></li>
  <li><a href="#rustup">Rust-Up:&nbsp;Using Rust in Scientific and High Performance Computing Setting</a></li>
  <li><a href="#treesai">TreesAI Integrated Climate Resilience Modelling</a></li>
  <li><a href="#conf">Confidential: Database with a&nbsp;Web-Interface</a></li>
  <li><a href="#bighypo">Big Hypotheses:&nbsp;Improving Performance of Sequential Monte Carlo Method</a></li>
  <li><a href="#credo">CReDo:&nbsp;Climate Resilience Demonstrator</a></li>
  <li><a href="#alcnr">Ada Lovelace Code:&nbsp;Neutron Reflectometry</a></li>
  <li><a href="#wormright">WormRight</a></li>
  <li><a href="#preconditioners">Implicit-Factorisation Preconditioners for NEPTUNE Programme</a></li>
  <li><a href="#ecp">Exascale Computing Project</a></li>
  <li><a href="#macad">Mathematics for Competitive Advantage</a></li>
  <li><a href="#zipfstime">Validation and Verification of an&nbsp;AI/ML System</a></li>
  <li><a href="#dlmeso">Parallel I/O for DL_MESO:&nbsp;NetCDF and HDF5</a></li>
  <li><a href="#ummap">Enabling UMMAP to Understand GROMACS File Formats</a></li>
</ol>

<ol reversed>
<li>
<h2 id="treesai++">TreesAI++ Integrated Climate Resilience Modelling</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Geoffrey Dawson, Junaid Butt, Katharina Reusch</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C</a>,
    <a href="https://starpu.gitlabpages.inria.fr">StarPU</a>,
    <a href="https://www.openmp.org">OpenMP</a>,
    <a href="https://www.mpi-forum.org">MPI</a>,
    <a href="https://www.perl.org">Perl</a>
  </td></tr>
  <tr><td>Duration:</td><td>10&nbsp;months</td></tr>
  <tr><td>Status:</td><td>ongoing</td></tr>
</table>

<p class="portfolio">Integrated Flood Modelling&nbsp;(IFM) lies at the core of running GeoDN workflows. It is the main computation engine simulating the water flow during a&nbsp;flood. Currently IFM is parallelised for shared-memory execution via OpenMP. It provides good performance scaling, but is limited to a&nbsp;single node. This project aims to interact with large-scale models, i.e. multiple flood scenarios with different grid resolution for various regions (cities) running simultaneously. Hence, distributed memory parallelisation is vital to the project’s success.</p>

<p class="portfolio">My goal is to deliver a&nbsp;distributed memory version of IFM. I&nbsp;plan to apply StarPU runtime. StarPU is based on a&nbsp;novel task-based parallel programming paradigm. This runtime provides automatic scheduling and execution of computationally intensive kernels on CPU, GPU and FPGA in shared and distributed memory modes.</p>
</li>


<li>
<h2 id="name">Next Generation Modelling System: Numerical Atmospheric-dispersion Modelling Environment&nbsp;(NAME)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Andrew Sunderland</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://fortran-lang.org">Fortran</a>,
    <a href="https://www.gnu.org/software/make">GNU Make</a>
  </td></tr>
  <tr><td>Duration:</td><td>11&nbsp;months</td></tr>
  <tr><td>Status:</td><td>ongoing</td></tr>
</table>

<p class="portfolio">Numerical Atmospheric-dispersion Modelling Environment&nbsp;(NAME) is a&nbsp;Lagrangian particle model which calculates the dispersion of materials released into the atmosphere by tracking model &ldquo;particles&rdquo; through the modelled atmosphere. These particles move with the resolved wind described by input meteorology, which can vary in space and time. The particles motion also has a&nbsp;random component to represent the effects of atmospheric turbulence. A&nbsp;consequence of this is that no&nbsp;assumptions need to be made for the shape of the concentration distribution, such as are required in Gaussian plume models.</p>
</li>


<li>
<h2 id="liverpool_hdd">Liverpool City Region High Demand Density</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Alessandro Raschella</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.mathworks.com/products/matlab.html">MATLAB</a>
  </td></tr>
  <tr><td>Duration:</td><td>14&nbsp;months</td></tr>
  <tr><td>Status:</td><td>ongoing</td></tr>
</table>

<p class="portfolio">This project focuses on creating a&nbsp;series of experimental mobile network setups at various venues in Liverpool. The main aim of the project is to explore <a href="https://www.o-ran.org">Open Radio Access Network&nbsp;(OpenRAN)</a> approach to creating efficient mobile networks. My role in this project lies in identification of potential <a href="https://orandownloadsweb.azurewebsites.net/specifications">OpenRAN use-cases</a> that can benefit from AI/ML. Once OpenRAN use-cases are identified I will introduce AI/ML algorithms to optimise mobile networks depending on the workload. Currently I&nbsp;identified three OpenRAN use-cases to take forward

<ol style="list-style-type: lower-roman;">
  <li>Use-case&nbsp;4: QoE optimisation,</li>
  <li>Use-case&nbsp;5: Traffic steering,</li>
  <li>Use-case&nbsp;8: QoS based resource optimisation,</li>
  <li>Use-case&nbsp;21: Energy saving</li>
</ol></p>

<p class="portfolio">and started learning, how to apply MATLAB to prototype AI/ML scenarios. Successful ML application needs good domain knowledge and ability to extract quality features from the data. This feature extraction may take a&nbsp;considerable amount of time.</p>
</li>


<li>
<h2 id="rustup">Rust-Up:&nbsp;Using Rust in Scientific and High Performance Computing Setting</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Ciaron Howell</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.rust-lang.org">Rust</a>
  </td></tr>
  <tr><td>Duration:</td><td>6&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">I&nbsp;learnt Rust in a&nbsp;six-month period allocated by the Hartree Training team. I&nbsp;structured a&nbsp;one-day course into four pairs of lectures and exercises. Together with Ciaron Howell we developed over 200&nbsp;lecture slides in PowerPoint. I&nbsp;created multiple practical exercises to reinforce learning. There were three to six exercises for each section of the course. Ciaron and I&nbsp;prepared solutions for these exercises. The first course was delivered on April&nbsp;12, 2024.</p>
</li>


<li>
<h2 id="treesai">TreesAI Integrated Climate Resilience Modeling (with <a href="https://research.ibm.com/labs/uk">IBM Research</a>, <a href="https://darkmatterlabs.org">Dark Matter Labs</a>, <a href="https://www.lucidminds.ai">Lucid Minds</a>)</h2>
<table>
  <tr><td style="vertical-align:top">Developers:</td><td>Maksims Abaļenkovs, Sarah Jackson, Geoffrey Dawson, Junaid Butt, Katharina Reusch</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C</a>,
    <a href="https://www.openmp.org">OpenMP</a>,
    <a href="https://www.mpi-forum.org">MPI</a>,
    <a href="https://www.python.org">Python</a>,
    <a href="https://www.perl.org">Perl</a>
  </td></tr>
  <tr><td>Duration:</td><td>7&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">This was an&nbsp;HNCDI project in collaboration with <a href="https://research.ibm.com/labs/uk">IBM Research</a>, <a href="https://darkmatterlabs.org">Dark Matter Labs</a> and <a href="https://www.lucidminds.ai">Lucid Minds</a>. The main idea of the project consisted in creating a&nbsp;simulation workflow. This workflow predicts, how growing more trees in urban areas can help reduce flooding. The workflow was based around IBM’s climate modelling platform called GeoDN. I&nbsp;worked on the main mathematical engine of this workflow called Integrated Flood Modelling&nbsp;(IFM). I&nbsp;modified the C&nbps;source code to introduce additional command-line options. There were two new things that I&nbsp;added:

<ol style="list-style-type: lower-roman;">
  <li>opportunity to skip the infiltration stage and</li>
  <li>ability to profile the code, i.e.&nbsp;measure time taken by each simulation stage.</li>
</ol></p>

<p class="portfolio">I&nbsp;created multiple Perl scripts to conveniently launch experiments on Scafell Pike and developed a&nbsp;Docker container with IFM. I&nbsp;ensured the OpenMP environment variables were set correctly to fully exploit the shared-memory parallelism. I&nbsp;experimented with the distributed memory version of IFM, but unfortunately it was not possible to run it. There are considerable differences in input files between the shared- and distributed-memory versions of IFM.</p>
</li>


<li>
<h2 id="conf">Confidential: Database with a&nbsp;Web-Interface (with <a href="https://www.cubethinking.co.uk">Cube Thinking</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Mark Birmingham</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.mongodb.com">MongoDB</a>,
    <a href="https://www.rust-lang.org">Rust</a>,
    <a href="https://www.ecma-international.org/publications-and-standards/standards/ecma-262">JavaScript</a>
  </td></tr>
  <tr><td>Duration:</td><td>6&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">This is a&nbsp;confidential proof-of-concept project sponsored internally by the <a href="https://www.ukri.org/councils/stfc">STFC</a>. It's main aim is twofold:
<ol style="list-style-type: lower-roman;">
  <li>to help UK start-ups apply for funding and</li>
  <li>to help <a href="https://www.ukri.org/councils/innovate-uk">Innovate UK</a> sponsor promising UK companies</li>
</ol></p>

<p class="portfolio">I'm a&nbsp;principal investigator on this project. With help of Jason Kingston (<a href="https://www.cubethinking.co.uk">Cube Thinking</a>) and Richard Harding (<a href="https://www.hartree.stfc.ac.uk">Hartree Centre</a>) I&nbsp;envisioned and designed the entire system. The key technical elements of the system are the database and its web interface. I&nbsp;developed the backend powered by a&nbsp;<a href="https://www.mongodb.com">MongoDB</a> instance running in the cloud. I&nbsp;wrote multiple <a href="https://www.rust-lang.org">Rust</a> programs to retrieve the data from various sources, format and store it in the database.</p>
</li>


<li>
<h2 id="bighypo">Big Hypotheses:&nbsp;Improving Performance of Sequential Monte Carlo Method (with <a href="https://www.liverpool.ac.uk/electrical-engineering-and-electronics/research/data-science-signal-processing/signalprocessing">University of Liverpool, Signal Processing Group</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Alessandro Varsi</td></tr>
  <tr><td style="vertical-align:top">Tech&nbsp;Stack:</td>
  <td>
    <a href="https://isocpp.org">C++</a>,
    <a href="https://www.openmp.org">OpenMP</a>,
    <a href="https://www.mpi-forum.org">MPI</a>,
    <a href="https://developer.nvidia.com/cuda-toolkit">NVIDIA CUDA</a>,
    <a href="https://www.perl.org">Perl</a>,
    <a href="https://mc-stan.org">Stan</a>,
    <a href="https://mc-stan.org/users/interfaces/math">Stan Math Library</a>,
    <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/advisor.html">Intel Advisor</a>,
    <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html">Intel VTune Profiler</a>
  </td></tr>
  <tr><td>Duration:</td><td>18&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">Initial ambition of this project was to port computationally expensive phases of Sequential Monte Carlo with <a href="https://mc-stan.org">Stan</a>&nbsp;(SMCS), an&nbsp;in-house statistical modelling software, to GPUs.</p>

<p class="portfolio">First, I&nbsp;verified the SMCS code hotspots with Intel's <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/advisor.html">Advisor</a> and <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html">VTune Profiler</a>. I&nbsp;conducted performance profiling in shared and distributed memory environments. I&nbsp;discovered poor SMCS performance on shared-memory architectures.</p>

<figure class="imagecentre">
  <a href="{{ site.url }}/assets/plot/bighypo_smcs_advisor_prof.svg">
    <img src="{{ site.url }}/assets/plot/bighypo_smcs_advisor_prof.svg" alt="Big Hypotheses: Profiling parallel SMCS code with Intel Advisor">
  </a>
  <figcaption><cite>Results of profiling parallel SMCS code with Intel's <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/advisor.html">Advisor</a>. There were five experiments with various distributed process (p) and shared-memory thread (t) numbers. These process and thread combinations are shown in the legend. x-axis depicts the SMCS function names and y-axis&mdash;total time spent per function.</cite></figcaption>
</figure>

<p class="portfolio">I&nbsp;designed a&nbsp;prototype CUDA code to call <tt>log_prob</tt>, one of the key <a href="https://mc-stan.org/users/interfaces/math">Stan Math</a> functions used in SMCS. Unfortunately, numerous experiments with <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler-download.html">NVIDIA CUDA</a> compiler and its options proved fruitless. It was impossible to link together the core <a href="https://isocpp.org">C++</a> code with a&nbsp;CUDA kernel calling <tt>log_prob</tt>. This happened due to generous inclusions of <a href="https://mc-stan.org/users/interfaces/math">Stan Math</a> functions into the final code and defining most of <a href="https://mc-stan.org">Stan</a> code in <tt>*.h</tt> header files.</p>

<figure class="imagecentre">
  <a href="{{ site.url }}/assets/plot/bighypo_smcs_vtune_prof.svg">
    <img src="{{ site.url }}/assets/plot/bighypo_smcs_vtune_prof.svg" alt="Big Hypotheses: Profiling results of parallel SMCS code">
  </a>
  <figcaption><cite>Results of evaluating parallel SMCS code with Intel's <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html">VTune Profiler</a>. There were five experiments with various distributed process (p) and shared-memory thread (t) numbers. These process and thread combinations are shown in the legend. x-axis depicts the SMCS function names and y-axis&mdash;effective time spent per function.</cite></figcaption>
</figure>

<p class="portfolio">The team decided to take a&nbsp;step back, resolve shared-memory issues and tap into the GPU power via <a href="https://www.openmp.org">OpenMP</a> offload pragmas. Automatic-differentiation&nbsp;(AD) tapes shown to be the main roadblock preventing shared-memory scaling. <a href="https://mc-stan.org">Stan</a> developers suggested creating dedicated AD tapes for each <a href="https://www.openmp.org">OpenMP</a> thread. I&nbsp;programmed a&nbsp;prototype <a href="https://www.openmp.org">OpenMP</a> tool to intercept thread creation events and instantiate AD tapes. However, this did not resolve performance problems. The new culprits were identified: these are memory allocation calls inside <a href="https://www.openmp.org">OpenMP</a> parallel regions. Investigation is ongoing.</p>
</li>


<li>
<h2 id="credo">CReDo: Climate Resilience Demonstrator (with <a href="">...</a>)</h2>
<table>
  <tr><td style="vertical-align:top">Developers:</td><td>Maksims Abaļenkovs, Mariel Reyes Salazar, Benjamin Mawdsley, Tim Powell, Benjamin Mummery (Hartree Centre and University of Edinburgh only)</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.python.org">Python</a>,
    <a href="https://networkx.org">NetworkX</a>,
    <a href="https://podman.io">Podman</a>,
    <a href="https://www.docker.com">Docker</a>
  </td></tr>
  <tr><td>Duration:</td><td>4&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>
    
<p class="portfolio">...</p>
</li>


<li>
<h2 id="alcnr">Ada Lovelace Code: Neutron Reflectometry (with <a href="https://www.isis.stfc.ac.uk/Pages/home.aspx">ISIS</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Ciaron Howell, Valeria Losasso, Arwel Hughes</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.perl.org">Perl</a>,
    <a href="https://isocpp.org">C++</a>,
    <a href="https://www.mathworks.com/products/matlab.html">MATLAB</a>,
    <a href="https://www.gnu.org/software/octave/index">GNU Octave</a>,
    <a href="https://apptainer.org">Apptainer</a>,
    <a href="https://www.docker.com">Docker</a>,
    <a href="https://www.nextflow.io">Nextflow</a>
  </td></tr>
  <tr><td>Duration:</td><td>15&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>
    
<p class="portfolio">Neutron reflectometry provides insight into layered molecular structures (e.g. supported lipid bilayers as models for biological membranes). Resolution of neutron reflectometry studies can be enhanced by integrating scattering length density profiles from molecular dynamics simulations into composite models describing the sample, support and surrounding medium {% cite Clifton19 --file references %}.</p>

<figure style="display: block; width: 50%; margin-top: 1em; margin-bottom: 0em; margin-left: auto; margin-right: auto;">
  <a href="{{ site.url }}/assets/photo/lipid_bilayer.png">
    <img src="{{ site.url }}/assets/photo/lipid_bilayer.png" alt="Neutron Reflectometry: Lipid Bilayer">
  </a>
  <figcaption><cite>Example of a&nbsp;simulated lipid bilayer with 26,000&nbsp;atoms.</cite></figcaption>
</figure>

<p class="portfolio">The main goal of this project was to pipeline molecular dynamics simulation with reflectometry analysis software. This workflow consisted of two simulation phases:

<ol style="list-style-type: lower-roman;">
  <li>molecular dynamics and</li>
  <li>neutron reflectometry.</li>
</ol></p>

<p class="portfolio">To streamline numerous experiments with various options both phases were containerised. I&nbsp;created and tested a&nbsp;<a href="https://www.centos.org/centos-linux">CentOS</a>-based <a href="https://apptainer.org">Apptainer</a> to setup the core simulation engines&mdash;<a href="https://www.ks.uiuc.edu/Research/vmd">Visual Molecular Dynamics&nbsp;(VMD)</a> and <a href="https://www.ks.uiuc.edu/Research/namd">Nanoscale Molecular Dynamics&nbsp;(NAMD)</a>. This container included <a href="https://www.python.org">Python</a>, <a href="https://pybilt.readthedocs.io/en/latest/readme.html">PyBILT</a>, <a href="https://www.perl.org">Perl</a>, <a href="http://charmplusplus.org">Charm++</a>, <a href="http://fftw.org">FFTW</a> and <a href="https://www.tcl-lang.org">Tcl</a>. I&nbsp;also developed a&nbsp;series of Perl scripts to launch molecular dynamics experiments in a&nbsp;quick and convenient manner.</p>

<figure style="display: block; width: 50%; margin-top: 1em; margin-bottom: 0em; margin-left: auto; margin-right: auto;">
  <a href="{{ site.url }}/assets/fragment/modify_slurm_script.png">
    <img src="{{ site.url }}/assets/fragment/modify_slurm_script.png" alt="Neutron Reflectometry: Perl subroutine to modify Slurm scripts">
  </a>
  <figcaption><cite>Perl subroutine to modify Slurm scripts.</cite></figcaption>
</figure>

<p class="portfolio">The neutron reflectometry part was developed based on a&nbsp;custom <a href="https://www.docker.com">Docker</a> container with <a href="https://www.mathworks.com/products/matlab.html">MATLAB</a>, its <a href="https://www.mathworks.com/products/parallel-computing.html">Parallel Computing Toolbox</a>, <a href="https://www.cdslab.org/paramonte">ParaMonte</a> and an&nbsp;in-house simulation package called <a href="https://github.com/RascalSoftware/RAT">Reflectivity Algorithms Toolbox for Rascal&nbsp;(RAT)</a>. Initial attempts to embed an&nbsp;<a href="https://www.gnu.org/software/octave/index">Octave</a> interpreter into the autogenerated C++ code failed. Compute-intensive engine of <a href="https://github.com/RascalSoftware/RAT">RAT</a> was automatically converted to C++ by <a href="https://www.mathworks.com/products/matlab-coder.html">MATLAB Coder</a>. Original idea was to run <a href="https://github.com/RascalSoftware/RAT">RAT</a> with an&nbsp;autogenerated C++ code from <a href="https://www.gnu.org/software/octave/index">Octave</a>. But differing definitions of <tt>mxArray</tt> data structure in <a href="">MATLAB</a> and <a href="https://www.gnu.org/software/octave/index">Octave</a> prevented the use of <a href="https://www.gnu.org/software/octave/index">Octave</a>.</p>

<p class="portfolio">Basic <a href="https://www.nextflow.io">Nextflow</a> scripts were written to execute independent experiment stages in parallel. New containers were passed on to STFC scientists. <a href="https://www.isis.stfc.ac.uk/Pages/home.aspx">ISIS</a> researchers uploaded them to Harbor, a&nbsp;local container registry, and made them available for internal use.</p>
</li>


<li>
<h2 id="wormright">WormRight (with <a href="https://www.rau.ac.uk">Royal Agricultural University</a>)</h2>
<table>
  <tr><td>Developer:</td><td>Maksims Abaļenkovs</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.python.org">Python</a>,
    <a href="https://www.tensorflow.org">TensorFlow&nbsp;2</a>,
    <a href="https://github.com/heartexlabs/labelImg">LabelImg</a>,
    <a href="https://cucumber.io/docs/bdd">Cucumber</a>
  </td></tr>
  <tr><td>Duration:</td><td>14&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">The main idea of this proof-of-concept project was to test, if it is possible to develop a&nbsp;machine learning model to detect <i>earthworm casts</i>. This project aimed to help the <a href="https://www.rau.ac.uk">Royal Agricultural University</a> researchers. Their hypothesis is that earthworm casts are a&nbsp;good indicator of farming land fertility. It is usually hard to whiteness earthworms on the land, but their casts remain there for a&nbsp;number of days.</p>

<figure style="display: block; width: 50%; margin-top: 1em; margin-bottom: 0em; margin-left: auto; margin-right: auto;">
  <a href="{{ site.url }}/assets/photo/earthworm_casts_labelled.png">
    <img src="{{ site.url }}/assets/photo/earthworm_casts_labelled.png" alt="WormRight: Labelled earthworm casts">
  </a>
  <figcaption><cite>Labelled earthworm casts.</cite></figcaption>
</figure>

<p class="portfolio">I was a&nbsp;technical leader on the project. Initially I used <a href="https://cucumber.io/docs/bdd">Cucumber</a>, a&nbsp;behaviour-driven development methodology, to identify the need for a&nbsp;high quality dataset with earthworm casts. Next we developed a&nbsp;protocol for collecting the images. Then I used <a href="https://github.com/heartexlabs/labelImg">LabelImg</a> software to label two sets of images with 145 and 827 photographs. Finally, I set up an&nbsp;STFC cloud instance with an&nbsp;NVIDIA Tesla V100 GPU to train a&nbsp;custom object detection model. I experimented with two pre-trained models from <a href="https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md">TensorFlow 2 Detection Model Zoo</a>: SSD ResNet{50,101,152} V1 FPN 640&times;640 {% cite Jung17 --file references %} and EfficientDet D1 640&times;640 {% cite Tan20 --file references %}. Both models were trained further on the labelled earthworm cast images. Models used in the experiments relied on Microsoft's Common Objects in Context (COCO) metrics {% cite Lin15 --file references %}.</p>

<figure class="imagecentre">
  <a href="{{ site.url }}/assets/plot/wormright_avg_prec_acc.svg">
    <img src="{{ site.url }}/assets/plot/wormright_avg_prec_acc.svg" alt="WormRight: Mean average precision of SSD ResNet50">
  </a>
  <figcaption><cite>Mean average precision of SSD ResNet50: x-axis shows the number of model training iterations, while the y-axis denotes the loss value.</cite></figcaption>
</figure>

<p class="portfolio">Depending on the strictness of the Intersection over Union (IoU) threshold, the SSD ResNet50 was able to detect between 3% and 27% of earthworm casts. This project proved that it is possible to train a&nbsp;machine learning model for the detection of earthworm casts. However, the precision and recall values are not high. More work is required to tune the model training for better cast detection.</p>
</li>


<li>
<h2 id="preconditioners">Implicit-Factorisation Preconditioners for NEPTUNE Programme<br>(with <a href="https://www.gov.uk/government/organisations/uk-atomic-energy-authority">UKAEA</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Emre Sahin</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a>,
    <a href="https://www.nektar.info">Nektar++</a>,
    <a href="https://isocpp.org">C++</a>,
    <a href="https://www.petsc.org">PETSc</a>,
    <a href="https://math.nist.gov/MatrixMarket">Matrix Market</a>,
    <a href="https://www.mathworks.com/products/matlab.html">MATLAB</a>,
    <a href="https://www.mpich.org">MPICH</a>,
    <a href="https://www.openmp.org">OpenMP</a>
  </td></tr>
  <tr><td>Duration:</td><td>4&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">This proof-of-concept project for the <a href="https://www.gov.uk/government/organisations/uk-atomic-energy-authority">UKAEA</a> aimed at testing in-house MCMCMI and MSPAI preconditioners in the numerical simulation packages <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a> and <a href="https://www.nektar.info">Nektar++</a>. At first we identified the key simulation problems of interest to the <a href="https://www.gov.uk/government/organisations/uk-atomic-energy-authority">UKAEA</a>. Then we estimated the possibility of integrating our preconditioners into the software packages. My responsibility in this project lied with the <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a> software. Due to the matrix-free nature of the differential equation solvers in <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a> direct integration of in-house preconditioners deemed possible, but time-consuming. That's why we decided to approach the problem in two stages:

<ol style="list-style-type: lower-roman;">
  <li>export system matrices and the right-handside vectors from <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a> and</li>
  <li>import them into MCMCMI and MSPAI for testing.</li>
</ol></p>

<p class="portfolio"><a href="https://www.petsc.org">PETSc</a> solver powering the <a href="https://github.com/boutproject/BOUT-dev">BOUT++</a> computation allowed me to export the matrices in the <tt>*.mtx</tt> <a href="https://math.nist.gov/MatrixMarket">Matrix Market</a> format. Performance tests of the MCMCMI and MSPAI preconditioners on a&nbsp;range of system matrices and right-handside vectors were conducted successfully.</p>
</li>


<li>
<h2 id="ecp">Exascale Computing Project&nbsp;(ECP)</h2>
<table>
  <tr><td>Developer:</td><td>Maksims Abaļenkovs</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://isocpp.org">C++</a>,
    <a href="https://www.gnu.org/software/octave/index">GNU Octave</a>,
    <a href="http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview">ParMETIS</a> library,
    <a href="https://www.hdfgroup.org/solutions/hdf5">HDF5</a> library
  </td></tr>
  <tr><td>Duration:</td><td>6&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">My&nbsp;role in&nbsp;the project comprised of&nbsp;learning and prototyping multiple mathematical methods: the Markov Chain Monte Carlo Matrix Inversion (MCMCMI) method and the <a href="https://en.wikipedia.org/wiki/Casimir_effect">Casimir interactions</a> method, as&nbsp;well as&nbsp;analysing the application of&nbsp;<a href="http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview">ParMETIS</a> library for direct solvers and developing an&nbsp;example code for reading and writing large sparse matrices in&nbsp;<a href="https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_%28CSR%2C_CRS_or_Yale_format%29">CRS format</a>. These matrices are read and written to&nbsp;hard disk by&nbsp;means of&nbsp;the parallel <a href="https://www.hdfgroup.org/solutions/hdf5">HDF5</a> library with compression.</p>
</li>


<li>
<h2 id="macad">Mathematics for Competitive Advantage (MACAD) (<a href="https://www.a4i.info/a4i-overview">A4I</a> with <a href="http://www.maxeler.com">Maxeler</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Emre Sahin</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C</a>,
    <a href="https://isocpp.org">C++</a>,
    <a href="https://www.perl.org">Perl</a>,
    <a href="https://www.gnu.org/software/octave/index">GNU Octave</a>
  </td></tr>
  <tr><td>Duration:</td><td>12&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">The main goals of&nbsp;this project were:

<ol style="list-style-type: lower-roman;">
  <li>adopt custom number formats, alternative to&nbsp;standard <a href="https://en.wikipedia.org/wiki/IEEE_754">IEEE754</a> single and double precision in&nbsp;order to&nbsp;save memory and speed-up algorithm execution on&nbsp;<a href="https://en.wikipedia.org/wiki/Field-programmable_gate_array">FPGAs</a> and</li>
  <li>design a&nbsp;general methodology for analysing numerical linear algebra algorithms and efficiently porting them to&nbsp;<a href="https://en.wikipedia.org/wiki/Field-programmable_gate_array">FPGA</a> hardware.</li>
</ol></p>

<p class="portfolio">The target algorithms were&nbsp;QR decomposition and Markov Chain Monte Carlo Matrix Inversion (MCMCMI) method. Classical Householder reflectors&rsquo; QR&nbsp;algorithm as&nbsp;well as&nbsp;compact block representations with&nbsp;WY and YTY<sup>T</sup> were developed in&nbsp;GNU Octave and ported to&nbsp;C and&nbsp;C++ for efficiency. Selected algorithms were value profiled with Maxeler&rsquo;s profiling library. Possibilities for utilising custom <i>arbitrary float</i> and <i>fixed point offset</i> number formats were detected. Prototype C++&nbsp;code enabling custom precision datatypes was developed. QR&nbsp;and MCMCMI algorithms in&nbsp;arbitrary float and fixed point offset datatypes (with various number of&nbsp;exponent and mantissa bits as&nbsp;well as&nbsp;total and fractional bit numbers) were tested. Experiments were performed with help of&nbsp;Maxeler library simulating custom number formats on&nbsp;CPUs. Optimal number format for&nbsp;QR was arbitrary float with 6&nbsp;exponent and 9&nbsp;mantissa bits. MCMCMI achieved peak performance in a&nbsp;fixed point offset format with 16&nbsp;bits comprised of&nbsp;2&nbsp;integer and 14&nbsp;fractional bits. Both algorithms were ported to&nbsp;Maxeler&rsquo;s <a href="https://en.wikipedia.org/wiki/Field-programmable_gate_array">FPGA</a> cards. Extensive performance tests of&nbsp;QR and MCMCMI&nbsp;on CPU and <a href="https://en.wikipedia.org/wiki/Field-programmable_gate_array">FPGA</a> architectures were conducted.</p>

<p class="portfolio">Semi-automated workflow for efficient porting of&nbsp;numerical algorithms to&nbsp;<a href="https://en.wikipedia.org/wiki/Field-programmable_gate_array">FPGAs</a> was proposed. <a href="https://en.wikipedia.org/wiki/Bisection_method">Bisection</a>, <a href="https://en.wikipedia.org/wiki/Golden-section_search">golden section</a> and <a href="https://en.wikipedia.org/wiki/Nelder–Mead_method">Nelder&ndash;Mead</a>&ndash;Adamovich algorithms were utilised to&nbsp;detect the optimal number of&nbsp;bits in&nbsp;a&nbsp;pair (exponent and mantissa bits for the arbitrary float or&nbsp;total and fractional bits for the fixed point offset).</p>

<p class="portfolio">Developed source code is&nbsp;open and is&nbsp;publicly available in&nbsp;the <a href="https://gitlab.com/mabalenk/macad">MACAD</a> Git repository. Results of this work were <a href="https://doi.org/10.1109/ScalA54577.2021.00011">published</a> and presented at the <a href="https://sc21.supercomputing.org">International Conference for High Performance Computing, Networking, Storage, and Analysis&nbsp;(SC21)</a> in November 2021.</p>
</li>


<li>
<h2 id="zipfstime">Validation and Verification of an&nbsp;AI/ML System (<a href="https://www.a4i.info/a4i-overview">A4I</a> with <a href="http://www.kadlytics.com">KADlytics</a>)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Michail Smyrnakis</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://www.python.org">Python</a>,
    <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C</a>,
    <a href="https://www.openmp.org">OpenMP</a>,
    <a href="https://jupyter.org">Jupyter</a> notebooks,
    <a href="https://networkx.org">NetworkX</a>,
    <a href="https://numpy.org">NumPy</a> modules
  </td></tr>
  <tr><td>Duration:</td><td>12&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">During this project we&nbsp;created a&nbsp;unique scientifically proven methodology for validation and verification of&nbsp;an&nbsp;AI system. The major difficulty hindering <i>classical</i> evaluation of&nbsp;the system was the lack of&nbsp;<i>ground truth data</i>. The developed methodology operates with the minimum (or&nbsp;no) ground truth data and applies to&nbsp;any AI/ML system that can be&nbsp;represented as&nbsp;a&nbsp;graph. <a href="https://gitlab.com/mabalenk/zipfstime">Project repository</a> contains Python and&nbsp;C source code to&nbsp;conduct the analysis, multiple Jupyter notebooks explaining the key concepts used in&nbsp;this evaluation as&nbsp;well as&nbsp;in-depth technical reports. A&nbsp;follow-up research paper is&nbsp;in&nbsp;preparation.</p>
</li>


<li>
<h2 id="dlmeso">Parallel I/O for DL_MESO: NetCDF and HDF5&nbsp;(IROR)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Chris Dearden</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://fortran-lang.org">Fortran</a>,
    <a href="https://www.mpich.org">MPICH</a>,
    <a href="https://www.unidata.ucar.edu/software/netcdf">NetCDF</a>,
    <a href="https://www.hdfgroup.org/solutions/hdf5">HDF5</a> libraries
  </td></tr>
  <tr><td>Duration:</td><td>6&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio">In&nbsp;this project we&nbsp;worked on&nbsp;an&nbsp;in-house computational chemistry suite called <a href="https://gitlab.stfc.ac.uk/dl_meso/dl_meso">DL_MESO</a>, DPD. We&nbsp;enhanced parallel output capabilities of&nbsp;DPD. The software was extended to&nbsp;organise and output simulation results in&nbsp;scientific data formats such as&nbsp;<a href="https://www.unidata.ucar.edu/software/netcdf">NetCDF</a> and <a href="https://www.hdfgroup.org/solutions/hdf5">HDF5</a>. Performance results on&nbsp;<a href="https://www.hartree.stfc.ac.uk/Pages/Installing-a-new-supercomputer.aspx">Scafell Pike</a> show sixfold reduction in&nbsp;output file size and twofold reduction in&nbsp;average memory (RAM) utilisation. Results were presented to&nbsp;Michael Seaton, the main developer of&nbsp;<a href="https://gitlab.stfc.ac.uk/dl_meso/dl_meso">DL_MESO</a>, and integrated into the software. Auxiliary Git repository <a href="http://hcp004.hartree.stfc.ac.uk/mabalenk/sci-file-io">sci-file-io</a> with multiple examples on&nbsp;creating, reading and writing sequential and parallel <tt>*.h5</tt> and <tt>*.nc</tt> files was created in&nbsp;the process.</p>
</li>


<li>
<h2 id="ummap">Enabling UMMAP to Understand GROMACS File Formats&nbsp;(IROR)</h2>
<table>
  <tr><td>Developers:</td><td>Maksims Abaļenkovs, Chris Dearden</td></tr>
  <tr><td>Tech&nbsp;Stack:</td>
  <td>
    <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C</a>,
    <a href="https://github.com/chemfiles/xdrfile">xdrfile</a> library
  </td></tr>
  <tr><td>Duration:</td><td>3&nbsp;months</td></tr>
  <tr><td>Status:</td><td>completed</td></tr>
</table>

<p class="portfolio"><a href="http://hcp004.hartree.stfc.ac.uk/dbray/UMMAP">UMMAP</a> is&nbsp;the Hartree Centre&rsquo;s in-house software tool for post-processing results of&nbsp;computational chemistry simulators. In&nbsp;this project we&nbsp;utilised <a href="https://www.gromacs.org">GROMACS</a> <a href="https://github.com/chemfiles/xdrfile">xdrfile</a> library to&nbsp;extend UMMAP functionality for reading <a href="https://manual.gromacs.org/archive/5.0.4/online/xtc.html">XTC</a> and <a href="https://manual.gromacs.org/archive/5.0.4/online/trr.html">TRR</a> file formats. Git repository <a href="git@hcp004.hartree.stfc.ac.uk:mabalenk/sci-file-io.git">sci-file-io</a> with <tt>*.xtc</tt> and <tt>*.trr</tt> file readers was created. After a&nbsp;series of&nbsp;tests the XTC and TRR reading functionality was integrated into UMMAP.</p>
</li>
</ol>

<h3>References</h3>

{% bibliography --file references --cited %}
